#include "MouseControll.h"



MouseControll::MouseControll()
{
}


MouseControll::~MouseControll()
{
}

bool MouseControll::Initialize(){
	sc = Screen::GetScreen();
	return true;
}

void MouseControll::Update(){
	
	//マウスの座標を取得
	glfwGetMousePos(&mousePOS.X, &mousePOS.Y);
	g_mousePOS.X = mousePOS.X + (int)sc->x;
	g_mousePOS.Y = mousePOS.Y + (int)sc->y;
	wheel = glfwGetMouseWheel();

	//左ボタンのクリックを取得
	if (GLFW_PRESS == glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT)){
		Lbutton = true;
	}
	else{
		Lbutton = false;
	}
	//右ボタンのクリックを取得
	if (GLFW_PRESS == glfwGetMouseButton(GLFW_MOUSE_BUTTON_RIGHT)){
		Rbutton = true;
	}
	else{
		Rbutton = false;
	}
}