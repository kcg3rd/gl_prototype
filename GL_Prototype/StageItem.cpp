#include <Windows.h>
#include "Scene.h"
#include "StageItem.h"
#include "Draw.h"
#include "ResourceController.h"

StageItem::StageItem(){
	getFlag = 0;
	onStage = true;
	width = 0;
	height = 0;
}
StageItem::~StageItem()
{

	//delete v;
}

StageItem::StageItem(int ID, ItemType Type, int sx, int sy, int param) : Item(ID, Type)
{
	StageItem();
	dr = Draw::GetDraw();
	RC = ResourceController::GetResourceController();
	getFlag = 1;	//取得可能
	x = sx;
	y = sy;
	itemparam1 = param;
	switch (Type){
	case ItemType::Gold://ゴールド
		v = Vertex(sx, sy, &RC->item.gold);
		break;
	case ItemType::Manekineko:
		v = Vertex(sx, sy, &RC->item.neko);
		break;
	case ItemType::Key:
		v = Vertex(sx, sy, &RC->item.key);
		break;
	default:
		assert(0);//定義外アイテム
	}
	width = v.GetWidth();
	height = v.GetHeight();
}
StageItem::StageItem(int ID, ObjectType Type, int sx, int sy, int w, int h,int param) : Item(ID, ItemType::Gold)
{
	StageItem();
	dr = Draw::GetDraw();
	RC = ResourceController::GetResourceController();
	x = sx;
	y = sy;
	width = w;
	height = h;
	getFlag = 0;
	objecttype = Type;
	onStage = true;
	assert(0);
}

  StageItem::StageItem(const StageItem& si) :Item(si.itemID, si.itemType)
{
	 StageItem();
	v = si.v;
	//*v = *si.v;
	dr = si.dr;
	RC = si.RC;
	getFlag = si.getFlag;
	x = si.x;
	y = si.y;
	width = si.width;
	height = si.height;
	objecttype = si.objecttype;
	onStage = si.onStage;

}

StageItem& StageItem::operator=(const StageItem& si){
	Item::operator=(si);
	StageItem();
	v = si.v;

 	dr = si.dr;
	RC = si.RC;

	getFlag = si.getFlag;
	x = si.x;
	y = si.y;
	width = si.width;
	height = si.height;
	objecttype = si.objecttype;
	onStage = si.onStage;

	return *this;
}

bool StageItem::CheckPosition(int sx, int sy,int swidth, int sheight){
	if (((x  >= sx && x + width/2 < sx + swidth ) || (x + width/2 >= sx && x+width <= sx+swidth ))
		&& (y >= sy && y+ height/2 <= sy + sheight || y + height/2 >= sy && y + height < sy + sheight)){
		if (getFlag == 1)
			return true;
	}
	return false;
}

bool StageItem::Update(){

	dr->VertexPushBack(&v, 2);
	return true;
}