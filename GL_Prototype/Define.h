#ifndef ___DEFINE_H___
#define ___DEFINE_H___

#define GLFW_DLL
#include <GL/glew.h>
#include <GL/glfw.h>

#include <cfloat>

#define S_CAST_I(value) static_cast<int>(value)		//スタティックキャスト用マクロ(int)
#define S_CAST_F(value) static_cast<float>(value)	//スタティックキャスト用マクロ(float)

//値がエラー値でないか判断するマクロ
//[value: 対象の値] [errorValue: エラー値] [defaultValue: エラー値だった場合の代替値]
#define CHECK_VALUE(value, errorValue, defaultValue) (value != errorValue ? value : defaultValue)

const float gravity = 0.6f;		//ゲーム内での落下速度の増加量

enum Direction{		//キャラクターの方向
	Left = -1,		//左向き
	Right = 1		//右向き
};

//矩形構造体
struct RectangleF{
	float left;
	float top;
	float right;
	float bottom;
	RectangleF(){}
	RectangleF(float left, float top, float right, float bottom)
		:left(left), top(top), right(right), bottom(bottom){ }
};

enum UIImageName{	//UI画像の名前列挙型
	Captuer = 0,	//「捕」マーク
	Exclamation,	//「！」マーク
	Question,		//「？」マーク
	Hide,			//「隠」マーク
	Shinobi			//「忍」マーク
};
//座標構造体
struct Position{
	float X;
	float Y;
};
struct Position_i{
	int X;
	int Y;
};
enum ObjectType{
	Box,
	StelthBox,
	Ladder,
	BackDoor,
	NextDoor
};

enum ItemType{
	Manekineko,
	Gold,
	Kunai,
	Makibishi,
	Key
};

enum EnemyType{		//Enemyの種類
	Type1 = 0,		//町人風(男性)のやつ
	Type2,			//町人風(女性)のやつ
	Type3,			//町奉行なやつ
	Type4,			//貴族風なやつ(使用しない)
	Type5,			//ボス
	Type6			//未作成
};

enum EnemyMoveMode{	//Enemyの行動パターン
	NotMove = -1,	//動かない
	BackAndForth,	//左右に行ったり来たり
	WallToWall,		//壁から壁まで歩き続ける
	GoToPlayer,		//Playerに向かって歩く
	Search,			//
	TestMode		//試作モード
};

struct CharacterParamSet {	//Characterを構成する値
	float posX;				//X座標
	float posY;				//Y座標
	float width;			//幅
	float height;			//高さ
	float speed;			//移動速度
	int   hp;				//体力
};

struct PlayerParamSet {		//Playerのパラメーター構造体
	bool  debugMode;		//デバッグモードフラグ
	float speed;			//移動速度
	float slowSpeed;		//しゃがみ移動速度
	float dashMinSpeed;		//走る速度最小値
	float dashMaxSpeed;		//走る速度最高値
	float dashAdd;			//走る速度上昇率
	float dashSub;			//走る速度減少率
	float jump;				//跳躍力
	int   hp;				//体力
	int   wallet;			//所持金
	int   weaponInterval;	//武器の使用間隔
	int   weaponSwitchNum;	//武器を切り替える間隔
	PlayerParamSet()
		:debugMode(false)
		, speed(-1.0f)
		, slowSpeed(-1.0f)
		, dashMinSpeed(-1.0f)
		, dashMaxSpeed(-1.0f)
		, dashAdd(-1.0f)
		, dashSub(-1.0f)
		, jump(-1.0f)
		, hp(-1)
		, wallet(-1)
		, weaponInterval(-1)
		, weaponSwitchNum(-1)
	{}
};

struct EnemyParamSet {		//Enemyのパラメーター構造体
	float speed;			//移動速度
	float chaseSpeed;		//Playerを追いかける速度
	float jump;				//跳躍力
	int   hp;				//体力
	int   power;			//攻撃力
	int   waitFrameNum;		//待機時間(フレーム数)
	int   moveFrameNum;		//移動時間(フレーム数)
	float searchRangeX;		//Playerを認識する範囲(X軸)
	float searchRangeY;		//Playerを認識する範囲(Y軸)
	explicit EnemyParamSet()
		: speed(-1.0f)
		, chaseSpeed(-1.0f)
		, jump(-1.0f)
		, hp(-1)
		, waitFrameNum(-1)
		, moveFrameNum(-1)
		, searchRangeX(-1.0f)
		, searchRangeY(-1.0f)
	{}
};
struct ItemParamSet{
	struct HookShot{
		int speed;
		int limit;
		explicit HookShot()
			:speed(-1)
			, limit(-1)
		{}
	}hookshot;
	explicit ItemParamSet()
		:hookshot()
	{}
};

struct StageParamSet{
	int stage_limits;
	int background_limits;
};

struct WeaponParamSet{		//武器のパラメーター構造体
	float speed;			//移動速度
	int   power;			//攻撃力
	int   activeFrameNum;	//有効フレーム数
	explicit WeaponParamSet()
		:speed(-1.0f)
		, power(-1)
		, activeFrameNum(-1)
	{}

};

#endif