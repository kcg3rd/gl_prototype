#include "Animation.h"

#include "Vertex.h"
#include "Draw.h"

void Animation::CreateAnimation(std::vector<GLuint>* textures)
{
	animation.resize(textures->size());
	int cnt = 0;
	for (std::vector<Vertex*>::iterator it = animation.begin(); it != animation.end(); it++){
		(*it) = new Vertex(0, 0, &textures->at(cnt++));
	}
}

void Animation::Initialize()
{
	frameCounter = 0;

	switch (playMode){
	case NormalPlay:
	case TurnbackPlay:
		currentAnimationNum = 0;	//正順で開始のとき
		playDir = normal;
		break;
	case ReversePlay:
	case TurnbackPlayR:
		currentAnimationNum = animation.size() - 1;	//逆順で開始のとき
		playDir = reverse;
		break;
	}
}

void Animation::Update()
{
 	if (frameCounter >= updateFrameNum){
		frameCounter = 0;
		//<1>カレント番号の更新処理
		currentAnimationNum += playDir;
		switch (playMode){
		case NormalPlay:
		case ReversePlay:
			if (currentAnimationNum > static_cast<int>(animation.size()) - 1){	//正順での限界値
				currentAnimationNum = 0;
			}
			else if (currentAnimationNum < 0){	//逆順での限界値
				currentAnimationNum = animation.size() - 1;
			}
			break;

		case TurnbackPlay:
		case TurnbackPlayR:
			if (currentAnimationNum <= -1 || currentAnimationNum >= static_cast<int>(animation.size())){
				playDir = (playDir == normal ? reverse : normal);
				currentAnimationNum += playDir * 2;	//1倍ではアニメーションが変化しないため
			}
			break;
		}
		//<1>
	}
	else{
		frameCounter++;
	}
}

void Animation::SetPlayMode(AnimPlayMode playMode)
{
	this->playMode = playMode;

	switch (playMode){
	case NormalPlay:
	case TurnbackPlay:
		playDir = normal;
		break;
	case ReversePlay:
	case TurnbackPlayR:
		playDir = reverse;
		break;
	}
}

Vertex* Animation::GetCurrentVertex()
{
	return animation[currentAnimationNum];
}

Vertex* Animation::GetVertex(int index)
{
	return animation[index];
}

bool Animation::EndOfAnimation()
{
	if (playMode == TurnbackPlay || playMode == TurnbackPlayR){
		//return false;	//折り返し再生モード時はどこが終点かよくわからない
		
		//とりあえずアニメーションの両端でTrueになる
		return currentAnimationNum == 0 || currentAnimationNum == animation.size() - 1;
	}
	else if (playMode == ReversePlay && currentAnimationNum == 0){
		return true;
	}
	else if (currentAnimationNum == animation.size() - 1){
		return true;
	}
	return false;
}

void Animation::DestroyAnimation()
{
	for (std::vector<Vertex*>::iterator it = animation.begin(); it != animation.end(); it++){
		delete (*it);
	}
	animation.clear();
}