#include "Weapon.h"

#include "Vertex.h"
#include "Draw.h"
#include "ResourceController.h"
#include "Stage.h"

Weapon::Weapon(float width, float height, float speed, int power, int frameNum)
	:x(0.0f), y(0.0f), width(width), height(height), speed(speed), power(power), lifeFrameNum(frameNum)
{
}

void Weapon::CreateWeapon(ItemType type)
{
	ResourceController* rc = ResourceController::GetResourceController();
	
	switch (type){
	case ItemType::Kunai:
		vertex = new Vertex(0, 0, &rc->item.kunai);
		MoveUpdate = &Weapon::UpdateKunai;
		speed        = CHECK_VALUE(rc->parameter.kunai.speed, -1.0f, 16.0f);
		power        = CHECK_VALUE(rc->parameter.kunai.power, -1, 0);
		lifeFrameNum = CHECK_VALUE(rc->parameter.kunai.activeFrameNum, -1, 40);
		break;

	case ItemType::Makibishi:
		vertex = new Vertex(0, 0, &rc->item.makibishi);
		MoveUpdate = &Weapon::UpdateMakibishi;
		speed        = CHECK_VALUE(rc->parameter.makibishi.speed, -1.0f, 4.0f);
		power        = CHECK_VALUE(rc->parameter.makibishi.power, -1, 0);
		lifeFrameNum = CHECK_VALUE(rc->parameter.makibishi.activeFrameNum, -1, 100);
		break;
	}
	
	dr = Draw::GetDraw();
}

void Weapon::Initialize()
{
	activeFlag = false;
	currentFrameCount = 0;
}

void Weapon::Update(Stage* stage)
{
	if (!activeFlag){
		return;
	}

	(this->*MoveUpdate)(stage);

	dr->VertexPushBack(vertex->SetDraw(x, y, direction == Left), 1);
}

//クナイでの更新処理
void Weapon::UpdateKunai(Stage* stage)
{
	if (currentFrameCount >= lifeFrameNum){
		activeFlag = false;
		return;
	}
	currentFrameCount++;

	float prevX = x, prevY = y;

	//一定方向に進み続ける
	x += direction * speed;
	y;

	//ステージと接触していたら非アクティブに変更
	//if (x >= 0.0f && x <= stage->GetWidth()){
	//	int stageData = stage->GetStageData(static_cast<int>(GetCenterX()), static_cast<int>(GetCenterY()));
	//	if (stageData == 1){
	//		activeFlag = false;
	//	}
	//}
}

//マキビシでの更新処理
void Weapon::UpdateMakibishi(Stage* stage)
{
	if (currentFrameCount >= lifeFrameNum){
		activeFlag = false;
		return;
	}
	currentFrameCount++;

	float prevX = x, prevY = y;
	x += direction * speed;
	y += fallSpeed += gravity;

	//ステージと接触した場合はその地点でとどまる
	int floorHeight = stage->GetCollision_Bottom((int)GetCenterX(), (int)GetCenterY());
	if ((int)GetBottom() >= floorHeight){
		x = prevX;
		//y = floorHeight - height;
		y = prevY;
		fallSpeed = 0.0f;
	}

}

//Weaponを作動させる
void Weapon::Activate(float posX, float posY, Direction dir, float speed){
	x = posX - width / 2.0f;	//↓
	y = posY - height / 2.0f;	//指定座標が中心になるように
	direction = dir;
	if (speed != FLT_MAX) this->speed = speed;
	activeFlag = true;
	currentFrameCount = 0;
	fallSpeed = -5.0f;		//マキビシのための落下速度の初期化
}


void Weapon::DestroyWeapon()
{
	delete vertex;
}