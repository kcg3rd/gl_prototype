#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "MouseControll.h"
#include "Vertex.h"
#include "Draw.h"
#include "Stage.h"

class Player;
class Draw;
class Screen;
class Light:protected MouseControll
{
public:
	Light();
	~Light();
	bool Initialize(Player* player);
	bool Update();
	bool InLight(Position_i*);
private:
	Screen* screen;
	Vertex* use_filter;
	Vertex* on;
	Vertex* off;
	ResourceController* RC;
	Player* p;
	Position_i light_mouse;
	Position_i light_chara;
	Position_i* use_pos;
	Position_i size;
	Position_i windowsize;
	int beforewheel;
	Draw* dr;
};

#endif