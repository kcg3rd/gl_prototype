#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_


#include <AL\al.h>
#include <AL\alc.h>
#include <AL\alut.h>
#include <string>
#include <vector>

/*シングルトンクラス*/
class SoundManager
{
public:
	static SoundManager* GetSoundManager(){
		static SoundManager soundmanager;
		return &soundmanager;
	}
	bool Initialize();
	int SetSound(std::string);
	void CallSound(int num);
private:
	SoundManager();
	~SoundManager();
	int  ReadHeaderWav(FILE* fp, int *channel, int* bit, int *size, int* freq);
	ALCdevice* device;
	ALCcontext* context;
	ALuint source_id;
	ALuint buffer_id;
	std::vector<std::string>file;
	std::vector <ALuint> buf;
	std::vector<ALuint> src;
	std::vector<unsigned char*>data;
	size_t pcm_freq;
	float key_freq;
	size_t pcm_length_sec;
	std::vector<ALdouble>pcm_data;
};

#endif