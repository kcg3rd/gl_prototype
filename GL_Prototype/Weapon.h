#ifndef _WEAPON_H_
#define _WEAPON_H_

#include "Define.h"

class Vertex;
class Draw;
class Stage;

//Playerが使用する武器クラス
class Weapon
{
public:
	Weapon(){};
	Weapon(float width, float height, float speed, int power, int lifeFrameNum);
	~Weapon(){ DestroyWeapon(); };

	//Weaponクラスを作成 + ResourceControllerのパラメーターを取得する
	void CreateWeapon(ItemType type);

	void Initialize();
	void Update(Stage* stage);


	//左(x)座標を取得する
	inline float GetLeft(){ return x; }

	//上(y)座標を取得する
	inline float GetTop(){ return y; }

	//右(x + width)座標を取得する
	inline float GetRight(){ return x + width; }

	//下(y + height)座標を取得する
	inline float GetBottom(){ return y + height; }

	//中心X座標を取得する
	inline float GetCenterX(){ return x + width / 2.0f; }

	//中心Y座標を取得する
	inline float GetCenterY(){ return y + height / 2.0f; }

	//サイズを取得する
	inline void GetSize(float* widthBuf, float* heightBuf) {
		*widthBuf = width;
		*heightBuf = height;
	}

	//移動方向を取得する
	inline Direction GetDirection(){ return direction; }

	//スピードを取得する
	inline float GetSpeed(){ return speed; }

	//攻撃力を取得する
	inline int GetPower(){ return power; }

	//有効フレーム数を取得する
	inline int GetLifeFrameNum(){ return lifeFrameNum; }

	//アクティブフラグを取得する
	inline bool GetActive(){ return activeFlag; }


	//位置を設定する
	inline void SetPosition(float x, float y) {
		this->x = x;
		this->y = y;
	}

	//サイズを設定する
	inline void SetSize(float width, float height) { this->width = width; this->height = height; }

	//移動方向を設定する
	inline void SetDirection(Direction dir){ direction = dir; }

	//スピードを設定する
	inline void SetSpeed(float speed){ this->speed = speed; }

	//攻撃力を設定する
	inline void SetPower(int value){ power = value; }

	//有効フレーム数を設定する
	inline void SetLifeFrameNum(int lifeFrameNum){ this->lifeFrameNum = lifeFrameNum; }

	//アクティブフラグを設定する
	inline void SetActive(bool active = true){ activeFlag = active; }

	//Weaponを作動させる
	//※speedを省略した場合は既存の値が設定される(デフォルト値は使用されない)
	void Activate(float posX, float posY, Direction dir, float speed = FLT_MAX);

	//当たったことを伝えるメソッド
	inline void HasHit(){ activeFlag = false; }

private:
	Vertex* vertex;
	Draw* dr;
	float x;			//X座標
	float y;			//Y座標
	float width;		//幅
	float height;		//高さ
	float speed;		//スピード
	int   power;		//攻撃力
	int   lifeFrameNum;	//有効フレーム数
	bool  activeFlag;	//ゲームシーン上にあるフラグ
	
	Direction direction;	//方向
	float fallSpeed;		//落下速度

	int currentFrameCount;

	void (Weapon::*MoveUpdate)(Stage*);	//武器ごとに異なった更新処理をする
	void UpdateKunai(Stage*);			//クナイでの更新メソッド
	void UpdateMakibishi(Stage*);		//マキビシでの更新メソッド

	//Weaponクラスで動的確保されたメモリリソースを開放するメソッド
	void DestroyWeapon();

};
#endif