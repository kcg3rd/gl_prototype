#include "Boss.h"

#include "Animation.h"
#include "ResourceController.h"
#include "Stage.h"
#include "Player.h"

Boss::Boss(float x, float y, float w, float h, float speed, float jumpF, Direction dir, int rank)
	:Enemy(x, y, w, h, speed, jumpF, dir, rank)
{


}

//Bossクラスを生成する処理
void Boss::CreateBoss(Player* player)
{
	dr = Draw::GetDraw();		//Drawクラスへのポインターを設定
	playerInfo = player;		//Playerクラスへのポインターを設定

	SetAnimationGraphic();		//アニメーションクラスを準備する
}

void Boss::SetAnimationGraphic()
{
	normalMoveAnim = new Animation();
	attackAnim = new Animation();

	ResourceController* rc = ResourceController::GetResourceController();
	normalMoveAnim->CreateAnimation(&rc->boss.walk);
	attackAnim->CreateAnimation(&rc->boss.action);
}

//ResourceControllerクラスのパラメーターを読み込む
void Boss::LoadResourceControllerValue()
{
	//ResourceController* rc = ResourceController::GetResourceController();
	Enemy::LoadResourceControllerValue();
}

//初期化処理
void Boss::Initialize()
{

}

//更新処理
void Boss::Update(Stage* stage)
{
	MoveMode5(stage);

	checkCollisionStage(stage);

	UpdateAnimation();

}

void Boss::UpdateAnimation()
{
	bool mirrorFlag = (direction == Direction::Right);

	//アニメーションクラスを更新する
	switch (animState){
	case AnimationState::Walk:
	case AnimationState::Stop:
	case AnimationState::Dash:
	case AnimationState::Found:
	case AnimationState::Damage:
	case AnimationState::Search:
		Boss::normalMoveAnim->Update();
		dr->VertexPushBack(normalMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;
	
	case AnimationState::Action:
		attackAnim->Update();
		dr->VertexPushBack(attackAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;
	}

}

void Boss::DestroyBoss()
{
	//クラスが破棄されるときの処理
	delete normalMoveAnim;
	delete attackAnim;

}
