#include "StageObject.h"

#include "Draw.h"
#include "ResourceController.h"
#include "Scene.h"

StageObject::StageObject()
{
}

StageObject::StageObject(int ID, ObjectType Type, int sx, int sy, int w, int h, int sparam){
	dr = Draw::GetDraw();
	RC = ResourceController::GetResourceController();
	x = sx;
	y = sy;
	width = w;
	height = h;
	objecttype = Type;
	switch (Type){

	case ObjectType::Box://木箱
		v = Vertex(x, y, width, height, &RC->stageObject.box);
		break;
	case ObjectType::StelthBox://隠れる箱
		v = Vertex(x, y, width, height, &RC->stageObject.box);
		v.ChangeAlpha(0.5f);
		break;
	case ObjectType::Ladder://はしご
		v = Vertex(x, y - 80, width, height + 80, &RC->stageObject.ladder, 0, 1);
		break;
	case ObjectType::NextDoor://ドア
	case ObjectType::BackDoor:
		v = Vertex(x, y, width, height, &RC->stageObject.door);
		param = sparam;
		break;
	default:
		assert(0);	//定義外オブジェクト
		break;
	}
	pos.X = (float)x;
	pos.Y = (float)y;
}


StageObject::~StageObject()
{
}

StageObject::StageObject(const StageObject& so)
{
	StageObject();
	v = so.v;
	dr = so.dr;
	RC = so.RC;
	x = so.x;
	y = so.y;
	pos = so.pos;
	width = so.width;
	height = so.height;
	objecttype = so.objecttype;
	param = so.param;
}

StageObject& StageObject::operator=(const StageObject& so){
	StageObject();
	v = so.v;
	dr = so.dr;
	RC = so.RC;
	x = so.x;
	y = so.y;
	width = so.width;
	height = so.height;
	objecttype = so.objecttype;
	param = so.param;
	return *this;
}


bool StageObject::Action(){
	switch ((objecttype))
	{
	case ObjectType::NextDoor:
	case ObjectType::BackDoor:
		Scene::GetScene()->StageChange(param);
		return true;
		break;
	default:
		return false;
	}
}

bool StageObject::Update(){
	switch (objecttype){

	case ObjectType::StelthBox://隠れる箱
		dr->VertexPushBack(&v, 1);
		break;
	default:
		dr->VertexPushBack(&v, 2);
		break;
	}
	return true;
}

bool StageObject::CheckPosition(int sx, int sy){
	if ((x <= sx && sx <= x + width) && (y <= sy && sy <= y + height)){
			return true;
	}
	return false;
}

Position& StageObject::GetPosition(){
	return pos;
}