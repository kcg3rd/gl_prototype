#ifndef ___COLLISION_H___
#define ___COLLISION_H___

////矩形構造体
//struct RectangleF{
//	explicit RectangleF(float left, float top, float right, float bottom)
//		:left(left), top(top), right(right), bottom(bottom){ }
//
//	float left;
//	float top;
//	float right;
//	float bottom;
//};
struct RectangleF;

//当たり判定用クラスメソッドクラス
class Collision
{
public:
	//矩形と矩形の当たり判定
	static bool RectToRect(const RectangleF* const rc1, const RectangleF* const rc2)
	{
		if ((rc1->left < rc2->right) && (rc1->right > rc2->left)
			&& (rc1->top < rc2->bottom) && (rc1->bottom > rc2->top)){
			return true;
		}
		else {
			return false;
		}

	}

	//矩形と点の当たり判定
	static bool RectToPoint(const RectangleF* const rc, const float pointX, const float pointY)
	{
		if ((pointX >= rc->left && pointX <= rc->right)
			&& (pointY >= rc->top && pointY <= rc->bottom)){
			return true;
		}
		else{
			return false;
		}
	}
	
	//矩形と円の当たり判定(未実装)
	static bool RectToCircle(const RectangleF* const rc, const int centerX, const int centerY, const int radius)
	{
		return false;
	}
	
	//円と円の当たり判定(未実装)
	static bool CircleToCircle(const int centerX1, const int centerY1, const int radius1,
		const int centerX2, const int centerY2, const int radius2)
	{
		return false;
	}

	//点と円の当たり判定(未実装)
	static bool PointToCircle(const int pointX, const int pointY, const int centerX, const int centerY, const int radius)
	{
		return false;
	}

private:
	Collision() = delete;
	~Collision(){};
	void* operator new(size_t) = delete;

};
#endif