﻿#include<Windows.h>

#define GLFW_DLL
// OpenGLの新しい機能を使うために
// ライブラリ「GLEW」を利用
// そのライブラリをスタティックリンク形式で使う

#define GLEW_STATIC

#if defined (_MSC_VER)
// GLEWのヘッダファイルを使う(Windows)
#include<GL/glew.h>
#endif
#include<GL/glfw.h>


#include<cstdlib>




#if defined (_MSC_VER)
#pragma comment(lib,"GLFWDLL.lib")
#pragma comment(lib,"opengl32.lib")  
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"OpenAL32.lib")
#pragma comment(lib,"alut.lib")

#ifdef _DEBUG
#pragma comment(lib,"glew32s.lib")

#else
#pragma comment(lib,"glew32s.lib")
#endif
#endif

#include "Game.h"

#include "MemoryLeakChecker.h"

int width = 800;// 1024;
int height = 600;// 800;

int main(){
	//static MemoryLeakChecker memoryLeakChecker;						 //デバッグ用	ライブラリのメモリを誤検出?

	bool Memory_Debug = 0;//メモリデバッグ

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);		//プログラム終了時に自動で_CrtDumpMemoryLeaksが呼ばれる
	if (Memory_Debug){
		MessageBox(NULL, TEXT("_CrtSetBreakAllocがオンです"), TEXT("確認"), MB_OK);
		_CrtSetBreakAlloc(43657);											//指定した番号の領域を取得した瞬間にブレーク(リーク特定用)
	}
	//glfwの初期化
	if (!glfwInit()){
		return EXIT_FAILURE;
	}

	//ウィンドウリサイズ不可に設定する
	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);

	//サイズ640*480のウィンドウを作る
	if (!glfwOpenWindow(width, height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW)){
		glfwTerminate();
		return EXIT_FAILURE;
	}

	//ウィンドウタイトルを設定
	glfwSetWindowTitle("どろ忍");

	//「ビューポート変換」を指定
	//glViewport(X座標, Y座標,幅,高さ)
	//画面⇐したからの座標(X,Y)から
	//(幅*高さ)ピクセルの領域を描画領域とする
	glViewport(0, 0, width, height);

	//「投影行列」を操作対象にする
	glMatrixMode(GL_PROJECTION);

	glfwSwapInterval(1);

#if defined(_MSC_VER)
	//GLEWを初期化
	if (glewInit() != GLEW_OK){
		glfwTerminate();
		return EXIT_FAILURE;
	}
#endif

	Game game(width, height);
	if (!game.Run()){
		glfwTerminate();
		return EXIT_FAILURE;
	}


	glfwTerminate();

	return EXIT_SUCCESS;
}