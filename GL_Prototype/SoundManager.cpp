// TIPS:VisualStudioでM_PIを使えるようにする
#define _USE_MATH_DEFINES


#include "SoundManager.h"
#include <assert.h>
#include <array>
#include <AL\alut.h>
#define R_SOUND ((std::string)"..\\Resource\\Sound\\")
SoundManager::SoundManager()
{
	alutInit(NULL,NULL);
}


SoundManager::~SoundManager()
{
	for (unsigned int i = 0; i < data.size(); i++){
		delete data[i];
	}
	if (buf.size() > 0)
		alDeleteBuffers(buf.size(), &buf[0]);
	if (src.size() > 0)
		alDeleteSources(src.size(), &src[0]);
	alutExit();
}



int SoundManager::SetSound(std::string fname)
{
	int num;
	int wavChannel, wavBit, wavSize, wavFreq;
	FILE* FP;
	file.push_back(R_SOUND + fname);
	num = file.size() - 1;
	fopen_s(&FP,file[num].c_str(), "rb");
	if (ReadHeaderWav(FP, &wavChannel, &wavBit, &wavSize, &wavFreq)){
		assert(0);
	}
	data.push_back(new unsigned char[wavSize]);
	fread(data[num], wavSize, 1, FP);
	buf.push_back(0);
	alGenBuffers(1,&buf[num]);
	if (wavChannel == 1){
		if (wavBit == 8){
			alBufferData(buf[num], AL_FORMAT_MONO8, data[num], wavSize, wavFreq);
		}
		else{
			alBufferData(buf[num], AL_FORMAT_MONO16, data[num], wavSize, wavFreq);
		}
	}
	else{
		if (wavBit == 8){
			alBufferData(buf[num], AL_FORMAT_STEREO8, data[num], wavSize, wavFreq);
		}
		else{
			alBufferData(buf[num], AL_FORMAT_STEREO16, data[num], wavSize, wavFreq);
		}
	}
	src.push_back(0);
	alGenSources(1, &src[num]);

	return num;
}
void SoundManager::CallSound(int num)
{
	//ソースに再生するバッファを割り当てる
	alSourcei(src[num], AL_BUFFER, buf[num]);

	//ソースの再生開始
	alSourcePlay(src[num]);

	//ソースへのバッファ割り当て解除
	alSourcei(src[num], AL_BUFFER, 0);

}

int SoundManager::ReadHeaderWav(FILE* fp, int *channel, int* bit, int *size, int* freq){
	short res16;
	int res32;
	int dataSize, chunkSize;
	short channelCnt, bitParSample, blockSize;
	int samplingRate, byteParSec;

	int dataPos;
	int flag = 0;

	fread(&res32, 4, 1, fp);
	if (res32 != 0x46464952){	//"RIFF"
		return 1;	//error 1
	}

	//データサイズ = ファイルサイズ - 8 byte の取得
	fread(&dataSize, 4, 1, fp);

	//WAVEヘッダーの読み
	fread(&res32, 4, 1, fp);
	if (res32 != 0x45564157){	//"WAVE"
		return 2;	//error 2
	}

	while (flag != 3){
		//チャンクの読み
		fread(&res32, 4, 1, fp);
		fread(&chunkSize, 4, 1, fp);

		switch (res32){
		case 0x20746d66:	//"fmt "
			//format 読み込み
			//PCM種類の取得
			fread(&res16, 2, 1, fp);
			if (res16 != 1){
				//非対応フォーマット
				return 4;
			}
			//モノラル(1)orステレオ(2)
			fread(&channelCnt, 2, 1, fp);
			if (res16 > 2){
				//チャンネル数間違い
				return 5;
			}
			//サンプリングレート
			fread(&samplingRate, 4, 1, fp);
			//データ速度(byte/sec)=サンプリングレート*ブロックサイズ
			fread(&byteParSec, 4, 1, fp);
			//ブロックサイズ(byte/sample)=チャンネル数*サンプルあたりのバイト数
			fread(&blockSize, 2, 1, fp);
			//サンプルあたりのbit数(bit/sample)：8 or 16
			fread(&bitParSample, 2, 1, fp);

			*channel = (int)channelCnt;
			*bit = (int)bitParSample;
			*freq = samplingRate;

			flag += 1;

			break;
		case  0x61746164:	//"data"

			*size = chunkSize;

			dataPos = ftell(fp);

			flag += 2;
			break;
		}

	}

	//頭出し("fmt "が"data"より後にあった場合のみ動く)
	if (dataPos != ftell(fp)){
		fseek(fp, dataPos, SEEK_SET);
	}

	return 0;
}
