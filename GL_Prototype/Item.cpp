#include "Item.h"
#include "StageItem.h"

Item::Item(){};


Item::Item(int ID,ItemType type)
{
	itemID = ID;
	itemType = type;
}

Item::Item(const Item& i){
	itemID = i.itemID;
	itemType = i.itemType;
	
}

Item& Item::operator=(const Item& i){
	itemID = i.itemID;
	itemType = i.itemType;
	return(*this);
}

