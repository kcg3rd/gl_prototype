#pragma once
#include "Item.h"
#include "Define.h"
class Vertex;
class Draw;
class ResourceController;

//ステージアイテムクラス(Itemクラスを継承)
class StageItem :
	public Item
{
public:
	StageItem();
	//画像サイズから自動で作成
	StageItem(int ID, ItemType type, int x, int y,int param = -1);
	//StageItem(int ID,int Type, int x, int y, int param = -1);
	//幅高さを指定して作成
	//StageItem(int ID, ItemType type, int x, int y,int xSize, int ySize, int param = -1);
	StageItem(int ID, ObjectType Type, int x, int y, int w, int h, int param = -1);

	StageItem(const StageItem&);
	StageItem& operator=(const StageItem&);
	~StageItem();
	
	//ステージアイテムと重なってるかチェック
	bool CheckPosition(int sx, int sy, int swidth, int sheight);

	bool Action();
	bool Update();
	Item GetItem();
	inline void SetNuber(int ID){
		itemID = ID;
	}
private:
	Vertex v;
	Draw* dr;
	ResourceController* RC;
	ObjectType objecttype;
	int getFlag;
	int x;
	int y;
	int width;
	int height;
	int itemparam1;		//パラメータ
	bool onStage;
};

