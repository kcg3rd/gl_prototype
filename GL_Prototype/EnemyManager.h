#ifndef _ENEMYMANAGER_H_
#define _ENEMYMANAGER_H_

#include <list>
#include "Enemy.h"

class Stage;

typedef std::vector<Enemy*> EnemyVector;		//Enemyを格納する配列
typedef EnemyVector::iterator EnemyVectorItr;	//EnemyVector用のイテレータ

//複数のEnemyを管理するクラス
class EnemyManager
{
public:
	//EnemyManagerクラスを作成する
	//[maxEnemyTypeCount: 管理するEnemyTypeの数]
	explicit EnemyManager(int maxEnemyTypeCount);
	~EnemyManager(){ ClearAllEnemyList(); }

	//登録されているEnemyを初期化する
	bool Initialize(Stage* stage);

	//登録されているEnemyを更新する
	bool Update(Stage* stage, bool inStage);

	//EnemyListに新たなEnemyを作成する
	//[戻り値: newされたEnemyクラスへのポインタ]
	Enemy* CreateNewEnemy(EnemyType type);

	//登録されているType別のEnemyの数を取得する
	//[existOnly: 生存しているEnemyのみカウントする]
	int GetEnemyCount(EnemyType type, bool existOnly = false);

	//登録されている全TypeのEnemyの数を取得する
	//[existOnly: 生存しているEnemyのみカウントする]
	int GetAllEnemyCount(bool existOnly = false);

	//登録されているEnemyにアクセスする
	//[戻り値: 登録されているEnemyクラスへのポインタ(範囲外指定時: nullptr)]
	Enemy* GetEnemy(EnemyType type, unsigned int index);

	//リストに登録されているEnemy共を破棄する
	//[type: 対象となるEnemyのタイプ]
	void ClearEnemyList(EnemyType type);

	//全リストに登録されているEnemy共を破棄する
	void ClearAllEnemyList();

private:
	int maxEnemyTypeCount;	//管理するEnemyTypeの数
	std::vector<EnemyVector> enemyList;	//管理するEnemy配列のリスト
	std::vector<EnemyVector>::iterator listItr;	//enemyList用のイテレータ

	EnemyVectorItr enemyItr;		//Enemyを指すイテレータ

	void DestroyEnemyManager();

};
#endif