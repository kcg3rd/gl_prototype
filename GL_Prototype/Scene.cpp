#include "Scene.h"
#include "Game.h"
#include "ResourceController.h"


Scene::Scene()
{
	RC = ResourceController::GetResourceController();
}
Scene::~Scene()
{
	for (unsigned int i = 0; i < menuList.size(); i++){
		delete menuList[i];
	}
	delete loading;
}

void Scene::StageSetup(){
	int num = RC->stage_param.stage_limits;
	if (num <= 0)
		num = 12;
	stageList.resize(num);
	Stage* s;
	for (int i = 0; i < num; i++){
		s = new Stage(i, 0);
		stageList[i] = *s;
		delete s;
	}

	menuList.resize(3);

}

bool Scene::Initialize(Game* g){
	try{
		StageSetup();
		game = g;
		loading = new Vertex(0, 0, &RC->menu.loading);
		menuList[0] = new Title;
		menuList[1] = new GameOver;
		menuList[2] = new GameClear;
		stage_num = -1;
		menu_num = 1;
		if (ResourceController::GetResourceController()->parameter.skiptitle >= 0){
			menu_num = 0;
			stage_num = ResourceController::GetResourceController()->parameter.skiptitle;
			now = &stageList[stage_num];
			AutoMove();

		}
		else{
			menu_num = 1;
			stage_num = 0;
			now = &stageList[stage_num];
		}
		
		for (unsigned int i = 0; i < menuList.size(); i++){
			menuList[i]->Initialize();
		}
		if (stage_num >= 0 &&  menu_num == 0)
			if (!now->Initialize())throw;
		keyhr = SoundManager::GetSoundManager()->SetSound("key.wav");
		clearhr = SoundManager::GetSoundManager()->SetSound("clear.wav");
	}
	catch (...)
	{
		return false;
	}

	return true;
}
int Scene::UpDate(){
	try{
		if (0 < menu_num){
			if (menu_num == 3){
				stageList.clear();
				StageSetup();
			}
			//メニュー画面を更新
			int menuresult = menuList[menu_num - 1]->Update();
			if (1 <= menuresult){
				if (menu_num == 1){
					StageChange(0);
					menu_num = 0;
				}
				else{
					menu_num = 1;
				}
			}
			else if (menuresult == -1)return false;	//正常終了
			else if (menuresult <= -2)throw;
		}
		else{
			//現在のステージを更新
			if (!now->UpDate())throw;
		}
		if (changeflag){
			AutoMove();
		}

	}
	catch (...){
		assert(0);	//シーンのアップデートに失敗
		return -1;
	}
	return true;
}
Stage* Scene::Now(){
	return now;
}
bool Scene::StageChange(int stageNum){
	before_stage_num = stage_num;
	if ((unsigned int)stageNum < stageList.size()){
		if (stageNum < 1 && gameClear){
			menu_num = 3;
			gameClear = false;	//フラグをもとにもどす
		}
		else{
			changeflag = true;
			stage_num = stageNum;
		}
	}
	else{
		assert(0);	//無効なステージ番号
		return false;
	}
	return true;
}
bool Scene::MenuChange(unsigned int num){
	if (0 <= num && num < menuList.size()){
		menu_num = num;
		return true;
	}
	assert(0);	//無効なメニュー番号
	return false;
}

bool Scene::inStage(){
	if (menu_num != 0){
		return false;
	}
	else{
		return true;
	}
}

bool Scene::ClearFlag(){
	return gameClear;
}

bool Scene::setClear(bool b){
	gameClear = b;
	return true;
}

bool Scene::AutoMove(){
	Draw::GetDraw()->QuickDraw(loading);
	if (now != NULL){
 		if (now->InitializeCheck()){
			now->Close();
		}
	}
	now = &stageList[stage_num];
	if (!now->InitializeCheck()){
		now->Initialize();
	}
	if (before_stage_num <= stage_num)
		game->CharacterSetPostion(0);
	else if (before_stage_num > stage_num)
		game->CharacterSetPostion(1);
	changeflag = false;
	Draw::GetDraw()->Clear();
	return true;
} 