﻿#define GLFW_DLL

#include "Game.h"
#include "Screen.h"
#include "Draw.h"
#include "Vertex.h"
#include "Scene.h"
#include "Stage.h"
#include "ResourceController.h"
#include "Light.h"

#include "Character.h"
#include "Player.h"
#include "Enemy.h"
#include "EnemyManager.h"

#ifndef GL_INITIALIZE_
#define GL_INITIALIZE_
#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/GLU.h>

#include<fstream>
#include<vector>
#include<assert.h>
#endif

#include "Collision.h"

Game::Game()
{
}

Game::Game(int w = 640, int h = 480)
{
	soundmanager = SoundManager::GetSoundManager();
	screen = Screen::GetScreen();
	screen->SetScreen(w, h);
	scene = Scene::GetScene();
	draw = Draw::GetDraw();
	RC = ResourceController::GetResourceController();
}

Game::~Game()
{
	delete player;
	delete enemyManager;
	delete light;
}
bool Game::Run(){
	bool result = true;

	if (!InitializeGame())
	{
		return false;
	}
	//ゲームループ
	while (glfwGetWindowParam(GLFW_OPENED))
	{
		try{
			glClear(GL_COLOR_BUFFER_BIT);

			if (player->GetGameOverFlag())
			{
				player->SetGameOverFlag(false);
				scene->MenuChange(2);	//playerのゲームオーバーフラグが設定されていればゲームオーバー画面に遷移する
			}

			if (glfwGetKey(GLFW_KEY_ESC)){ break; }

			if (!UpdateGame()) return 0;

			if (!DrawGame())throw;

			glfwSwapBuffers();
		}
		catch (...){
			return false;
		}
	}
	return true;
}

bool Game::InitializeGame()
{
	try{
		if (!RC->Initialize())throw;

		//Playerを作成(Stageを必要としない範囲の初期化をする)
		player = new Player(0.0f, 0.0f, 50.0f, 80.0f, 0.0f, 0.0f, Direction::Right, 1);
		player->LoadResourceControllerValue();
		player->CreatePlayer();
		player->Initialize();

		//EnemyManagerを作成
		enemyManager = new EnemyManager(5);	//Enemy1,2,3, ,Boss

		light = new Light;
		if (!scene->Initialize(this))throw;
		//if (!light->Initialize(player)) throw;
	}
	catch (...){
		assert(0);	//Initializeに失敗
		return false;
	}
	return true;
}

bool Game::UpdateGame()
{
	if (scene->inStage()){
		player->Update(scene->Now(), scene->inStage());

		enemyManager->Update(scene->Now(), scene->inStage());
	}
	if (!scene->UpDate()) return 0;
	if (scene->inStage()){
		if (!light->Update())throw;
	}
	return true;
}

bool Game::DrawGame()
{
	bool result = true;


	screen->Scroll();


	if (!draw->UpDate())throw;

	return result;
}

bool Game::ResetGame(){
	Game::~Game();
	return InitializeGame();
}

bool Game::CharacterSetPostion(bool reverse){

	Position pos;

	//Playerを再構築
	pos = *scene->Now()->GetStartPos(reverse);
	player->SetPosition(pos.X, pos.Y);

	player->LoadResourceControllerValue();
	player->Initialize();
	player->InitCamera(scene->Now());	//カメラを初期化する

	enemyManager->ClearAllEnemyList();		//Enemyの実体を破棄する

	//Enemyを再構築
	Enemy* e;
	float w = 50.0f, h = 80.0f, boss_w = 100.0f, boss_h = 160.0f;
	std::vector<Position>::const_iterator posCItr;	//ステージのキャラクター座標へのイテレーター
	//Enemy1の初期化
	if (scene->Now()->Enemy0Pos->data() != NULL){
		for (posCItr = scene->Now()->Enemy0Pos->begin(); posCItr != scene->Now()->Enemy0Pos->end(); posCItr++){
			pos = *posCItr;
			e = enemyManager->CreateNewEnemy(EnemyType::Type1);
			e->SetParameter(pos.X, pos.Y, w, h, 2.0f, 100, Direction::Left, 1);
			e->SetProperty(EnemyType::Type1, EnemyMoveMode::TestMode);
			e->LoadResourceControllerValue();
			e->CreateEnemy(player);
		}
	}
	//Enemy2の初期化
	if (scene->Now()->Enemy1Pos != NULL){
		for (posCItr = scene->Now()->Enemy1Pos->begin(); posCItr != scene->Now()->Enemy1Pos->end(); posCItr++){
			pos = *posCItr;
			e = enemyManager->CreateNewEnemy(EnemyType::Type2);
			e->SetParameter(pos.X, pos.Y, w, h, 2.0f, 100, Direction::Left, 1);
			e->SetProperty(EnemyType::Type2, EnemyMoveMode::TestMode);
			e->LoadResourceControllerValue();
			e->CreateEnemy(player);

			//ボステスト用
			//e = enemyManager->CreateNewEnemy(EnemyType::Type5);
			//e->SetParameter(pos.X, pos.Y, boss_w, boss_h, 2.0f, 100, Direction::Left, 1);
			//e->SetProperty(EnemyType::Type5, EnemyMoveMode::TestMode);
			//e->LoadResourceControllerValue();
			//e->CreateEnemy(player);
		}
	}
	//Enemy3の初期化
	if (scene->Now()->Enemy2Pos->data() != NULL){
		for (posCItr = scene->Now()->Enemy2Pos->begin(); posCItr != scene->Now()->Enemy2Pos->end(); posCItr++){
			pos = *posCItr;
			e = enemyManager->CreateNewEnemy(EnemyType::Type3);
			e->SetParameter(pos.X, pos.Y, w, h, 2.0f, 100, Direction::Left, 1);
			e->SetProperty(EnemyType::Type3, EnemyMoveMode::TestMode);
			e->LoadResourceControllerValue();
			e->CreateEnemy(player);
		}
	}

	//EnemyPos3をボス用に使う
	if (scene->Now()->Enemy3Pos->data() != NULL){
		for (posCItr = scene->Now()->Enemy3Pos->begin(); posCItr != scene->Now()->Enemy3Pos->end(); posCItr++){
			pos = *posCItr;
			e = enemyManager->CreateNewEnemy(EnemyType::Type5);
			e->SetParameter(pos.X, pos.Y, boss_w, boss_h, 2.0f, 100, Direction::Left, 1);
			e->SetProperty(EnemyType::Type5, EnemyMoveMode::TestMode);
			e->LoadResourceControllerValue();
			e->CreateEnemy(player);
		}
	}

	enemyManager->Initialize(scene->Now());
	//int a = enemyManager->GetAllEnemyCount();	//Enemyの数確認用
	if (!light->Initialize(player)) throw;
	return true;
}

bool Game::Serialize(){
	return true;
}