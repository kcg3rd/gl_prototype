//プレイヤークラス
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Character.h"
#include "HookShot.h"
#include "SoundManager.h"

//Playerの操作キー定義
#define MOVE_KEY_LEFT       'A'				//左移動キー
#define MOVE_KEY_RIGHT      'D'				//右移動キー
#define MOVE_KEY_UP         'W'				//ハシゴ登るキー
#define MOVE_KEY_DOWN       'S'				//ハシゴ降りるキー
#define ACTION_KEY_JUMP     GLFW_KEY_SPACE	//ジャンプキー
#define ACTION_KEY_SHINOBI  'S'				//忍びキー
#define ACTION_KEY_DASH     GLFW_KEY_LSHIFT	//ダッシュキー

//#define ITEM_KEY_FUROSHIKI  GLFW_KEY_LSHIFT	//フロシキ使用キー
#define ITEM_KEY_FUROSHIKI  GLFW_MOUSE_BUTTON_MIDDLE

#define ITEM_KEY_WEAPON     'E'				//武器使用キー

#define STAGE_CHECK         'C'				//ステージチェックキー

class Animation;

class Stage;
class Screen;
class Item;
class Weapon;

class Player : public Character
{
	enum State{ Idol, Walk, Shinobi, Jump, Ladder, Furoshiki, Attack, Found, Damage };	//キャラクターの状態

	//操作キーの状態
	struct PlayerKeyState {
		bool left = false;		//左移動キー
		bool right = false;		//右移動キー
		bool up = false;		//上昇キー
		bool down = false;		//下降キー
		bool jump = false;		//ジャンプキー
		bool shinobi = false;	//忍びキー
		bool dash = false;		//ダッシュキー
		bool furoshiki = false;	//フロシキ使用キー
		bool attack = false;	//攻撃キー
		bool checkStage = false;//ステージを調べるキー
	};

public:
	Player() : Character() { }
	Player(
		float startPosX,
		float startPosY,
		float width,
		float height,
		float speed,
		float jumpForce,
		Direction startDir,
		int drawRank,
		int HP = 100,
		bool useGravity = true
		);
	~Player(){ DestroyPlayer(); }

	//Playerデータを作成する
	//※このメソッドはPlayer::Initialize()より先に呼び出す必要がある
	void CreatePlayer();

	//ResourceControllerのパラメータを取得する
	void LoadResourceControllerValue();

	//プレイヤーの初期化を行う
	//※このメソッドはPlayer::CreatePlayer()の後に実行する
	virtual void Initialize();

	//プレイヤーを更新する
	virtual void Update(Stage* stage, bool inStage);

	//カメラを初期化する
	void InitCamera(Stage* stage);

	//スクロール制御
	void ScrollScreen();

	//隠れている割合を取得する (0.0→隠れていない, 1→100%隠れている)
	inline float GetHideRate() { return hideRate; }

	//移動中フラグを取得する
	inline bool GetMovingFlag() { return moving; }

	//shinobi(しゃがみ)中フラグを取得する
	inline bool GetShinobiFlag() { return stealthMoving; }

	//アイテム感知用のY座標を取得する
	inline float GetSearchPosY() { return searchPosY; }

	//残金を取得する
	inline int GetWallet() { return wallet; }

	//Weaponクラスへのポインターを取得する
	Weapon* GetWeapon(ItemType weaponName);

	//アイテムリストゲッター

	//ゲームオーバーフラグを取得する
	inline bool GetGameOverFlag() { return this->gameOverFlag; }

	//Playerを捕捉しているEnemyの数を取得する
	inline int GetFoundCount() { return findCount; }


	//残金を設定する
	inline void SetWallet(int money) { wallet = money; }

	//装備状態セッター
	//アイテムリストセッター

	//ゲームオーバーフラグを設定する
	inline void SetGameOverFlag(bool gameOver = true){ this->gameOverFlag = gameOver && !debugMode; }

	//Playerの発見フラグを設定する
	inline void SetFoundFlag(){ findCount++; }

	//デバッグモード(GameOverにならない)を設定する
	inline void SetDebugMode(bool flag = true) { debugMode = flag; }

	//Enemyと接触したことを伝えるメソッド
	inline void SetDamage(int damage, Direction dir) {
		hp -= damage;
		enemyDir = dir;
		damageFlag = true;
	}

private:
	bool debugMode;
	HookShot hookshot;
	Screen* screen;
	bool callwalk;	//構造体作るべき
	int walkhr;
	bool callfuroshiki;
	int furoshikihr;
	bool calldamage;
	int damagehr;


	int     stageWidth;		//ステージの幅
	int     stageHeight;	//ステージの高さ

	Animation* idolAnim;		//待機アニメーション
	Animation* normalMoveAnim;	//通常移動アニメーション
	Animation* slowMoveAnim;	//忍び移動アニメーション
	Animation* fastMoveAnim;	//高速移動アニメーション
	Animation* ladderAnim;		//ハシゴ移動アニメーション
	Animation* attackAnim;		//攻撃のアニメーション
	Animation* foundAnim;		//発見時のアニメーション
	Vertex*    jumpAnim;		//ジャンプ絵
	State animationState;	//アニメーションの状態
	//animtionStateを選択するメソッド

	std::vector<Vertex*> UIImages;
	float uiOffsetX, uiOffsetY;
	float uiDelta;

	PlayerKeyState keyState;	//操作キーの入力状態
	PlayerKeyState prevKeyState;
	void UpdateKeyState();		//キーの入力状態を更新する
	float			  moveSpeed;
	float             searchPosY;		//感知用のY座標
	float             fastSpeed;		//ダッシュ速度（現在）
	float			  fastMinSpeed;		//ダッシュ初期値
	float			  fastMaxSpeed;		//ダッシュ上限値
	float			  fastAdd;			//ダッシュ加算の倍率
	float			  fastSub;			//ダッシュ減算の倍率
	float             slowSpeed;		//忍び移動速度
	bool              moving;			//移動中フラグ
	bool              stealthMoving;	//忍び足フラグ
	float             hideRate;			//隠れている割合(0.0 ~ 1.0の範囲)
	int               wallet;			//財布
	std::vector<Item> equipment;		//装備品
	std::vector<Item> itemList;			//所持アイテムリスト

	void Move(Stage* stage);		//移動メソッド
	void StageAction(Stage* stage);

	Animation* furoshikiAnim;	//フロシキ動作アニメーション
	bool       usingFuroshiki;	//フロシキ使用中フラグ

	Weapon*    kunai;			//クナイ
	bool       usingKunai;		//クナイ使用中フラグ
	Weapon*    makibishi;		//マキビシ
	bool       usingMakibishi;	//マキビシ使用中フラグ

	bool       attacking;		//攻撃モーション中フラグ
	int        weaponKeyCounter;		//武器使用キーの入力フレームカウンタ
	int        weaponSwitchFrameNum;	//使用する武器を切り替えるしきい値
	int        weaponInterval;			//武器の使用間隔
	int        weaponFrameCounter;		//武器用フレームカウンタ
	void       UseItem(Stage* stage);	//アイテム使用メソッド

	bool gameClear;		// ゲームクリアフラグ
	bool gameOverFlag;	// ゲームオーバーフラグ
	void LifeCheck();	//生存確認メソッド

	int       findCount;		//Playerを捕捉しているEnemyの数
	int       prevFindCount;	//1フレーム前のfindCount
	bool      damageFlag;		//ダメージフラグ
	Direction enemyDir;			//接触したEnemyの方向

	void SetAnimationGraphic();	//アニメーショングラフィックを設定する
	void UpdateAnimation();		//アニメーションの更新を行う
	void PrintUI();

	void DestroyPlayer();

};
#endif