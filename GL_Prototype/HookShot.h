#ifndef _HOOKSHOT_H_
#define _HOOKSHOT_H_

#include "MouseControll.h"

#include "Scene.h"
#include "Screen.h"
#include "Vertex.h"
#include "Draw.h"

class Player;

class HookShot :
	protected MouseControll
{
public:
	HookShot();
	~HookShot();
	bool Initialize();
	bool Update(Player*,float& x, float& y);
private:
	Vertex v;
	Scene* scene;
	ResourceController* rc;
	Draw* draw;
	int se_movehr;
	bool useFlag;
	bool moveFlag;
	Position_i startPos;
	Position_i toolPos;
	Position_i targetPos;
	Position_i notPos;
	int speed;
};


#endif