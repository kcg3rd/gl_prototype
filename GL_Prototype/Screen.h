#ifndef _SCREEN_H_
#define _SCREEN_H_
//画面情報を保持するシングルトンクラス
class Screen
{
public:
	void SetScreen(int,int);

	//シングルトンクラス
	static Screen* GetScreen(){
		static Screen screen;
	return &screen;
	}
	//幅を取得
	inline int Width(){
		return width;
	}
	//高さを取得
	inline int Height(){
		return height;
	}
	void Scroll();
	void Zoom(int);
	//X座標のスクロール
	float x;
	//Y座標のスクロール
	float y;
private:
	Screen();
	~Screen();
	int zoom;
	float scale;
	float top;
	float left;
	float right;
	float bottom;
	int scroll;
	int width;
	int height;
};

#endif