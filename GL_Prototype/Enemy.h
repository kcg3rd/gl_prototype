#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Character.h"

#include "ResourceController.h"

class Animation;
class Stage;
class Player;

//
class Enemy : public Character
{
protected:
	enum AnimationState{Stop, Walk, Dash, Found, Jump, Ladder, Search, Damage, Action}; //アニメーションの状態
	
	//Enemyの行動状態
	enum ActionState{
		Normal,			//通常
		ChasePlayer,	//Player追跡中
		LostPlayer		//Playerを見失った
	};

public:
	Enemy() : Character() { }
	Enemy(
		float startPosX,
		float startPosY,
		float width,
		float height,
		float speed,
		float jumpForce,
		Direction startDir,
		int drawRank,
		int HP = 100,
		bool useGravity = true
		);
	~Enemy() { DestroyEnemy(); };

	//Enemyデータを作成する
	//※このメソッドはEnemy::Initialize()より先に呼び出す必要がある
	void CreateEnemy(Player* player);

	//ResourceControllerのパラメータを取得する
	//※このメソッドはEnemy::SetType()の後に呼び出す
	void LoadResourceControllerValue();

	void Initialize(Stage* stage);
	void Update(Stage* stage, bool inStage);

	//Enemyの種類を取得する
	inline EnemyType GetEnemyType(){ return enemyType; }

	//Enemyの種類を設定する
	//※このメソッドはEnemy::CreateEnemy()より先に呼び出す必要がある
	inline void SetType(EnemyType type){ enemyType = type; }

	//行動タイプを設定する
	void SetMoveMode(EnemyMoveMode = NotMove);

	//待機時間(フレーム数)を設定する
	//※SetMoveMode()より後に呼び出す
	inline void SetWaitFrameNum(int frameLength) { waitFrameNum = frameLength; }

	//移動時間(フレーム数)を設定する
	//※SetMoveMode()より後に呼び出す
	inline void SetMoveFrameNum(int frameLength) { moveFrameNum = frameLength; }

	//Enemyの属性(種類＆行動パターン)を設定する
	//※このメソッドはEnemy::CreateEnemy()より先に呼び出す必要がある
	inline void SetProperty(EnemyType type, EnemyMoveMode moveMode)
	{
		SetType(type);
		SetMoveMode(moveMode);
	}

	//クラスの準備が出来ているか取得する
	bool IsReady();

protected:

	AnimationState animState, prevAnimState;		//Enemyの状態
	ActionState actionState, prevActionState;

	int power;	//攻撃力

	Player* playerInfo;	//Playerへのポインター
	bool findPlayer, prevFindplayer;	//Player発見フラグ
	bool findActionFlag;				//発見時の動作中フラグ

	int waitFrameNum = 0;		//待機している間隔
	int moveFrameNum = 0;		//移動している間隔
	int actionFrameNum = 0;		//アクションを行うフレーム数
	int searchDelay = 0;		//再サーチまでの間隔
	int moveFrameCounter;		//行動制御用フレームカウンタ

	void (Enemy::*MoveUpdate)(Stage* stage);	//位置を更新する
	void MoveMode0(Stage* stage);	//動かないやつ
	void MoveMode1(Stage* stage);	//往復するやつ
	void MoveMode2(Stage* stage);	//壁まで歩くやつ
	void MoveMode3(Stage* stage);	//Playerに向かって歩く
	void MoveMode4(Stage* stage);	//試作中モード
	void MoveMode5(Stage* stage);	//同上

	bool JumpCheck(Stage* stage);	//ジャンプをするか判断するメソッド

	void UpdateActionState();	//actionStateを切り替える
	
	EnemyType  enemyType = Type1;//Enemyの種類
	Animation* idolAnim;		//待機アニメーション
	Animation* normalMoveAnim;	//通常移動アニメーション
	Animation* fastMoveAnim;	//高速移動アニメーション
	Animation* ladderAnim;		//ハシゴ移動アニメーション
	Animation* foundAnim;		//Player追跡中のアニメーション
	Animation* searchAnim;		//Playerを探しているときのアニメーション
	Vertex* jumpAnim;			//ジャンプ絵

	std::vector<Vertex*> UIImages;
	float uiOffsetX, uiOffsetY;
	float uiDelta;

	float searchRangeX;	//X軸の索敵距離
	float searchRangeY;

	float chaseSpeed;	//Playerを追いかけるスピード

	void LookAtPlayer();	//Playerの方向を向く
	void SearchPlayer(Stage* stage);

	void SetAnimationGraphic();
	void UpdateAnimation();
	void PrintUI(UIImageName);

	void WeaponHitCheck();	//PlayerのWeaponとの接触判定
	Direction weaponDir;	//当たった時のWeaponの方向

	void DestroyEnemy();
};
#endif