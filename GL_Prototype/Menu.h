#ifndef _MENU_H_
#define _MENU_H_

#include<vector>
#define GLFW_DLL
#include <GL/glew.h>
#include <GL/glfw.h>

#include "ResourceController.h"
#include "Vertex.h"
#include "Draw.h"
#include "Screen.h"

class ResourceController;
class Vertex;
class Draw;
class Screen;


class Menu
{
public:
	Menu();
	virtual ~Menu();
	
	Menu(const Menu&);
	Menu& operator= (const Menu&);

	virtual bool Initialize();
	virtual int Update();
protected:
	unsigned int select;
	ResourceController* RC;
	Vertex* background;
	Vertex* Arrow;
	std::vector<Vertex> const_graphics;
	std::vector<Vertex> graphics;
	Draw* DR;
	Screen* SC;
	unsigned int resource;
	bool button1;

};

class Title :public Menu
{
public:
	Title();
	virtual~Title();
	bool Initialize();
	int Update();
private:
	bool w, s;
};

class GameOver :public Menu
{
public:
	GameOver();
	~GameOver();
	bool Initialize();
	int Update();
private:
	bool callover;
	int overhr;
};

class GameClear :public Menu
{
public:
	GameClear();
	~GameClear();
	bool Initialize();
	int Update();
private:
	bool callclear;
	int clearhr;
};


#endif