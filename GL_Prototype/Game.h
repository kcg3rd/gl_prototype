//ゲームを稼働させるクラス
#ifndef _GAME_H_
#define _GAME_H_

#include "Scene.h"
#include "SoundManager.h"

class Screen;
class Vertex;
class Draw;
class ResourceController;

class Character;
class Player;
class Enemy;
class EnemyManager;
class Light;

class Game{
public:
	Game();
	Game(int, int);
	~Game();

	//ゲーム開始メソッド
	bool Run();

	//キャラクターをスタート位置にセットする
	bool CharacterSetPostion(bool reverse);
	bool Serialize();
private:
	SoundManager* soundmanager;
	Screen* screen;
	Scene* scene;
	Draw* draw;
	ResourceController* RC;
	Light* light;
	Player* player;
	
	EnemyManager* enemyManager;

	bool InitializeGame();
	bool UpdateGame();
	bool DrawGame();
	bool ResetGame();
	void SetStatus();

	bool loop;
};
#endif
