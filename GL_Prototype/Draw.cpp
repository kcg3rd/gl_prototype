#define GLFW_DLL
#include "Draw.h"
#include <GL/glew.h>
#include <GL/glfw.h>
Draw::Draw()
{
	vertexrank.push_back(&vertexlist3);
	vertexrank.push_back(&vertexlist2);
	vertexrank.push_back(&vertexlist1);
	vertexrank.push_back(&vertexlist0);
}


Draw::~Draw()
{
}
bool Draw::VertexPushBack(Vertex* vtx, int drawrank){
	//�����ʂɕ�����
	switch (drawrank)
	{
	case(4) :
		vertexlist4.push_back(vtx);
	case(3) :
		vertexlist3.push_back(vtx);
		break;
	case(2) :
		vertexlist2.push_back(vtx);
		break;
	case(1) :
		vertexlist1.push_back(vtx);
		break;
	case(0) :
	default:
		vertexlist0.push_back(vtx);
		break;
	}
	return true;
}

bool Draw::UpDate(){
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnable(GL_BLEND);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	for (std::vector<std::vector<Vertex*>*>::iterator v = vertexrank.begin(); v != vertexrank.end(); v++){

		for (std::vector<Vertex*>::iterator i = (*v)->begin(); i != (*v)->end(); i++){
			VertexDraw(*i);
		}
		(*v)->clear();
	}
	glDisable(GL_COLOR_ARRAY);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	return true;
}

bool Draw::Clear(){
	vertexlist0.clear();
	vertexlist1.clear();
	vertexlist2.clear();
	vertexlist3.clear();
	vertexlist4.clear();
	return true;
}

void Draw::QuickDraw(Vertex* v){
	glEnable(GL_TEXTURE_2D);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnable(GL_BLEND);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	VertexDraw(v);
	glDisable(GL_COLOR_ARRAY);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glfwSwapBuffers();
}

void Draw::VertexDraw(Vertex* v){
	glVertexPointer(2, GL_FLOAT, 0, (v)->GetVertex());
	if (NULL != (v)->GetTextureID()){
		glBindTexture(GL_TEXTURE_2D, (v)->GetTextureID());
		glTexCoordPointer(2, GL_FLOAT, 0, (v)->GetTextureUV());
		glColorPointer(4, GL_FLOAT, 0, (v)->GetColor());
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDrawArrays(GL_QUADS, 0, 4);
	}
	else{
		glDisable(GL_TEXTURE_2D);
		glColorPointer(4, GL_FLOAT, 0, (v)->GetColor());
		glDrawArrays(GL_QUADS, 0, 4);
		glEnable(GL_TEXTURE_2D);
	}
}