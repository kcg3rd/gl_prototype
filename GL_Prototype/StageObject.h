#pragma once

#include "Define.h"
#include "Vertex.h"

class Vertex;
class Draw;
class ResourceController;
class StageObject
{
public:
	StageObject();
	StageObject(int ID, ObjectType Type, int x, int y, int w, int h, int param = -1);
	~StageObject();
	StageObject(const StageObject&);
	StageObject& operator=(const StageObject&);
	bool Action();
	bool Update();
	inline ObjectType GetObjectType(){
		return objecttype;
	}
	bool CheckPosition(int x, int y);
	Position& GetPosition();
private:
	Vertex v;
	Draw* dr;
	ResourceController* RC;
	ObjectType objecttype;
	int x;
	int y;
	Position pos;
	int width;
	int height;
	int param;		//パラメータ
};

