#include "Light.h"
#include "ResourceController.h"
#include "Screen.h"
#include "Player.h"

Light::Light()
	:MouseControll()
{
	beforewheel = 0;
}


Light::~Light()
{
	MouseControll::~MouseControll();
	delete on;
	delete off;
}

bool Light::Initialize(Player* player)
{
	MouseControll::Initialize();
	RC = ResourceController::GetResourceController();
	screen = Screen::GetScreen();
	p = player;
	windowsize.X = screen->Width();
	windowsize.Y = screen->Height();
	size.X = windowsize.X * 2;
	size.Y = windowsize.Y * 2;
	if (on != NULL)
		delete on;
	on = new Vertex(0, 0, size.X,size.Y,&RC->filter.on);
	if (off != NULL)
		delete off;
	off = new Vertex(0, 0, size.X, size.Y, &RC->filter.off);
	dr = Draw::GetDraw();
	return true;
}

bool Light::Update()
{
	MouseControll::Update();

	////ズーム
	//if (wheel != beforewheel){
	//	screen->Zoom((wheel-beforewheel) * (int)RC->parameter.zoomratio);
	//}
	
	//ウィンドウ外の位置補正
	if (mousePOS.X < 0){
		mousePOS.X = 0;
	}
	else if (mousePOS.X > windowsize.X){
		mousePOS.X = windowsize.X;
	}

	if (mousePOS.Y < 0){
		mousePOS.Y = 0;
	}
	else if (mousePOS.Y > windowsize.Y){
		mousePOS.Y = windowsize.Y;
	}
	light_mouse.X = (int)screen->x + mousePOS.X - windowsize.X;
	light_mouse.Y = (int)screen->y + mousePOS.Y - windowsize.Y;
	
	light_chara.X = (int)(p->GetCenterX() - windowsize.X);
	light_chara.Y = (int)(p->GetCenterY() - windowsize.Y);

	//フィルターを描画
	if (Rbutton){
		//点灯フィルター
		use_pos = &light_mouse;
		use_filter = on;
	}
	else{
		//消灯フィルター
		use_pos = &light_chara;
		use_filter = off;
	}
	dr->VertexPushBack(use_filter->SetDraw((float)use_pos->X,(float)use_pos->Y), 0);
	//beforewheel = wheel;
	light_mouse.X = (int)screen->x + mousePOS.X;
	light_mouse.Y = (int)screen->y + mousePOS.Y;
	InLight(&light_mouse);
	return true;
}

bool Light::InLight(Position_i* position){
		if ((use_pos->X + windowsize.X) - 50 * 3.14 < position->X &&
			(use_pos->X + windowsize.X) + 50 * 3.14 > position->X){
			if (use_pos->Y + windowsize.Y - 50 * 3.14 < position->Y &&
				use_pos->Y + windowsize.Y + 50 * 3.14 > position->Y){
				return true;
			}
		}
 		return false;
}