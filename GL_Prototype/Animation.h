#ifndef ___ANIMATION_H___
#define ___ANIMATION_H___
//アニメーションクラス

#include <GL\glew.h>

#include <vector>

class Vertex;

//アニメーションの再生モード
enum AnimPlayMode {
	NormalPlay,		//通常再生
	ReversePlay,	//逆再生
	TurnbackPlay,	//折り返し再生(正順→逆順)
	TurnbackPlayR	//折り返し再生(逆順→正順)
};

//アニメーションクラス
//実行順序
//1st:CreateAnimation()
//2nd:SetParameter()
//3rd:Initialize()
//4th:Update()
class Animation
{
	typedef unsigned int UINT;

	enum PlayDirection{	//アニメーションの再生方向
		normal = 1,		//順方向
		reverse = -1	//逆方向
	};

public:
	Animation(){};
	~Animation(){ DestroyAnimation(); };

	//アニメーションデータを作成する
	void CreateAnimation(std::vector<GLuint>* textures);

	//アニメーションを更新するフレーム数を設定する
	inline void SetUpdateFrameNum(int frameNum) { updateFrameNum = frameNum; }

	//アニメーションのプレイモードを設定する
	void SetPlayMode(AnimPlayMode playMode);

	//各パラメーターを設定する
	inline void SetParameter(int updateFrameNum = 4, AnimPlayMode playMode = NormalPlay)
	{
		this->updateFrameNum = updateFrameNum;
		this->playMode = playMode;
	}

	//アニメーションクラスを初期化する
	void Initialize();

	//アニメーションを更新する
	void Update();

	//現在のVertex番号を取得する
	inline int GetCurrentVertexNum(){ return currentAnimationNum; }

	//現在のVertexを取得する
	Vertex* GetCurrentVertex();

	//指定したインデックスのVertexを取得する
	Vertex* GetVertex(int index);

	//アニメーションデータを破棄する
	void DestroyAnimation();

	//アニメーションサイクルの終端であればtrueを返す
	bool EndOfAnimation();


private:
	std::vector<Vertex*> animation;	//アニメーショングラフィックを格納する配列

	AnimPlayMode playMode;		//再生モード
	PlayDirection playDir;		//再生方向

	int frameCounter;			//更新用フレームカウンタ
	int updateFrameNum;			//アニメーションを更新するフレーム数
	int currentAnimationNum;	//表示するグラフィック番号

};
#endif