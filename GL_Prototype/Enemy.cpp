#include "Enemy.h"

#include "Animation.h"
#include "ResourceController.h"
#include "Vertex.h"
#include "Draw.h"
#include "Collision.h"
#include "Player.h"
#include "Weapon.h"

#include "Stage.h"

Enemy::Enemy(float posX, float posY, float width, float height, float speed, float jumpF, Direction dir, int drRank, int hp, bool useGravity)
	:Character(posX, posY, width, height, speed, jumpF, dir, drRank, hp, useGravity)
{
}

void Enemy::CreateEnemy(Player* player)
{
	//Character::CreateCharacter(texture_id);
	dr = Draw::GetDraw();
	playerInfo = player;	//Playerへの参照を登録

	SetAnimationGraphic();
}

void Enemy::LoadResourceControllerValue()
{
	ResourceController* RC = ResourceController::GetResourceController();

	//EnemyTypeごとにパラメータを設定する
	switch (enemyType){
	case EnemyType::Type1:	//町人風(男性)の設定をする
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.enemy1_param.speed, -1.0f, 2.0f);
		chaseSpeed   = CHECK_VALUE(RC->parameter.enemy1_param.chaseSpeed, -1.0f, 4.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.enemy1_param.jump, -1.0f, 8.0f);
		hp           = CHECK_VALUE(RC->parameter.enemy1_param.hp, -1, 100);
		power        = CHECK_VALUE(RC->parameter.enemy1_param.power, -1, 1);
		waitFrameNum = CHECK_VALUE(RC->parameter.enemy1_param.waitFrameNum, -1, 80);
		moveFrameNum = CHECK_VALUE(RC->parameter.enemy1_param.moveFrameNum, -1, 240);
		searchRangeX = CHECK_VALUE(RC->parameter.enemy1_param.searchRangeX, -1.0f, 300.0f);
		searchRangeY = CHECK_VALUE(RC->parameter.enemy1_param.searchRangeY, -1.0f, 200.0f);

		break;

	case EnemyType::Type2:	//町人風(女性)の設定をする
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.enemy2_param.speed, -1.0f, 2.0f);
		chaseSpeed   = CHECK_VALUE(RC->parameter.enemy2_param.chaseSpeed, -1.0f, 4.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.enemy2_param.jump, -1.0f, 8.0f);
		hp           = CHECK_VALUE(RC->parameter.enemy2_param.hp, -1, 100);
		power        = CHECK_VALUE(RC->parameter.enemy2_param.power, -1, 1);
		waitFrameNum = CHECK_VALUE(RC->parameter.enemy2_param.waitFrameNum, -1, 80);
		moveFrameNum = CHECK_VALUE(RC->parameter.enemy2_param.moveFrameNum, -1, 240);
		searchRangeX = CHECK_VALUE(RC->parameter.enemy2_param.searchRangeX, -1.0f, 300.0f);
		searchRangeY = CHECK_VALUE(RC->parameter.enemy2_param.searchRangeY, -1.0f, 200.0f);

		break;

	case EnemyType::Type3:	//町奉行の設定をする
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.enemy3_param.speed, -1.0f, 2.0f);
		chaseSpeed   = CHECK_VALUE(RC->parameter.enemy3_param.chaseSpeed, -1.0f, 4.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.enemy3_param.jump, -1.0f, 8.0f);
		hp           = CHECK_VALUE(RC->parameter.enemy3_param.hp, -1, 100);
		power        = CHECK_VALUE(RC->parameter.enemy3_param.power, -1, 1);
		waitFrameNum = CHECK_VALUE(RC->parameter.enemy3_param.waitFrameNum, -1, 80);
		moveFrameNum = CHECK_VALUE(RC->parameter.enemy3_param.moveFrameNum, -1, 240);
		searchRangeX = CHECK_VALUE(RC->parameter.enemy3_param.searchRangeX, -1.0f, 300.0f);
		searchRangeY = CHECK_VALUE(RC->parameter.enemy3_param.searchRangeY, -1.0f, 200.0f);

		break;

	case EnemyType::Type4:	//(貴族風は使わない)
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.enemy4_param.speed, -1.0f, 2.0f);
		chaseSpeed   = CHECK_VALUE(RC->parameter.enemy4_param.chaseSpeed, -1.0f, 4.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.enemy4_param.jump, -1.0f, 8.0f);
		hp           = CHECK_VALUE(RC->parameter.enemy4_param.hp, -1, 100);
		power        = CHECK_VALUE(RC->parameter.enemy4_param.power, -1, 1);
		waitFrameNum = CHECK_VALUE(RC->parameter.enemy4_param.waitFrameNum, -1, 80);
		moveFrameNum = CHECK_VALUE(RC->parameter.enemy4_param.moveFrameNum, -1, 240);
		searchRangeX = CHECK_VALUE(RC->parameter.enemy4_param.searchRangeX, -1.0f, 300.0f);
		searchRangeY = CHECK_VALUE(RC->parameter.enemy4_param.searchRangeY, -1.0f, 200.0f);

		break;
		
		case EnemyType::Type5:
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.boss_param.speed, -1.0f, 2.0f);
		chaseSpeed   = CHECK_VALUE(RC->parameter.boss_param.chaseSpeed, -1.0f, 4.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.boss_param.jump, -1.0f, 8.0f);
		hp           = CHECK_VALUE(RC->parameter.boss_param.hp, -1, 100);
		power        = CHECK_VALUE(RC->parameter.boss_param.power, -1, 1);
		waitFrameNum = CHECK_VALUE(RC->parameter.boss_param.waitFrameNum, -1.0f, 0);
		moveFrameNum = CHECK_VALUE(RC->parameter.boss_param.moveFrameNum, -1.0f, 0);
		searchRangeX = CHECK_VALUE(RC->parameter.boss_param.searchRangeX, -1.0f, 0);
		searchRangeY = CHECK_VALUE(RC->parameter.boss_param.searchRangeY, -1.0f, 0);

		break;
		
		/*
		case EnemyType::Type6:
		//パラメータを取得
		speed        = CHECK_VALUE(RC->parameter.enemy6_param.speed, -1.0f, 2.0f);
		jumpForce    = CHECK_VALUE(RC->parameter.enemy6_param.jump, -1.0f, 8.0f);
		waitFrameNum = CHECK_VALUE(RC->parameter.enemy6_param.waitFrameNum, -1.0f, .0001f);
		moveFrameNum = CHECK_VALUE(RC->parameter.enemy6_param.moveFrameNum, -1.0f, .0001f);
		searchRangeX = CHECK_VALUE(RC->parameter.enemy6_param.searchRangeX, -1.0f, 0);
		searchRangeY = CHECK_VALUE(RC->parameter.enemy6_param.searchRangeY, -1.0f, 0);

		break;
		*/
	}
}

void Enemy::Initialize(Stage* /*stage*/)
{
	direction = startDir;
	moveFrameCounter = 0;
	actionState = ActionState::Normal;

	x = startPosX;
	y = startPosY;

	idolAnim->Initialize();
	normalMoveAnim->Initialize();
	fastMoveAnim->Initialize();
	ladderAnim->Initialize();
	foundAnim->Initialize();
	searchAnim->Initialize();
	//Init jumpAnim

	prevFindplayer = findPlayer = false;

	findActionFlag = false;

	uiOffsetX = 50.0f;
	uiOffsetY = 100.0f;
	uiDelta = 0.49f;
}

void Enemy::Update(Stage* stage, bool inStage)
{
	if (!inStage){ return; }

	prevX = x;
	prevY = y;
	prevAnimState = animState;

	//移動処理
	(this->*MoveUpdate)(stage);	//行動タイプごとに異なる
	y += fallSpeed;
	checkCollisionStage(stage);	//ステージとの接触処理

	UpdateAnimation();
}

//Enemyの行動タイプを設定する
void Enemy::SetMoveMode(EnemyMoveMode moveMode)
{
	switch (moveMode){
	case EnemyMoveMode::NotMove:
		MoveUpdate = &Enemy::MoveMode0;
		break;

	case EnemyMoveMode::BackAndForth:
		MoveUpdate = &Enemy::MoveMode1;
		waitFrameNum = 80;	//立ち止まる時間
		moveFrameNum = 300;	//歩き続ける時間
		break;

	case EnemyMoveMode::WallToWall:
		MoveUpdate = &Enemy::MoveMode2;
		waitFrameNum = 50;	//立ち止まる時間
		break;

	case EnemyMoveMode::GoToPlayer:
		MoveUpdate = &Enemy::MoveMode3;
		waitFrameNum = 40;	//立ち止まる時間
		break;

	case EnemyMoveMode::TestMode:
		//MoveUpdate = &Enemy::MoveMode4;
		MoveUpdate = &Enemy::MoveMode5;
		waitFrameNum = 80;	//立ち止まる時間
		moveFrameNum = 280;	//歩き続ける時間
		actionFrameNum = 80;

		searchRangeX = 300.0f;		//プレイヤーを認識する距離(X軸)を設定
		searchRangeY = 200.0f;		//プレイヤーを認識する距離(Y軸)を設定
		chaseSpeed = speed * 2.0f;	//プレイヤーを追いかける速度を設定
		break;
	}
}

bool Enemy::IsReady()
{
	bool result = true;

	//使用するテクスチャやクラスが設定されていなければ使用準備が出来ていない
	if (dr == nullptr) { result = false; }
	if (idolAnim == nullptr) { result = false; }
	if (normalMoveAnim == nullptr) { result = false; }
	if (fastMoveAnim == nullptr) { result = false; }
	if (ladderAnim == nullptr) { result = false; }
	//if (jumpAnim == nullptr) { result = false; }
	if (playerInfo == nullptr) { result = false; }
	if (UIImages.size() == 0) { result = false; }

	return result;
}

/*なにもしない*/
void Enemy::MoveMode0(Stage* /*stage*/)
{
	animState = AnimationState::Stop;
}

/*指定フレーム数待機後、反転してから指定フレーム数移動する*/
void Enemy::MoveMode1(Stage* stage)
{
	moveFrameCounter++;
	if (moveFrameCounter <= waitFrameNum){	//待機中
		animState = AnimationState::Search;
	}
	else if (moveFrameCounter - waitFrameNum <= moveFrameNum){	//移動中
		if (animState == AnimationState::Search){
			direction = (direction == Left ? Right : Left);	//前フレームが待機状態だったなら向きを変える
		}

		x += direction * speed;
		animState = AnimationState::Walk;
	}
	else{
		moveFrameCounter = -1;
	}
}

/*指定フレーム数待機後、反転してから壁に当たるまで移動する*/
void Enemy::MoveMode2(Stage* stage)
{
	moveFrameCounter++;
	if (moveFrameCounter <= waitFrameNum){	//待機中
		animState = Stop;
	}
	else if (!collisionWall){	//移動中
		if (animState == Stop){
			direction = (direction == Left ? Right : Left);	//前フレームが待機状態だったなら向きを変える
		}

		x += direction * speed;
		animState = Walk;
	}
	else{
		moveFrameCounter = -1;
	}
}

/*Playerのいる方向(水平のみ)へ移動する*/
void Enemy::MoveMode3(Stage* stage)
{
	moveFrameCounter++;
	if (moveFrameCounter <= waitFrameNum){	//待機中
		animState = Stop;
	}
	else if (x + width < playerInfo->GetCenterX() || x > playerInfo->GetCenterX()){	//playerとの距離が離れていれば
		LookAtPlayer();
		x += direction * speed;
		animState = Walk;

		if (collisionWall && onTheGround){
			animState = Stop;
		}
	}
	else{
		moveFrameCounter = -1;
	}
}

/* MoveMode1の動作をしながら、Playerを発見すれば追いかける(試作コード) */
void Enemy::MoveMode4(Stage* stage)
{
	moveFrameCounter++;
	if (!findPlayer && moveFrameCounter <= waitFrameNum){	//待機中
		animState = Stop;
	}
	else if (moveFrameCounter - waitFrameNum <= moveFrameNum){	//移動中
		if (animState == Stop){
			if (findPlayer){		//Playerを見失ったときは初期位置まで帰るようにしたい
				LookAtPlayer();
				if (onTheGround){
					fallSpeed = -8.0f;	//停止状態で発見したときは小ジャンプする
				}
			}
			else{
				direction = (direction == Left ? Right : Left);	//前フレームが待機状態だったなら向きを変える
			}
		}

		x += direction * (findPlayer ? chaseSpeed : speed);
		animState = Walk;
	}
	else{
		moveFrameCounter = -1;
	}
}

//Enemyの動きをActionStateで切り替える(理想)
void Enemy::MoveMode5(Stage* stage)
{
	prevActionState = actionState;

	WeaponHitCheck();

	//ダメージモーション中
	if (animState == AnimationState::Damage && !onTheGround){
		x += -weaponDir * speed;
		return;
	}

	if (searchDelay <= 0){
		//SearchPlayer_proto(stage);	//Playerの位置を確認する
		SearchPlayer(stage);		//Playerの位置を確認する
	}
	else{
		searchDelay--;
	}

	UpdateActionState();	//ActionStateを更新する

	Position playerPos = { playerInfo->GetCenterX(), playerInfo->GetCenterY() };

	switch (actionState) {
	case ActionState::Normal:

		//前のActionStateがLostPlayerだったとき
		if (prevActionState != ActionState::Normal){
			moveFrameCounter = -1;	//移動用フレームカウンタのリセット
		}

		//----------通常時の行動----------

		//MoveMode1()のパターン
		moveFrameCounter++;
		if (moveFrameCounter <= waitFrameNum){	//停止中
			animState = Stop;
		}
		else if (moveFrameCounter - waitFrameNum <= moveFrameNum){	//移動中
			if (animState == Stop){
				direction = (direction == Left ? Right : Left);
			}

			x += direction * speed;			//X座標を更新する
			animState = (collisionWall ? Stop : Walk);
		}
		else{
			moveFrameCounter = -1;
		}

		break;

	case ActionState::ChasePlayer:
		//前のActionStateがChasePlayer以外だったとき
		if (prevActionState != ActionState::ChasePlayer){
			if (onTheGround){ fallSpeed = -5.0f; }	//小ジャンプする
			findActionFlag = true;
			onTheGround = false;
		}

		//----------Player発見時(捕捉中)の行動----------

		//追跡動作--------------------
		LookAtPlayer();	//進行方向をPlayerのいる方向へ設定

		//障害物を避ける
		if (onTheGround && JumpCheck(stage) && animState != AnimationState::Stop){
			fallSpeed = -jumpForce;
		}

		if (!onTheGround){
			animState = AnimationState::Found;	//空中(発見時の小ジャンプ中)でのアニメーション
		}
		else if (x > playerPos.X || x + width < playerPos.X){
			if (!findActionFlag){
				x += direction * chaseSpeed;	//playerとの距離が離れていれば動く
			}
			else{
				//接地するまでfindActionFlagはtrue
				if (fallSpeed == 0.0f){
					findActionFlag = false;
				}
			}
			animState = (collisionWall ? AnimationState::Stop : AnimationState::Dash);	//追いかけるアニメーションに設定する
		}
		else {
			//縦方向でもPlayerと接触していたら
			if (y < playerPos.Y && y + height > playerPos.Y){
				//playerに接触したことを伝える
				Direction dir = (X_CENTER < playerPos.X ? Direction::Left : Direction::Right);
				playerInfo->SetDamage(power, dir);
				//playerInfo->SetGameOverFlag();		//Playerのゲームオーバーフラグを設定する
			}

			animState = Stop;
		}

		PrintUI(UIImageName::Exclamation);	//！マークを表示する

		//(ランダムor一定)間隔でジャンプする？

		break;

	case ActionState::LostPlayer:
		//前のActionStateがLostPlayer以外だったとき
		if (prevActionState != ActionState::LostPlayer){
			moveFrameCounter = -1;	//移動用フレームカウンタのリセット
			searchAnim->SetPlayMode(direction == Direction::Right ? AnimPlayMode::TurnbackPlay : AnimPlayMode::TurnbackPlayR);
			searchAnim->Initialize();
		}

		//----------Playerを見失ったときの行動----------
		
		//出現地点へ帰る動作--------------------

		moveFrameCounter++;
		//立ち止まる
		if (moveFrameCounter <= waitFrameNum){
			//waitFrame間立ち止まる
			animState = AnimationState::Stop;
		}
		//見渡すアクション
		else if (moveFrameCounter - waitFrameNum <= actionFrameNum){
			animState = AnimationState::Search;
			direction = (searchAnim->GetCurrentVertexNum() >= 4 ? Direction::Left : Direction::Right);
		}
		//出現位置へ移動する
		else{
			direction = (X_CENTER < startPosX ? Right : Left);
			x += direction * speed;
			animState = (collisionWall ? Stop : Walk);
		}

		PrintUI(UIImageName::Question);	//？マークを表示する

		break;
	}
}

//ジャンプをするか判断する
//[戻り値: ジャンプをするか否か]
bool Enemy::JumpCheck(Stage* stage)
{
	//中心Y座標から進行方向を見て壁があったとき、その壁がジャンプで越えられるか判断する
	//感知距離：左・右点＋自身の幅
	//許容できる壁の高さ：上点＋自身の高さ

	int sX = S_CAST_I(X_CENTER), sY = S_CAST_I(TOP);
	int upperY = S_CAST_I(y - height);	//ジャンプで越えられる高さ

	if (direction == Direction::Left){
		int centerWallDist = stage->GetCollision_Left(sX, sY);	//中心から進行方向の壁までの距離
		float leftX = LEFT - width;
		if (centerWallDist < leftX){	//(左点 + 自身の幅)内に壁がなければ
			return false;
		}

		int upperWallDist = stage->GetCollision_Left(sX, upperY);	//上点から左壁までの距離
		//centerWallDistよりupperWallDistが大きければジャンプで登れる
		if (centerWallDist > upperWallDist){
			return true;
		}
	}
	else{
		int centerWallDist = stage->GetCollision_Right(sX, sY);	//中心から進行方向の壁までの距離
		float rightX = RIGHT + width;
		if (centerWallDist > rightX){	//(右点 + 自身の幅)内に壁がなければ
			return false;
		}

		int upperWallDist = stage->GetCollision_Right(sX, upperY);	//上点から右壁までの距離
		//centerWallDistよりupperWallDistの方が遠ければジャンプで登れる
		if (centerWallDist < upperWallDist){
			return true;
		}
	}

	return false;	//ジャンプで越えられる可能性がなかった
}

//ActionStateを切り替えるメソッド
void Enemy::UpdateActionState()
{
	switch (actionState){
	case ActionState::Normal:
		if (findPlayer){	//Playerを捕捉したら
			actionState = ActionState::ChasePlayer;	//Chase状態へ遷移
		}
		break;

	case ActionState::ChasePlayer:
		if (!findPlayer){	//Playerを見失ったとき
			actionState = ActionState::LostPlayer;	//LostPlayer状態へ遷移
		}
		break;

	case ActionState::LostPlayer:
		if (findPlayer){	//Playerを再捕捉したとき
			actionState = ActionState::ChasePlayer;	//Chase状態へ遷移
		}
		//出現地点(X座標)まで戻ってきたか
		else if ((LEFT <= startPosX && RIGHT >= startPosX)
			/*&& (TOP <= startPosY && BOTTOM >= startPosY)*/){
			actionState = ActionState::Normal;	//Normal状態へ遷移
			break;
		}
		break;
	}
}

//Playerのいる方向へ向ける(水平方向限定)
void Enemy::LookAtPlayer()
{
	if (X_CENTER > playerInfo->GetCenterX()){
		direction = Left;
	}
	else{
		direction = Right;
	}
}

//Playerを感知するメソッド
void Enemy::SearchPlayer(Stage* stage)
{
	prevFindplayer = findPlayer;

	float findRate = 1.0f - playerInfo->GetHideRate();	//Playerを発見できる割合
	
	RectangleF searchArea;	//Playerを感知する範囲
	searchArea.left   = X_CENTER - (direction == Left ? searchRangeX : searchRangeX / 2.0f) * findRate;
	searchArea.right  = X_CENTER + (direction == Right ? searchRangeX : searchRangeX / 2.0f) * findRate;
	searchArea.top    = BOTTOM - (height + searchRangeY) * findRate;
	searchArea.bottom = BOTTOM + searchRangeY * (playerInfo->GetOnLadderFlag() ? 0.2f : findRate);

	RectangleF playerRect;	//Playerの矩形

	float playerX = playerInfo->GetCenterX(), playerY = playerInfo->GetSearchPosY();	//Playerの基準座標
	int eyeY = S_CAST_I(y + height / 80.0f);		//目線のY座標

	//壁による感知範囲の制限処理
	//キャラクターの「たぶん目の高さ」から見た左壁の座標X
	float leftWallX = S_CAST_F(stage->GetCollision_Left(S_CAST_I(X_CENTER), eyeY));
	if (searchArea.left < leftWallX){
		//searchArea.left = leftWallX;	//searchAreaのleftが左壁を越えていたら、leftを左壁までとする
	}
	//キャラクターの「たぶん目の高さ」から見た右壁の座標X
	float rightWallX = S_CAST_F(stage->GetCollision_Right(S_CAST_I(X_CENTER), eyeY));
	if (searchArea.right > rightWallX){
		//searchArea.right = rightWallX;	//searchAreaのrightが右壁を越えていたら、rightを右壁までとする
	}

	//Playerの中心座標がsearchArea内にあるか調べる
	if (Collision::RectToPoint(&searchArea, playerX, playerY)){
		
		//Enemyの向きにPlayerがいるか
		if (direction == Direction::Left && playerX < X_CENTER){
			findPlayer = true;
			if (playerX < leftWallX && !prevFindplayer){
				findPlayer = false;
			}
		}
		else if (direction == Direction::Right && playerX > X_CENTER){
			findPlayer = true;
			if (playerX > rightWallX && !prevFindplayer){
				findPlayer = false;
			}
		}
		//発見中にPlayerが背後に移動したとき
		else if (prevFindplayer){
			findPlayer = false;
			searchDelay = 40;	//指定時間、感知を停止する
		}
		//背後のPlayerがしゃがまずに移動した場合
		else if (/*playerInfo->GetOnGroundFlag() &&*/ !playerInfo->GetShinobiFlag() && playerInfo->GetMovingFlag() ){
			findPlayer = true;
		}
	}
	else{
		findPlayer = false;
	}

	if (animState == AnimationState::Damage){ findPlayer = prevFindplayer; }
	if (findPlayer){ playerInfo->SetFoundFlag(); }	//発見フラグがtrueならPlayerに伝える
}

void Enemy::SetAnimationGraphic()
{
	ResourceController* RC = ResourceController::GetResourceController();

	idolAnim = new Animation();
	normalMoveAnim = new Animation();
	fastMoveAnim = new Animation();
	ladderAnim = new Animation();
	foundAnim = new Animation();
	searchAnim = new Animation();

	//EnemyTypeごとのアニメーションデータを設定する
	switch (enemyType){
	case EnemyType::Type1:	//町人風(男性)の設定する
		//アニメーショングラフィックを取得
		idolAnim->CreateAnimation(&RC->enemy1.idol);
		idolAnim->SetParameter(6);

		normalMoveAnim->CreateAnimation(&RC->enemy1.walk);
		normalMoveAnim->SetParameter(4);

		fastMoveAnim->CreateAnimation(&RC->enemy1.dash);
		fastMoveAnim->SetParameter(2);

		ladderAnim->CreateAnimation(&RC->enemy1.ladder);
		ladderAnim->SetParameter(4);

		foundAnim->CreateAnimation(&RC->enemy1.found);
		foundAnim->SetParameter(1);

		searchAnim->CreateAnimation(&RC->enemy1.search);
		searchAnim->SetParameter(3, AnimPlayMode::TurnbackPlay);
		break;

	case EnemyType::Type2:	//町人風(女性)のグラフィックを設定する
		idolAnim->CreateAnimation(&RC->enemy2.idol);
		idolAnim->SetParameter(6);

		normalMoveAnim->CreateAnimation(&RC->enemy2.walk);
		normalMoveAnim->SetParameter(4);

		fastMoveAnim->CreateAnimation(&RC->enemy2.dash);
		fastMoveAnim->SetParameter(1);

		ladderAnim->CreateAnimation(&RC->enemy2.ladder);
		ladderAnim->SetParameter(4);

		foundAnim->CreateAnimation(&RC->enemy2.found);
		foundAnim->SetParameter(2);

		searchAnim->CreateAnimation(&RC->enemy2.search);
		searchAnim->SetParameter(4, AnimPlayMode::TurnbackPlay);
		break;

	case EnemyType::Type3:	//町奉行のグラフィックを設定する
		idolAnim->CreateAnimation(&RC->enemy3.idol);
		idolAnim->SetParameter(6);

		normalMoveAnim->CreateAnimation(&RC->enemy3.walk);
		normalMoveAnim->SetParameter(4);

		fastMoveAnim->CreateAnimation(&RC->enemy3.dash);
		fastMoveAnim->SetParameter(1);

		ladderAnim->CreateAnimation(&RC->enemy3.ladder);
		ladderAnim->SetParameter(4);

		foundAnim->CreateAnimation(&RC->enemy3.found);
		foundAnim->SetParameter(4);

		searchAnim->CreateAnimation(&RC->enemy3.search);
		searchAnim->SetParameter(4, AnimPlayMode::TurnbackPlay);
		break;

	case EnemyType::Type4:
		idolAnim->CreateAnimation(&RC->enemy4.idol);
		idolAnim->SetParameter(6);

		normalMoveAnim->CreateAnimation(&RC->enemy4.walk);
		normalMoveAnim->SetParameter(4);

		fastMoveAnim->CreateAnimation(&RC->enemy4.dash);
		fastMoveAnim->SetParameter(1);

		ladderAnim->CreateAnimation(&RC->enemy4.ladder);
		ladderAnim->SetParameter(4);

		foundAnim->CreateAnimation(&RC->enemy4.found);
		foundAnim->SetParameter(4);

		searchAnim->CreateAnimation(&RC->enemy4.search);
		searchAnim->SetParameter(4, AnimPlayMode::TurnbackPlay);
		break;
				
	case EnemyType::Type5:
		idolAnim->CreateAnimation(&RC->boss.idol);
		idolAnim->SetParameter(6);

		normalMoveAnim->CreateAnimation(&RC->boss.walk);
		normalMoveAnim->SetParameter(4);

		fastMoveAnim->CreateAnimation(&RC->boss.dash);
		fastMoveAnim->SetParameter(1);

		ladderAnim->CreateAnimation(&RC->boss.ladder);
		ladderAnim->SetParameter(4);

		foundAnim->CreateAnimation(&RC->boss.found);
		foundAnim->SetParameter(4);

		searchAnim->CreateAnimation(&RC->boss.search);
		searchAnim->SetParameter(4, AnimPlayMode::TurnbackPlay);
		break;
	}

	UIImages.resize(5);
	for (std::vector<Vertex*>::iterator it = UIImages.begin(); it != UIImages.end(); it++){
		(*it) = new Vertex(0, 0, &RC->ui.images[it - UIImages.begin()]);
	}
}

void Enemy::UpdateAnimation()
{
	bool mirrorFlag = (direction == Left);	//グラフィック反転フラグ

	switch (animState){
	case AnimationState::Stop:
		idolAnim->Update();
		dr->VertexPushBack(idolAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;

	case AnimationState::Walk:
		if (prevAnimState == Stop){
			normalMoveAnim->Initialize();
		}
		normalMoveAnim->Update();
		dr->VertexPushBack(normalMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;

	case AnimationState::Dash:
		fastMoveAnim->Update();
		dr->VertexPushBack(fastMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;

	case AnimationState::Found:
	case AnimationState::Damage:
		foundAnim->Update();
		dr->VertexPushBack(foundAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag), drawRank);
		break;

	case AnimationState::Search:
		searchAnim->Update();
		dr->VertexPushBack(searchAnim->GetCurrentVertex()->SetDraw(x, y), drawRank);
		break;
	}
}

void Enemy::PrintUI(UIImageName imgName)
{
	uiOffsetY += uiDelta;
	if (uiOffsetY > 110.0f || uiOffsetY < 100.0f){
		uiDelta = -uiDelta;
	}

	dr->VertexPushBack(UIImages[imgName]->SetDraw(X_CENTER - uiOffsetX, TOP - uiOffsetY), 1);
}

void Enemy::WeaponHitCheck()
{
	RectangleF rect1(GetLeft(), GetTop(), GetRight(), GetBottom());	//Enemy自身の矩形

	//クナイとの接触判定
	Weapon* w = playerInfo->GetWeapon(ItemType::Kunai);
	if (w != nullptr && w->GetActive()){
		if (Collision::RectToPoint(&rect1, w->GetCenterX(), w->GetCenterY())){
			//クナイと接触した
			animState = AnimationState::Damage;
			weaponDir = w->GetCenterX() < X_CENTER ? Direction::Left : Direction::Right;	//クナイの方向
			fallSpeed = -jumpForce * 0.7f;	//7割のジャンプ力で飛ぶ
			onTheGround = false;
			hp -= w->GetPower();
			w->HasHit();
		}
	}

	//マキビシとの接触判定(クナイと当たっていれば省く)
	w = playerInfo->GetWeapon(ItemType::Makibishi);
	if (w != nullptr && w->GetActive()){
		if (Collision::RectToPoint(&rect1, w->GetCenterX(), w->GetCenterY())){
			//マキビシと接触した
			animState = AnimationState::Damage;
			weaponDir = w->GetCenterX() < X_CENTER ? Direction::Left : Direction::Right;	//クナイの方向
			fallSpeed = -jumpForce * 0.7f;	//7割のジャンプ力で飛ぶ
			onTheGround = false;
			hp -= w->GetPower();
			//w->HasHit();
		}
	}

}

void Enemy::DestroyEnemy()
{
	delete idolAnim;
	delete normalMoveAnim;
	delete fastMoveAnim;
	delete ladderAnim;
	delete foundAnim;
	delete searchAnim;

	for (unsigned int i = 0; i < UIImages.size(); i++){
		delete UIImages[i];
	}
}
