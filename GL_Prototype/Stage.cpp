#include "Vertex.h"
#include "Stage.h"
#include "Draw.h"
#include "StageItem.h"
#include "Scene.h"

#include "SoundManager.h"

#include "ResourceController.h"
#define R_STAGE ((std::string)"../Resource\\Stage\\")

Stage::Stage()
{
	PlayerStartPos.X = 0;
	PlayerStartPos.Y = 0;
	Enemy0Pos = &EnemyStartPos0;
	Enemy1Pos = &EnemyStartPos1;
	Enemy2Pos = &EnemyStartPos2;
	Enemy3Pos = &EnemyStartPos3;
	initializeFlag = false;
	stageClear = false;
	itemlist.resize(0);
	initializeFlag = false;
	initializeFlag_item = false;
}
Stage::Stage(int num, int background)
{
	Stage();
	stageNum = num;
	backgroundNum = background;
}

Stage::Stage(const Stage &s){

	EnemyStartPos0 = s.EnemyStartPos0;
	EnemyStartPos1 = s.EnemyStartPos1;
	EnemyStartPos2 = s.EnemyStartPos2;
	EnemyStartPos3 = s.EnemyStartPos3;
	Enemy0Pos = &EnemyStartPos0;
	Enemy1Pos = &EnemyStartPos1;
	Enemy2Pos = &EnemyStartPos2;
	Enemy3Pos = &EnemyStartPos3;
	if (s.initializeFlag){
		if (!s.vertex){
			vertex = new Vertex();
			*vertex = *s.vertex;
		}
		if (!s.background){
			background = new Vertex();
			*background = *s.background;
		}
	}
	scene = s.scene;
	dr = s.dr;
	enemy = s.enemy;
	itemlist = s.itemlist;
	width = s.width;
	height = s.height;

	colision = s.colision;
	stageData = s.stageData;
	stageNum = s.stageNum;
	backgroundNum = s.backgroundNum;

	stageClear = s.stageClear;
	initializeFlag = s.initializeFlag;
	initializeFlag_item = s.initializeFlag_item;
	PlayerStartPos = s.PlayerStartPos;

}
Stage& Stage::operator=(const Stage &s){
	EnemyStartPos0 = s.EnemyStartPos0;
	EnemyStartPos1 = s.EnemyStartPos1;
	EnemyStartPos2 = s.EnemyStartPos2;
	EnemyStartPos3 = s.EnemyStartPos3;
	Stage();
	if (s.vertex){
		vertex = new Vertex();
		*vertex = *s.vertex;
	}
	if (s.background){
		background = new Vertex();
		*background = *s.background;
	}
	scene = s.scene;
	dr = s.dr;
	enemy = s.enemy;
	itemlist = s.itemlist;
	width = s.width;
	height = s.height;

	colision = s.colision;
	stageData = s.stageData;
	stageNum = s.stageNum;
	backgroundNum = s.backgroundNum;

	stageClear = s.stageClear;
	initializeFlag = s.initializeFlag;
	initializeFlag_item = s.initializeFlag_item;
	PlayerStartPos = s.PlayerStartPos;

	return (*this);
}

Stage::~Stage()
{
	if (initializeFlag){
		if (vertex != NULL)
			delete vertex;
		if (background != NULL)
			delete background;
	}
}

bool Stage::Initialize(){
	if (initializeFlag)
		return true;
	scene = Scene::GetScene();
	RC = ResourceController::GetResourceController();
	//Drawクラス
	dr = Draw::GetDraw();
	//ステージグラフィックを作成
	vertex = new Vertex(0, 0, &RC->stage.stage[stageNum]);
	//ステージデータ画像をロード
	IplImage* img = RC->stage.stage_data[stageNum];
	if (img == NULL){
		assert(0);
		return false;
	}
	width = img->width;
	height = img->height;

	//コリジョン・データ配列初期化
	colision.resize(width);
	stageData.resize(width);
	for (int x = 0; x < width; x++){
		colision[x].resize(height);
		stageData[x].resize(height);
		for (int y = 0; y < height; y++){
			colision[x][y] = -1;
			stageData[x][y] = -1;
		}
	}
	/*各色ごとに保持する配列の宣言*/
	std::vector<unsigned char> color(3);

	//アイテム領域用
	int countX = 0;
	int countY = 0;

	//画素値（B,G,R）を順次取得する
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++){
			if (colision[x][y] == -1 && stageData[x][y] == -1){
				//コリジョン画像の画素を配列に取得
				color[0] = img->imageData[y * img->widthStep + x * img->nChannels + 0];
				color[1] = img->imageData[y * img->widthStep + x * img->nChannels + 1];
				color[2] = img->imageData[y * img->widthStep + x * img->nChannels + 2];

				//壁
				if (color[0] + color[1] + color[2] == 0){
					colision[x][y] = 1;
					stageData[x][y] = 1;
				}
				//青色255（隠れる箱）
				else if ((color[0] == 255 && color[1] + color[2] == 0)){
					CheckRectImage(*img, x, y, 255, 0, 0, countX, countY);
					//ステージのアイテムリストに登録（左上座標）
					StageObject stelthbox(itemlist.size(), ObjectType::StelthBox, x, y, countX, countY);
					objectlist.push_back(stelthbox);
					ClearRectImage(x, y, countX, countY, 0, 3);
				}
				//青色200(当たり判定あり箱)
				else if (color[0] == 200 && color[1] + color[2] == 0){
					CheckRectImage(*img, x, y, 200, 0, 0, countX, countY);
					//ステージのアイテムリストに登録（左上座標）
					StageObject woodbox(itemlist.size(), ObjectType::Box, x, y, countX, countY);
					objectlist.push_back(woodbox);
					ClearRectImage(x, y, countX, countY, 1, 0);
				}
				//小判
				else if (color[1] == 255 && color[0] + color[2] == 0 && !initializeFlag_item){
					//正方形の場合アイテムを作成する
					if (CheckRectImage(*img, x, y, 0, 255, 0, countX, countY)){
						StageItem gold(itemlist.size(), ItemType::Gold, x, y);
						itemlist.push_back(gold);
						ClearRectImage(x, y, countX, countY, 0);
					}
				}
				//鍵
				else if (color[1] == 200 && color[0] + color[2] == 0 && !initializeFlag_item){
					//正方形の場合アイテムを作成する
					if (CheckRectImage(*img, x, y, 0, 200, 0, countX, countY)){
						StageItem key(itemlist.size(), ItemType::Key, x, y);
						itemlist.push_back(key);
						ClearRectImage(x, y, countX, countY, 0);
					}
				}
				//招き猫
				else if (color[1] == 150 && color[0] + color[2] == 0 && !initializeFlag_item){
					//正方形の場合アイテムを作成する
					if (CheckRectImage(*img, x, y, 0, 150, 0, countX, countY)){
						StageItem neko(itemlist.size(), ItemType::Manekineko, x, y);
						itemlist.push_back(neko);
						ClearRectImage(x, y, countX, countY, 0);
					}
				}
				//黄色(はしご)
				else if (color[1] + color[2] == 510 && color[0] == 0){
					CheckRectImage(*img, x, y, 0, 255, 255, countX, countY);
					StageObject ladder(itemlist.size(), ObjectType::Ladder, x, y, countX, countY);
					objectlist.push_back(ladder);
					ClearRectImage(x, y, countX, countY, 0, 1);
				}
				//赤色255（次ドア）
				else if (color[0] + color[1] == 0 && color[2] == 255){
					CheckRectImage(*img, x, y, 0, 0, 255, countX, countY);
					StageObject nextdoor(itemlist.size(), ObjectType::NextDoor, x, y, countX, countY, stageNum + 1);	//次のステージに移動する
					objectlist.push_back(nextdoor);
					ClearRectImage(x, y, countX, countY, 0, 15);
				}//赤色200(前ドア)
				else if (color[0] + color[1] == 0 && color[2] == 200){
					CheckRectImage(*img, x, y, 0, 0, 200, countX, countY);
					StageObject backdoor(itemlist.size(), ObjectType::BackDoor, x, y, countX, countY, stageNum - 1);	//前のステージに移動する
					objectlist.push_back(backdoor);
					ClearRectImage(x, y, countX, countY, 0, 15);
				}
				//白色
				else if (color[0] + color[1] + color[2] == 255 * 3){
					colision[x][y] = 0;
					stageData[x][y] = 0;
				}
				//BGR 150,150,150プレイヤースタート地点
				else if (color[0] == 150 && color[1] == 150 && color[2] == 150){
					CheckRectImage(*img, x, y, 150, 150, 150, countX, countY);
					PlayerStartPos.X = (float)x + countX / 2;
					PlayerStartPos.Y = (float)y;
					ClearRectImage(x, y, countX, countY);
				}
				//BGR 0,200,200エネミー1スタート地点
				else if (color[0] == 0 && color[1] == 200 && color[2] == 200){
					CheckRectImage(*img, x, y, 0, 200, 200, countX, countY);
					Position enemy_temp;
					enemy_temp.X = (float)x + countX / 2;
					enemy_temp.Y = (float)y;
					EnemyStartPos0.push_back(enemy_temp);
					ClearRectImage(x, y, countX, countY);
				}
				//BGR 200,200,0エネミー2スタート地点
				else if (color[0] == 200 && color[1] == 200 && color[2] == 0){
					CheckRectImage(*img, x, y, 200, 200, 0, countX, countY);
					Position enemy_temp;
					enemy_temp.X = (float)x + countX / 2;
					enemy_temp.Y = (float)y ;
					EnemyStartPos1.push_back(enemy_temp);
					ClearRectImage(x, y, countX, countY);
				}
				//BGR 200,0,200エネミー3スタート地点
				else if (color[0] == 200 && color[1] == 0 && color[2] == 200){
					CheckRectImage(*img, x, y, 200, 0, 200, countX, countY);
					Position enemy_temp;
					enemy_temp.X = (float)x + countX / 2;
					enemy_temp.Y = (float)y;
					EnemyStartPos2.push_back(enemy_temp);
					ClearRectImage(x, y, countX, countY);
				}
				//BGR 200,200,200エネミー4スタート地点
				else if (color[0] == 200 && color[1] == 200 && color[2] == 200){
					CheckRectImage(*img, x, y, 200, 200, 200, countX, countY);
					Position enemy_temp;
					enemy_temp.X = (float)x + countX / 2;
					enemy_temp.Y = (float)y;
					EnemyStartPos3.push_back(enemy_temp);
					ClearRectImage(x, y, countX, countY);
				}

			}
		}
	}
	//背景グラフィックを作成
	background = new Vertex(0, 0, width, height, &RC->stage.background[backgroundNum], 1, 1);

	initializeFlag = true;
	initializeFlag_item = true;

	return true;
}
bool Stage::CheckRectImage(IplImage& img, int x, int y, int B, int G, int R, int& countX, int&countY){

	countX = 0;
	countY = 0;
	//X行の指定色画素をカウント
	for (int i = x; i < width; i++){
		std::vector<unsigned char>color(3);
		color[0] = img.imageData[y * img.widthStep + i * img.nChannels + 0];
		color[1] = img.imageData[y * img.widthStep + i * img.nChannels + 1];
		color[2] = img.imageData[y * img.widthStep + i * img.nChannels + 2];
		if (color[0] == B && color[1] == G && color[2] == R){
			countX++;
		}
		else{
			break;
		}
	}
	//Y行の指定色画素をカウント
	for (int j = y; j < height; j++){
		std::vector<unsigned char>color(3);
		color[0] = img.imageData[j * img.widthStep + x * img.nChannels + 0];
		color[1] = img.imageData[j * img.widthStep + x * img.nChannels + 1];
		color[2] = img.imageData[j * img.widthStep + x * img.nChannels + 2];
		if (color[0] == B && color[1] == G && color[2] == R){
			countY++;
		}
		else{
			break;
		}
	}
	//正方形
	if (countX == countY){
		return true;
		//それ以外
	}
	else{
		return false;
	}
}
void Stage::ClearRectImage(int x, int y, int countX, int countY, int colisionnum, int stagedatanum){
	for (int i = x; i < x + countX; i++){
		for (int j = y; j < y + countY; j++){
			colision[i][j] = colisionnum;
			stageData[i][j] = stagedatanum;
		}
	}
}
bool Stage::UpDate(){
	//描画リストに登録
	dr->VertexPushBack(vertex, 2);
	dr->VertexPushBack(background, 3);

	for (std::vector<StageItem>::iterator i = itemlist.begin(); i != itemlist.end(); i++){
		i->Update();
	}
	for (std::vector<StageObject>::iterator i = objectlist.begin(); i != objectlist.end(); i++){
		i->Update();
	}
	return true;
}

bool Stage::Close(){
	colision.clear();
	stageData.clear();
	objectlist.clear();
	EnemyStartPos0.clear();
	EnemyStartPos1.clear();
	EnemyStartPos2.clear();
	EnemyStartPos3.clear();
	delete vertex;
	delete background;
	initializeFlag = false;
	return true;
}
Item Stage::GetStageItem(int num){
	Item a;
	unsigned int i = num;
	if (0 <= i && i < itemlist.size()){
		a = itemlist[i];
		//リストを詰める
		for (unsigned int j = i + 1; j < itemlist.size(); j++){
			itemlist[j - 1] = itemlist[j];
		}
		itemlist.pop_back();
	}
	if (a.GetType() == ItemType::Gold)
		SoundManager::GetSoundManager()->CallSound(scene->keyhr);
	if (a.GetType() == ItemType::Key){
		stageClear = true;
		SoundManager::GetSoundManager()->CallSound(scene->keyhr);
	}
	if (a.GetType() == ItemType::Manekineko){
		scene->setClear();
		SoundManager::GetSoundManager()->CallSound(scene->keyhr);
	}

	return a;
}
bool Stage::ActionObject(StageObject& so){
	if (so.GetObjectType() == ObjectType::NextDoor){
		if (stageClear == 1){
			SoundManager::GetSoundManager()->CallSound(scene->clearhr);
			so.Action();
			return true;
		}
	}
	else if (so.GetObjectType() == ObjectType::BackDoor){
		if (scene->ClearFlag() == 1){
			SoundManager::GetSoundManager()->CallSound(scene->clearhr);
			so.Action();
		}
	}
	return false;
}
Position* Stage::GetStartPos(bool reverse){
	if (!reverse)
		return &PlayerStartPos;
	else{
		for (unsigned int i = 0; i < objectlist.size(); i++){
			if (objectlist[i].GetObjectType() == ObjectType::NextDoor)
				return &objectlist[i].GetPosition();
		}
		//ドアが見つからない場合
		return &PlayerStartPos;
	}
}