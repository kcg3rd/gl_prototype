#include "EnemyManager.h"


EnemyManager::EnemyManager(int eTypeCount)
{
	maxEnemyTypeCount = eTypeCount;
	enemyList.resize(eTypeCount);
}

bool EnemyManager::Initialize(Stage* stage)
{
	//enemyList内のEnemy配列の要素すべてのInitialize()メソッドを実行する
	for (listItr = enemyList.begin(); listItr != enemyList.end(); listItr++){
		for (enemyItr = listItr->begin(); enemyItr != listItr->end(); enemyItr++){
			if ((*enemyItr)->IsReady()){	//対象のEnemyの準備が出来ているか
				(*enemyItr)->Initialize(stage);
			}
		}
	}

	return true;
}

bool EnemyManager::Update(Stage* stage, bool inStage)
{
	if (!inStage){ return true; }

	////enemyList内のEnemy配列内の生存しているEnemyのみ更新する(描画も同じ)
	for (listItr = enemyList.begin(); listItr != enemyList.end(); listItr++){
		for (enemyItr = listItr->begin(); enemyItr != listItr->end(); enemyItr++){
			if ((*enemyItr)->IsExist() && (*enemyItr)->IsReady()){
				(*enemyItr)->Update(stage, inStage);
			}
		}
	}

	return true;
}

Enemy* EnemyManager::CreateNewEnemy(EnemyType type)
{
	EnemyVector* target = &enemyList[type];		//対象のEnemy配列
	target->push_back(new Enemy());		//新たなEnemyを作成→追加する
	return target->back();		//作成されたEnemyへの参照を返す
}

int EnemyManager::GetEnemyCount(EnemyType type, bool existOnly)
{
	int counter = 0;

	EnemyVector* target = &enemyList[type];		//対象のEnemy配列
	if (existOnly){
		//生存しているEnemyのみカウントする
		for (enemyItr = target->begin(); enemyItr != target->end(); enemyItr++){
			if ((*enemyItr)->IsExist()){
				counter++;
			}
		}
	}
	else{
		//要素数すべてをカウント
		counter = target->size();
	}

	return counter;
}

int EnemyManager::GetAllEnemyCount(bool existOnly)
{
	int counter = 0;
	
	//enemyList内の全Enemy配列が対象
	for (listItr = enemyList.begin(); listItr != enemyList.end(); listItr++){
		listItr - enemyList.begin();
		counter += GetEnemyCount(EnemyType(listItr - enemyList.begin()), existOnly);
	}

	return counter;
}

Enemy* EnemyManager::GetEnemy(EnemyType type, unsigned int index)
{
	if (enemyList.size() == 0) { return nullptr; }

	EnemyVector* target = &enemyList[type];		//対象のEnemy配列
	if (index >= target->size()){
		return nullptr;		//引数のindexがEnemy配列の要素数を越えていたとき
	}
	return target->at(index);
}

void EnemyManager::ClearEnemyList(EnemyType type)
{
	if (enemyList.size() == 0) { return; }

	EnemyVector* target = &enemyList[type];	//対象のEnemy配列
	//if (target == nullptr){ return; }

	//対象のEnemy配列内の要素を破棄していく
	for (enemyItr = target->begin(); enemyItr != target->end(); enemyItr++){
		delete (*enemyItr);
	}
	target->clear();
}

void EnemyManager::ClearAllEnemyList()
{
	if (enemyList.size() == 0) { return; }

	//enemyList内の全Enemy配列が対象
	for (listItr = enemyList.begin(); listItr != enemyList.end(); listItr++){
		ClearEnemyList(EnemyType(listItr - enemyList.begin()));
	}
}

void EnemyManager::DestroyEnemyManager()
{
	ClearAllEnemyList();
}