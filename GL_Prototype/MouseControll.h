#ifndef _MOUSECONTROLL_H_
#define _MOUSECONTROLL_H_

#define GLFW_DLL
#include <GL/glew.h>
#include <GL/glfw.h>

#include "Screen.h"
#include "Define.h"

class MouseControll
{
public:
	MouseControll();
	~MouseControll();
	bool Initialize();
	void Update();
protected:
	Screen* sc;
	Position_i mousePOS;
	Position_i g_mousePOS;	//スクロールを適用したマウス位置
	bool Lbutton;
	bool Rbutton;
	int wheel;
};

#endif