#ifndef _ITEM_H_
#define _ITEM_H_

#include "Define.h"
#include "Vertex.h"

class Draw;
class StageItem;

class Item
{
public:
	Item();
	virtual ~Item(){};
	Item(int number, ItemType type);
	Item(const Item&);
	Item& operator =(const Item&);

	inline ItemType GetType(){
		return itemType;
	}
	inline int GetItemID(){
		return itemID;
	}
protected:
	int itemID;	//アイテムの番号
	ItemType itemType;	//アイテムの種類
};

#endif