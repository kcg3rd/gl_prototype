#include "Menu.h"

#include <stdlib.h>
#include <time.h>
#include "SoundManager.h"

Menu::Menu()
{
	RC = ResourceController::GetResourceController();
	DR = Draw::GetDraw();
	SC = Screen::GetScreen();
}


Menu::~Menu()
{
	if (background != nullptr)
		delete background;
	if (Arrow != nullptr)
		delete Arrow;
	const_graphics.clear();
	graphics.clear();

}

Menu::Menu(const Menu& m)
{
	Menu();
	graphics = m.graphics;
	background = m.background;
}

Menu& Menu::operator=(const Menu& m){
	Menu();
	graphics = m.graphics;
	background = m.background;
	return *this;
}

bool Menu::Initialize(){
	return true;
}

int Menu::Update(){
	SC->x = 0;
	SC->y = 0;
	DR->VertexPushBack(background, 3);

	for (std::vector<Vertex>::iterator it = const_graphics.begin(); it != const_graphics.end(); it++){
		DR->VertexPushBack(&(*it));
	}
	for (std::vector<Vertex>::iterator it = graphics.begin(); it != graphics.end(); it++){
		DR->VertexPushBack(&(*it));
	}

	if (Arrow != nullptr)
		DR->VertexPushBack(Arrow);
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////

Title::Title() :Menu()
{
}


Title::~Title()
{
}

bool Title::Initialize(){
	//Menu::Initialize(0);
	int SCheight = SC->Height();
	int SCwidth = SC->Width();
	background = new Vertex(0, 0, &RC->menu.title);
	button1 = false;
	srand((unsigned)time(NULL));
	for (unsigned int i = 0; i < RC->ui.title.size(); i++){
		Vertex menu(SCwidth / 2, SCheight / 2 + i * 150, &RC->ui.title[i]);
		graphics.push_back(menu);
	}
	Arrow = new Vertex(graphics[0].GetX() - graphics[0].GetWidth()/4, graphics[0].GetY() + graphics[0].GetHeight() / 2, &RC->item.kunai);
	w = s = false;
	return true;
}
int Title::Update(){
	Menu::Update();
	if (glfwGetKey('W') == GLFW_RELEASE)
		w = true;
	if (glfwGetKey('S') == GLFW_RELEASE)
		s = true;
	if (button1 && glfwGetKey(GLFW_KEY_SPACE) == GLFW_RELEASE)
		button1 = true;
	if (w && glfwGetKey('W')){
		if (select <= 0){
			select = graphics.size() - 1;
		}
		else{
			select--;
		}
		w = false;
	}
	else if (s && glfwGetKey('S')){
		if (select >= graphics.size() - 1){
			select = 0;
		}
		else{
			select++;
		}
		s = false;
	}
	//スペースキーが押された場合
	if (glfwGetKey(GLFW_KEY_SPACE)){
		if (select == 0){
			return 1;
		}
		else if (select == graphics.size() - 1){
			return -1;
		}
		button1 = false;
	}
	Arrow->SetPosition((float)graphics[select].GetX() - graphics[select].GetWidth() / 4, (float)graphics[select].GetY() + graphics[select].GetHeight() / 2);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////

GameOver::GameOver() :Menu()
{
}

GameOver::~GameOver()
{
}

bool GameOver::Initialize()
{
	background = new Vertex(0, 0, &RC->menu.over);
	overhr = SoundManager::GetSoundManager()->SetSound("gameover.wav");
	callover = false;
	return true;
}

int GameOver::Update()
{
	Menu::Update();
	if (glfwGetKey(GLFW_KEY_ENTER) == GLFW_RELEASE)
		button1 = true;
	if (button1 && glfwGetKey(GLFW_KEY_ENTER)){
		button1 = false;
		callover = false;
		return 1;
	}
	if (callover == false){
		SoundManager::GetSoundManager()->CallSound(overhr);
		callover = true;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////

GameClear::GameClear() :Menu()
{
}

GameClear::~GameClear()
{
}

bool GameClear::Initialize()
{
	background = new Vertex(0, 0, &RC->menu.clear);
	clearhr = SoundManager::GetSoundManager()->SetSound("gameclear.wav");
	callclear = false;
	return true;
}

int GameClear::Update(){
	Menu::Update();
	if (glfwGetKey(GLFW_KEY_ENTER) == GLFW_RELEASE)
		button1 = true;
	if (button1 && glfwGetKey(GLFW_KEY_ENTER)){
		button1 = false;
		callclear = false;
		return 1;
	}
	
	if (callclear == false){
		SoundManager::GetSoundManager()->CallSound(clearhr);
		callclear = true;
	}

	return 0;
}