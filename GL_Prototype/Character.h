//キャラクタークラス
#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include "Define.h"
#include "Draw.h"

#define X_CENTER	(x + width / 2.0f)		//中心X座標
#define Y_CENTER	(y + height / 2.0f)		//中心Y座標
#define LEFT		(x)						//キャラクターの左座標
#define TOP			(y)						//キャラクターの上座標
#define RIGHT		(x + width)				//キャラクターの右座標
#define BOTTOM		(y + height)			//キャラクターの下座標

class Vertex;
class Draw;
class Stage;


class Character
{
public:
	Character(){ }
	Character(
		float startPosX,
		float startPosY,
		float width,
		float height,
		float speed,
		float jumpForce,
		Direction startDir,
		int drawRank,
		int HP = 100,
		bool useGravity = true
		);
	virtual ~Character(){ DestroyCharacter(); }

	//キャラクターグラフィックを設定する(最終的には使わない)
	void CreateCharacter(GLuint* texture_id);

	virtual void Initialize();
	virtual void Update(Stage* stage, bool inStage);
	virtual void draw();	//同じ名前のクラスがある影響でDraw()にできなかった

	//左(x)座標を取得する
	inline float GetLeft() { return LEFT; }

	//上(y)座標を取得する
	inline float GetTop(){ return TOP; }

	//右(x + width)座標を取得する
	inline float GetRight() { return RIGHT; }

	//下(y + height)座標を取得する
	inline float GetBottom(){ return BOTTOM; }

	//キャラクターの中心X座標を取得する
	inline float GetCenterX() { return X_CENTER; }

	//キャラクターの中心Y座標を取得する
	inline float GetCenterY() { return Y_CENTER; }

	//幅を取得する
	inline float GetWidth() { return width; }

	//高さを取得する
	inline float GetHeight() { return height; }

	//スピードを取得する
	inline float GetSpeed() { return speed; }

	//跳躍力を取得する
	inline float GetJumpForce() { return jumpForce; }

	//体力を取得する
	inline int GetHP() { return hp; }

	//生存中であるか調べる
	inline bool IsExist(){ return hp > 0; }

	//自由落下フラグを取得する
	inline bool GetUseGravityFlag() { return this->useGravity; }

	//接地中フラグを取得する
	bool GetOnGroundFlag() { return onTheGround; }

	//ハシゴに掴まっているフラグを取得する
	bool GetOnLadderFlag() { return onTheLadder && !onTheGround; }

	//描画ランクを取得する
	inline int GetDrawRank() { return drawRank; }


	//位置を設定する
	inline void SetPosition(float x, float y) {
		this->x = this->startPosX = x;
		this->y = this->startPosY = y;
	}

	//サイズを設定する
	inline void SetSize(float width, float height) { this->width = width; this->height = height; }

	//スピードを設定する
	inline void SetSpeed(float speed) { this->speed = speed; }

	//跳躍力を設定する
	inline void SetJumpForce(float value) { jumpForce = value; }

	//体力を設定する
	inline void SetHP(int hp) { this->hp = hp; }

	//自由落下フラグを設定する
	inline void SetUseGravityFlag(bool val){ this->useGravity = val; }

	//初期方向を設定する
	inline void SetStartDirection(Direction dir) { startDir = dir; }

	//描画ランクを設定する
	inline void SetDrawRank(int drawRank){ this->drawRank = drawRank; }

	//メンバ変数の一括設定を行う
	inline void SetParameter(float startX, float startY, float width, float height, float speed, int hp, Direction startDir, int drawRank, bool useGravity = true)
	{
		SetPosition(startX, startY);
		SetSize(width, height);
		SetSpeed(speed);
		SetHP(hp);
		SetStartDirection(startDir);
		SetDrawRank(drawRank);
		SetUseGravityFlag(useGravity);
	}

protected:

	Vertex* vertex;
	Draw*   dr;				//drawクラスのポインタ
	int     drawRank;		//描画ランク
	float   x;				//キャラクターのX座標
	float   y;				//キャラクターのY座標(直接変更は非推奨)
	float   width;			//キャラクターの幅
	float   height;			//キャラクターの高さ
	float   speed;			//キャラクターの移動速度
	float   jumpForce;		//キャラクターの跳躍力

	float startPosX;		//初期X座標
	float startPosY;		//初期Y座標
	float prevX;			//更新前のX座標
	float prevY;			//更新前のY座標
	float fallSpeed;		//落下速度
	bool  onTheGround;		//接地フラグ
	bool  onTheLadder;		//ハシゴと重なっているフラグ
	bool  useHook;			//フックショット使用中のフラグ
	bool  collisionWall;	//壁にぶつかっているフラグ

	bool useGravity = true;	//自由落下をするかフラグ

	Direction startDir = Right;		//初期方向
	Direction direction;			//キャラクターの方向

	int hp;			//体力
	//会話状態

	//ステージとの接触を処理する
	void checkCollisionStage(Stage* stage);

	void DestroyCharacter();

};
#endif