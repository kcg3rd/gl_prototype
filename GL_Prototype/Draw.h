#ifndef _DRAW_H_
#define _DRAW_H_



#include <GL/glew.h>
#include <GL/glfw.h>

#include<vector>

#include "Vertex.h"
//実際に描画を行うシングルトンクラス
class Draw
{
public:
	//シングルトンクラス
	static Draw* GetDraw(){
		static Draw draw;
		return &draw;
	}

	bool VertexPushBack(Vertex*,int = 0);
	bool UpDate();
	bool Clear();
	void QuickDraw(Vertex*);
private:
	Draw();
	~Draw();
	void VertexDraw(Vertex*);
	std::vector<std::vector<Vertex*>*> vertexrank;
	std::vector< Vertex* >vertexlist0;
	std::vector< Vertex* >vertexlist1;
	std::vector< Vertex* >vertexlist2;
	std::vector< Vertex* >vertexlist3;
	std::vector< Vertex* >vertexlist4;
	
};


#endif