#include "HookShot.h"
#include "Player.h"
#include "SoundManager.h"
HookShot::HookShot() :MouseControll()
{
	rc = ResourceController::GetResourceController();
	v = Vertex(0, 0, &rc->item.kunai);
	speed = rc->parameter.itemparam.hookshot.speed;
	if (speed == -1)
		speed = 2;
}


HookShot::~HookShot()
{
	MouseControll::~MouseControll();
}

bool HookShot::Initialize(){
	MouseControll::Initialize();
	scene = Scene::GetScene();
	draw = Draw::GetDraw();
	se_movehr = SoundManager::GetSoundManager()->SetSound("hook.wav");
	return true;
}

bool HookShot::Update(Player* player,float& x, float& y){
	MouseControll::Update();
	if (Lbutton && !useFlag && !moveFlag){
		targetPos = g_mousePOS;
		startPos = toolPos = { (int)player->GetCenterX(), (int)player->GetCenterY() };
		if (5 > abs(targetPos.X - startPos.X) / speed || 5 > abs((targetPos.Y - startPos.Y) / speed)){
			//移動距離が短い場合は終了
			return false;
		}
		useFlag = true;
	}



	if (useFlag){
		toolPos.X += (targetPos.X - startPos.X) / speed;
		toolPos.Y += (targetPos.Y - startPos.Y) / speed;
		draw->VertexPushBack(v.SetDraw((float)toolPos.X, (float)toolPos.Y));
		if (toolPos.X < 0 || toolPos.X > scene->Now()->GetWidth() || toolPos.Y < 0 || toolPos.Y > scene->Now()->GetHeight()){
			useFlag = false;
			return false;
		}	
		if (1 == scene->Now()->GetStageData(toolPos.X, toolPos.Y)){
			useFlag = 0;
			moveFlag = 1;
			startPos = { (int)x, (int)y };
			//x = (float)toolPos.X;
			//y = (float)toolPos.Y;
			SoundManager::GetSoundManager()->CallSound(se_movehr);
		}
		else if (toolPos.X < sc->x || toolPos.X > sc->x + sc->Width() || toolPos.Y < sc->y || toolPos.Y > sc->y + sc->Height()){
			useFlag = 0;
		}
		return true;
	}
	else if (moveFlag){
		int movelangeX = (toolPos.X - startPos.X) / (speed * 3);
		int movelangeY = (toolPos.Y - startPos.Y) / (speed * 3);
		if (abs(movelangeX) < 1 || abs(movelangeY) < 1){
			moveFlag = false;
			return true;
		}


		if (movelangeX < 0 && x >scene->Now()->GetCollision_Left((int)x, (int)y) && scene->Now()->GetCollision_Left((int)x, (int)y) > x + movelangeX){
			x = scene->Now()->GetCollision_Left(x, y);
			moveFlag = false;
		}
		else if (movelangeX > 0 && x < scene->Now()->GetCollision_Right(x, y + player->GetWidth()) && scene->Now()->GetCollision_Right(x + player->GetWidth(),y) < x + player->GetWidth() + movelangeX){
			x = (float)scene->Now()->GetCollision_Right((int)x- player->GetWidth(), y) - player->GetWidth();
			if (x < 0)
				x = 0;
			moveFlag = false;
		}
		if (moveFlag == false){
			return false;
		}
		if (movelangeY < 0 && y > scene->Now()->GetCollision_Top(x, y) && scene->Now()->GetCollision_Top(x, y ) > y + movelangeY){
			y = (float)scene->Now()->GetCollision_Top((int)x, (int)y);
			moveFlag = false;
		}
		else if (movelangeY > 0 && y < scene->Now()->GetCollision_Bottom(x, y+player->GetHeight()) && scene->Now()->GetCollision_Bottom(x, y + player->GetHeight()) < y +  player->GetHeight() + movelangeY){
			y = (float)scene->Now()->GetCollision_Bottom(x, y + player->GetHeight()) - player->GetHeight();
			if (y < 0)
				y = 0;
			moveFlag = false;
		}
		if (moveFlag == false)	//地形にぶつかった時はi終了
			return false;

		if ((x < toolPos.X && x + movelangeX > toolPos.X) || (x + player->GetWidth() > toolPos.X && x + player->GetWidth()+ movelangeX < toolPos.X))
			x = (float)toolPos.X;
		if ((y < toolPos.Y && y+movelangeY > toolPos.Y) ||( y + player->GetHeight() > toolPos.Y && y + player->GetHeight() + movelangeY < toolPos.Y))
			y = (float)toolPos.Y;
		if (x != toolPos.X)
			x += (float)movelangeX;
		if (y != toolPos.Y)
			y += (float)movelangeY;
		if (toolPos.X == x && toolPos.Y == y){
			moveFlag = false;
		}
		return true;
	}
	return false;
}