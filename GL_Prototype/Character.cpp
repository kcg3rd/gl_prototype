#include "Character.h"
//#include <math.h>
#include <string>

#include "Vertex.h"
#include "Draw.h"
#include "Stage.h"
#include "Item.h"

Character::Character(float posX, float posY, float width, float height, float speed, float jumpF, Direction dir, int drRank, int hp, bool useGravity)
	:x(posX), y(posY), width(width), height(height)
	,speed(speed), jumpForce(jumpF), startDir(dir), drawRank(drRank)
	, hp(hp), useGravity(useGravity)
{
	startPosX = posX;
	startPosY = posY;
}

void Character::CreateCharacter(GLuint* texture_id)
{
	vertex = new Vertex(0, 0, texture_id);
	dr = Draw::GetDraw();
}

void Character::Initialize()
{
	direction = startDir;
	x = startPosX;
	y = startPosY;
}

void Character::Update(Stage* stage, bool inStage)
{
	if (!inStage){ return; }

	x;
	y += fallSpeed;

	checkCollisionStage(stage);

	dr->VertexPushBack(vertex->SetDraw(x, y), 0);
}

void Character::checkCollisionStage(Stage* stage)
{
	if (useGravity){

		//----------垂直の接触処理↓----------
		const int ceilingHeight = stage->GetCollision_Top(S_CAST_I(X_CENTER), S_CAST_I(Y_CENTER)) + 1;	//キャラクターの中心座標の天井の高さ
		const int floorHeight = stage->GetCollision_Bottom(S_CAST_I(X_CENTER), S_CAST_I(Y_CENTER)) - 1;	//キャラクターの中心座標の床の高さ

		const int downTilt = 5;	//下り坂の傾き

		if (TOP < ceilingHeight){	//天井と接触した場合
			y = S_CAST_F(ceilingHeight);
			fallSpeed = 0.0f;
		}

		if (BOTTOM >= floorHeight){		//床と接触した場合
			y = S_CAST_F(floorHeight - height);
			fallSpeed = 0.0f;
			onTheGround = true;
		}
		else if (BOTTOM < floorHeight){	//床と接していない場合
			if (onTheGround && !onTheLadder && BOTTOM + downTilt > floorHeight){	//下り坂での落下
				y = S_CAST_F(floorHeight - height);
			}
			else{
				onTheGround = false;
				fallSpeed += gravity;	//落下加速度
			}
		}
	}
	//----------水平の接触処理↓----------
	const float stepHeight = height / 3.0f;		//通れる段差の高さ
	float wallCheckHeight = height - stepHeight;	//下部壁検知点の高さ

	const int upperWallL = stage->GetCollision_Left(S_CAST_I(X_CENTER), S_CAST_I(TOP));		//キャラクター上部左側の壁X座標
	const int upperWallR = stage->GetCollision_Right(S_CAST_I(X_CENTER), S_CAST_I(TOP));	//キャラクター上部右側の壁X座標
	
	const int middleWallL = stage->GetCollision_Left(S_CAST_I(X_CENTER), S_CAST_I(TOP + wallCheckHeight / 2.0f));	//上部と下部の中心左側の壁X座標
	const int middleWallR = stage->GetCollision_Right(S_CAST_I(X_CENTER), S_CAST_I(TOP + wallCheckHeight / 2.0f));	//上部と下部の中心右側の壁X座標
	
	const int lowerWallL = stage->GetCollision_Left(S_CAST_I(X_CENTER), S_CAST_I(TOP + wallCheckHeight));	//キャラクター下部左側の壁X座標
	const int lowerWallR = stage->GetCollision_Right(S_CAST_I(X_CENTER), S_CAST_I(TOP + wallCheckHeight));	//キャラクター下部右側の壁X座標
		
	collisionWall = false;
	if (LEFT < upperWallL || LEFT < middleWallL || LEFT < lowerWallL){	//(3点のどれかが)左の壁と接触している場合
		x = S_CAST_F(MAX(upperWallL, MAX(middleWallL, lowerWallL)));	//一番右に出ている壁に合わせる
		collisionWall = true;
	}
	else if (RIGHT > upperWallR || RIGHT > middleWallR || RIGHT > lowerWallR){	//(3点のどれかが)右の壁と接触している場合
		x = S_CAST_F((MIN(upperWallR, MIN(middleWallR, lowerWallR)) - width));	//一番左に出ている壁に合わせる
		collisionWall = true;
	}

	//----------ハシゴとの接触処理----------
	//キャラクターの中心下部にハシゴあるか調べる
	if (stage->GetStageData(S_CAST_I(X_CENTER), S_CAST_I(BOTTOM)) == 1){
		onTheLadder = true;
		if (fallSpeed > 0.0f){	//落下しているときは掴まれる
			fallSpeed = 0.0f;
		}
	}
	else{
		onTheLadder = false;
	}
}

void Character::draw()
{
}

void Character::DestroyCharacter()
{
	delete vertex;
}
