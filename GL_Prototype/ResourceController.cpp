#include<Windows.h>	//メッセージボックス表示のため
#include <fstream>
#include <iostream>
#include "ResourceController.h"


#define ARRAY_LENGTH(array) (sizeof(array) / sizeof(array[0]))

ResourceController::ResourceController()
{
	const int stagesize = 12;	//ステージ数
	player.idol.resize(12);
	player.walk.resize(12);
	player.shinobi.resize(12);
	player.dash.resize(12);
	player.ladder.resize(12);
	player.attack.resize(12);
	player.furoshiki.resize(12);
	player.found.resize(12);

	enemy1.idol.resize(12);
	enemy1.walk.resize(12);
	enemy1.dash.resize(12);
	enemy1.ladder.resize(12);
	enemy1.found.resize(12);
	enemy1.search.resize(7);

	enemy2.idol.resize(12);
	enemy2.walk.resize(12);
	enemy2.dash.resize(12);
	enemy2.ladder.resize(12);
	enemy2.found.resize(12);
	enemy2.search.resize(7);

	enemy3.idol.resize(12);
	enemy3.walk.resize(12);
	enemy3.dash.resize(12);
	enemy3.ladder.resize(12);
	enemy3.found.resize(12);
	enemy3.search.resize(7);

	enemy4.idol.resize(12);
	enemy4.walk.resize(12);
	enemy4.dash.resize(12);
	enemy4.ladder.resize(12);
	enemy4.found.resize(12);
	enemy4.search.resize(7);

	int idolLength = 12;
	boss.idol.resize(12);
	boss.walk.resize(14);
	boss.dash.resize(8);
	//
	boss.ladder.resize(idolLength);
	boss.found.resize(idolLength);
	boss.search.resize(idolLength);

	stage.stage.resize(stagesize);
	stage.stage_data.resize(stagesize);
	stage.background.resize(2);

	menu.background.resize(4);

	stageparameter.doorlist.resize(stagesize);

	ui.images.resize(5);
	ui.title.resize(2);
}


ResourceController::~ResourceController()
{
	for (unsigned int i = 0; i < stage.stage_data.size(); i++){
		cvReleaseImage(&stage.stage_data[i]);
	}
}


bool ResourceController::Initialize()
{
	std::string filepath;
	//アイテム
	filepath = R_ITEM + "gold.png";
	LoadTexture(filepath, &item.gold);
	filepath = R_ITEM + "kunai.png";
	LoadTexture(filepath, &item.kunai);
	filepath = R_ITEM + "makibishi.png";
	LoadTexture(filepath, &item.makibishi);
	filepath = R_ITEM + "neko.png";
	LoadTexture(filepath, &item.neko);
	filepath = R_ITEM + "key.png";
	LoadTexture(filepath, &item.key);
	
	//オブジェクト
	filepath = R_OBJECT + "box.png";
	LoadTexture(filepath, &stageObject.box);

	filepath = R_OBJECT + "ladder.png";
	LoadTexture(filepath, &stageObject.ladder);

	filepath = R_OBJECT + "door.png";
	LoadTexture(filepath, &stageObject.door);
	
	//プレイヤー
	for (GLuint i = 0; i < player.idol.size(); i++){
		filepath = R_CHARACTER + "Player_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.idol[i]);
	}
	for (GLuint i = 0; i < player.walk.size(); i++){
		filepath = R_CHARACTER + "Player_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.walk[i]);
	}
	for (GLuint i = 0; i < player.dash.size(); i++){
		filepath = R_CHARACTER + "Player_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.dash[i]);
	}
	for (GLuint i = 0; i < player.ladder.size(); i++){
		filepath = R_CHARACTER + "Player_hashigo\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.ladder[i]);
	}
	for (GLuint i = 0; i < player.attack.size(); i++){
		filepath = R_CHARACTER + "Player_attack\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.attack[i]);
	}
	for (GLuint i = 0; i < player.shinobi.size(); i++){
		filepath = R_CHARACTER + "Player_shinobiashi\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.shinobi[i]);
	}
	for (GLuint i = 0; i < player.found.size(); i++){
		filepath = R_CHARACTER + "Player_found\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.found[i]);
	}
	filepath = R_CHARACTER + "Player_jump\\1.png";
	LoadTexture(filepath, &player.jump);

	for (GLuint i = 0; i < player.furoshiki.size(); i++){
		filepath = R_CHARACTER + "Player_furoshiki\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &player.furoshiki[i]);
	}

	//Enemy1のグラフィック
	for (GLuint i = 0; i < enemy1.idol.size(); i++){
		filepath = R_ENEMY + "Enemy1_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.idol[i]);
	}
	for (GLuint i = 0; i < enemy1.walk.size(); i++){
		filepath = R_ENEMY + "Enemy1_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.walk[i]);
	}
	for (GLuint i = 0; i < enemy1.dash.size(); i++){
		filepath = R_ENEMY + "Enemy1_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.dash[i]);
	}
	for (GLuint i = 0; i < enemy1.ladder.size(); i++){
		filepath = R_ENEMY + "Enemy1_hashigo\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.ladder[i]);
	}
	for (GLuint i = 0; i < enemy1.found.size(); i++){
		filepath = R_ENEMY + "Enemy1_found\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.found[i]);
	}
	for (GLuint i = 0; i < enemy1.search.size(); i++){
		filepath = R_ENEMY + "Enemy1_search\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy1.search[i]);
	}

	//Enemy2のグラフィック
	for (GLuint i = 0; i < enemy2.idol.size(); i++){
		filepath = R_ENEMY + "Enemy2_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.idol[i]);
	}
	for (GLuint i = 0; i < enemy2.walk.size(); i++){
		filepath = R_ENEMY + "Enemy2_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.walk[i]);
	}
	for (GLuint i = 0; i < enemy2.dash.size(); i++){
		filepath = R_ENEMY + "Enemy2_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.dash[i]);
	}
	for (GLuint i = 0; i < enemy2.ladder.size(); i++){
		filepath = R_ENEMY + "Enemy2_hashigo\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.ladder[i]);
	}
	for (GLuint i = 0; i < enemy2.found.size(); i++){
		filepath = R_ENEMY + "Enemy2_found\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.found[i]);
	}
	for (GLuint i = 0; i < enemy2.search.size(); i++){
		filepath = R_ENEMY + "Enemy2_search\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy2.search[i]);
	}

	//Enemy3のグラフィック
	for (GLuint i = 0; i < enemy3.idol.size(); i++){
		filepath = R_ENEMY + "Enemy3_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.idol[i]);
	}
	for (GLuint i = 0; i < enemy3.walk.size(); i++){
		filepath = R_ENEMY + "Enemy3_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.walk[i]);
	}
	for (GLuint i = 0; i < enemy3.dash.size(); i++){
		filepath = R_ENEMY + "Enemy3_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.dash[i]);
	}
	for (GLuint i = 0; i < enemy3.ladder.size(); i++){
		filepath = R_ENEMY + "Enemy3_hashigo\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.ladder[i]);
	}
	for (GLuint i = 0; i < enemy3.found.size(); i++){
		filepath = R_ENEMY + "Enemy3_found\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.found[i]);
	}
	for (GLuint i = 0; i < enemy3.search.size(); i++){
		filepath = R_ENEMY + "Enemy3_search\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy3.search[i]);
	}

	//Enemy4のグラフィック
	for (GLuint i = 0; i < enemy4.idol.size(); i++){
		filepath = R_ENEMY + "Enemy4_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.idol[i]);
	}
	for (GLuint i = 0; i < enemy4.walk.size(); i++){
		filepath = R_ENEMY + "Enemy4_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.walk[i]);
	}
	for (GLuint i = 0; i < enemy4.dash.size(); i++){
		filepath = R_ENEMY + "Enemy4_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.dash[i]);
	}
	for (GLuint i = 0; i < enemy4.ladder.size(); i++){
		filepath = R_ENEMY + "Enemy4_hashigo\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.ladder[i]);
	}
	for (GLuint i = 0; i < enemy4.found.size(); i++){
		filepath = R_ENEMY + "Enemy4_found\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.found[i]);
	}
	for (GLuint i = 0; i < enemy4.search.size(); i++){
		filepath = R_ENEMY + "Enemy4_search\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &enemy4.search[i]);
	}

	//Bossのグラフィック
	for (GLuint i = 0; i < boss.idol.size(); i++){
		filepath = R_ENEMY + "Boss_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.idol[i]);
	}
	for (GLuint i = 0; i < boss.walk.size(); i++){
		filepath = R_ENEMY + "Boss_walk\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.walk[i]);
	}
	for (GLuint i = 0; i < boss.dash.size(); i++){
		filepath = R_ENEMY + "Boss_dash\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.dash[i]);
	}

	for (GLuint i = 0; i < boss.ladder.size(); i++){
		filepath = R_ENEMY + "Boss_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.ladder[i]);
	}
	for (GLuint i = 0; i < boss.found.size(); i++){
		filepath = R_ENEMY + "Boss_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.found[i]);
	}
	for (GLuint i = 0; i < boss.search.size(); i++){
		filepath = R_ENEMY + "Boss_idol\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &boss.search[i]);
	}

	//メニュー背景
	for (GLuint i = 0; i < menu.background.size(); i++){
		filepath = R_MENU + std::to_string(i) + ".png";
		LoadTexture(filepath, &menu.background[i]);
	}
	filepath = R_MENU + "title.png";
	LoadTexture(filepath, &menu.title);

	filepath = R_MENU + "over.png";
	LoadTexture(filepath, &menu.over);

	filepath = R_MENU + "clear.png";
	LoadTexture(filepath, &menu.clear);

	//ロード画像
	filepath = R_MENU + "loading.png";
	LoadTexture(filepath, &menu.loading);

	//ステージ画像
	for (GLuint i = 0; i < stage.stage.size(); i++){
		filepath = R_STAGE + "Graphic\\" + std::to_string(i) + ".png";
		LoadTexture(filepath, &stage.stage[i]);
	}
	//ステージデータ
	for (GLuint i = 0; i < stage.stage_data.size(); i++){
		filepath = R_STAGE + "Data\\" + std::to_string(i) + "_data.png";
		stage.stage_data[i] = cvLoadImage(filepath.c_str(), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	}
	//背景画像
	for (GLuint i = 0; i < stage.background.size(); i++){
		filepath = R_STAGE + "BackGround\\" + std::to_string(i) + ".png";
		LoadTexture(filepath, &stage.background[i]);
	}
	//ライト（フィルター）画像
	filepath = R_LIGHT + "on.png";
	LoadTexture(filepath, &filter.on);
	filepath = R_LIGHT + "off.png";
	LoadTexture(filepath, &filter.off);


	//コンフィグの読み込み
	std::ifstream ifs;
	std::string str;

	//パラメータの読み込み
	ifs.open("../config.txt");
	if (!ifs.fail())
	{
		while (getline(ifs, str)){
			SetParameter(str);
		}
		
	}
	ifs.close();

	//ステージ設定の読み込み
	ifs.open(R_CONFIG + "door.txt");
	if (!ifs.fail())
	{

	}
	ifs.close();
	
	//
	if (stage_param.stage_limits >= 0){
		int num = stage.stage.size();
		stage.stage.resize(stage_param.stage_limits);
		stage.stage_data.resize(stage_param.stage_limits);
		//ステージ画像
		for (GLuint i = num; i < stage.stage.size(); i++){
			filepath = R_STAGE + "Graphic\\" + std::to_string(i) + ".png";
			LoadTexture(filepath, &stage.stage[i]);
		}
		//ステージデータ
		for (GLuint i = num; i < stage.stage_data.size(); i++){
			filepath = R_STAGE + "Data\\" + std::to_string(i) + "_data.png";
			stage.stage_data[i] = cvLoadImage(filepath.c_str(), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		}
	}

	//キャラクターUI
	for (GLuint i = 0; i < ui.images.size(); i++){
		filepath = R_UI + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &ui.images[i]);
	}
	//タイトルUI
	for (GLuint i = 0; i < ui.title.size(); i++){
		filepath = R_UI+ "title\\" + std::to_string(i + 1) + ".png";
		LoadTexture(filepath, &ui.title[i]);
	}

	return true;

}

bool ResourceController::LoadTexture(std::string& filepath, GLuint* texture_id){
	IplImage* image;
	image = cvLoadImage(filepath.c_str(), CV_LOAD_IMAGE_UNCHANGED | CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	if (image == nullptr)
	{
		//エラー処理
		MessageBox(NULL, TEXT("cvLoadImage"), TEXT("確認"), MB_OK);
		assert(image->imageData == nullptr);
	}
	//テクスチャIDをOpenGLから取得
	glGenTextures(1, texture_id);
	//テクスチャIDにテクスチャを設定する
	glBindTexture(GL_TEXTURE_2D, *texture_id);
	//テクスチャ処理の設定
	if (image->nChannels == 4){
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_BGRA, GL_UNSIGNED_BYTE, image->imageData);
	}
	else{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_BGR, GL_UNSIGNED_BYTE, image->imageData);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	cvReleaseImage(&image);
	return true;
}

bool ResourceController::SetParameter(std::string text){
	
	//パラメータで読み込みしたい項目をここに書く
	std::string keyword; //取得するパラメータ名

	//----------Playerのパラメータ読み込み----------
	keyword = "player_debugMode";
	InputTextValue(text, keyword, &parameter.player_param.debugMode);

	keyword = "player_speed";
	InputTextValue(text, keyword, &parameter.player_param.speed);

	keyword = "player_dashMinSpeed";
	InputTextValue(text, keyword, &parameter.player_param.dashMinSpeed);
	
	keyword = "player_dashMaxSpeed";
	InputTextValue(text, keyword, &parameter.player_param.dashMaxSpeed);

	keyword = "player_dashAdd";
	InputTextValue(text, keyword, &parameter.player_param.dashAdd);

	keyword = "player_dashSub";
	InputTextValue(text, keyword, &parameter.player_param.dashSub);

	keyword = "player_shinobiSpeed";
	InputTextValue(text, keyword, &parameter.player_param.slowSpeed);

	keyword = "player_jump";
	InputTextValue(text, keyword, &parameter.player_param.jump);
	
	keyword = "player_hp";
	InputTextValue(text, keyword, &parameter.player_param.hp);

	keyword = "player_wallet";
	InputTextValue(text, keyword, &parameter.player_param.wallet);

	keyword = "player_weaponInterval";
	InputTextValue(text, keyword, &parameter.player_param.weaponInterval);

	keyword = "player_weaponSwitchNum";
	InputTextValue(text, keyword, &parameter.player_param.weaponSwitchNum);

	//----------Stageのパラメータ読み込み----------
	keyword = "stage_limits";
	InputTextValue(text, keyword, &stage_param.stage_limits);

	//----------HookShotのパラメータ読み込み----------
	keyword = "hookshot_speed";
	InputTextValue(text, keyword, &parameter.itemparam.hookshot.speed);

	keyword = "hookshot_limit";
	InputTextValue(text, keyword, &parameter.itemparam.hookshot.limit);

	//----------クナイのパラメータ読み込み----------
	keyword = "kunai_speed";
	InputTextValue(text, keyword, &parameter.kunai.speed);

	keyword = "kunai_power";
	InputTextValue(text, keyword, &parameter.kunai.power);

	keyword = "kunai_activeFrameNum";
	InputTextValue(text, keyword, &parameter.kunai.activeFrameNum);
	
	//----------マキビシのパラメータ読み込み----------
	keyword = "makibishi_speed";
	InputTextValue(text, keyword, &parameter.makibishi.speed);

	keyword = "makibishi_power";
	InputTextValue(text, keyword, &parameter.makibishi.power);

	keyword = "makibishi_activeFrameNum";
	InputTextValue(text, keyword, &parameter.makibishi.activeFrameNum);

	//----------Enemy1のパラメータ読み込み----------
	std::string enemyName = "enemy1_";
	keyword = enemyName + "speed";
	InputTextValue(text, keyword, &parameter.enemy1_param.speed);

	keyword = enemyName + "chaseSpeed";
	InputTextValue(text, keyword, &parameter.enemy1_param.chaseSpeed);

	keyword = enemyName + "jump";
	InputTextValue(text, keyword, &parameter.enemy1_param.jump);

	keyword = enemyName + "hp";
	InputTextValue(text, keyword, &parameter.enemy1_param.hp);

	keyword = enemyName + "power";
	InputTextValue(text, keyword, &parameter.enemy1_param.power);

	keyword = enemyName + "waitFrameNum";
	InputTextValue(text, keyword, &parameter.enemy1_param.waitFrameNum);

	keyword = enemyName + "moveFrameNum";
	InputTextValue(text, keyword, &parameter.enemy1_param.moveFrameNum);

	keyword = enemyName + "searchRangeX";
	InputTextValue(text, keyword, &parameter.enemy1_param.searchRangeX);

	keyword = enemyName + "searchRangeY";
	InputTextValue(text, keyword, &parameter.enemy1_param.searchRangeY);

	//----------Enemy2のパラメータ読み込み----------
	enemyName = "enemy2_";
	keyword = enemyName + "speed";
	InputTextValue(text, keyword, &parameter.enemy2_param.speed);

	keyword = enemyName + "chaseSpeed";
	InputTextValue(text, keyword, &parameter.enemy2_param.chaseSpeed);

	keyword = enemyName + "jump";
	InputTextValue(text, keyword, &parameter.enemy2_param.jump);

	keyword = enemyName + "hp";
	InputTextValue(text, keyword, &parameter.enemy2_param.hp);

	keyword = enemyName + "power";
	InputTextValue(text, keyword, &parameter.enemy2_param.power);

	keyword = enemyName + "waitFrameNum";
	InputTextValue(text, keyword, &parameter.enemy2_param.waitFrameNum);

	keyword = enemyName + "moveFrameNum";
	InputTextValue(text, keyword, &parameter.enemy2_param.moveFrameNum);

	keyword = enemyName + "searchRangeX";
	InputTextValue(text, keyword, &parameter.enemy2_param.searchRangeX);

	keyword = enemyName + "searchRangeY";
	InputTextValue(text, keyword, &parameter.enemy2_param.searchRangeY);

	//----------Enemy3のパラメータ読み込み----------
	enemyName = "enemy3_";
	enemyName + "speed";
	InputTextValue(text, keyword, &parameter.enemy3_param.speed);

	keyword = enemyName + "chaseSpeed";
	InputTextValue(text, keyword, &parameter.enemy3_param.chaseSpeed);

	keyword = enemyName + "jump";
	InputTextValue(text, keyword, &parameter.enemy3_param.jump);

	keyword = enemyName + "hp";
	InputTextValue(text, keyword, &parameter.enemy3_param.hp);

	keyword = enemyName + "power";
	InputTextValue(text, keyword, &parameter.enemy3_param.power);

	keyword = enemyName + "waitFrameNum";
	InputTextValue(text, keyword, &parameter.enemy3_param.waitFrameNum);

	keyword = enemyName + "moveFrameNum";
	InputTextValue(text, keyword, &parameter.enemy3_param.moveFrameNum);

	keyword = enemyName + "searchRangeX";
	InputTextValue(text, keyword, &parameter.enemy3_param.searchRangeX);

	keyword = enemyName + "searchRangeY";
	InputTextValue(text, keyword, &parameter.enemy3_param.searchRangeY);

	//----------Enemy4のパラメータ読み込み----------
	enemyName = "enemy4_";
	enemyName + "speed";
	InputTextValue(text, keyword, &parameter.enemy4_param.speed);

	keyword = enemyName + "chaseSpeed";
	InputTextValue(text, keyword, &parameter.enemy4_param.chaseSpeed);

	keyword = enemyName + "jump";
	InputTextValue(text, keyword, &parameter.enemy4_param.jump);

	keyword = enemyName + "hp";
	InputTextValue(text, keyword, &parameter.enemy4_param.hp);

	keyword = enemyName + "power";
	InputTextValue(text, keyword, &parameter.enemy4_param.power);

	keyword = enemyName + "waitFrameNum";
	InputTextValue(text, keyword, &parameter.enemy4_param.waitFrameNum);

	keyword = enemyName + "moveFrameNum";
	InputTextValue(text, keyword, &parameter.enemy4_param.moveFrameNum);

	keyword = enemyName + "searchRangeX";
	InputTextValue(text, keyword, &parameter.enemy4_param.searchRangeX);

	keyword = enemyName + "searchRangeY";
	InputTextValue(text, keyword, &parameter.enemy4_param.searchRangeY);

	//----------Bossのパラメータ読み込み----------
	enemyName = "boss_";
	enemyName + "speed";
	InputTextValue(text, keyword, &parameter.boss_param.speed);

	keyword = enemyName + "chaseSpeed";
	InputTextValue(text, keyword, &parameter.boss_param.chaseSpeed);

	keyword = enemyName + "jump";
	InputTextValue(text, keyword, &parameter.boss_param.jump);

	keyword = enemyName + "hp";
	InputTextValue(text, keyword, &parameter.boss_param.hp);

	keyword = enemyName + "power";
	InputTextValue(text, keyword, &parameter.boss_param.power);

	keyword = enemyName + "waitFrameNum";
	InputTextValue(text, keyword, &parameter.boss_param.waitFrameNum);

	keyword = enemyName + "moveFrameNum";
	InputTextValue(text, keyword, &parameter.boss_param.moveFrameNum);

	keyword = enemyName + "searchRangeX";
	InputTextValue(text, keyword, &parameter.boss_param.searchRangeX);

	keyword = enemyName + "searchRangeY";
	InputTextValue(text, keyword, &parameter.boss_param.searchRangeY);


	//----------その他のパラメータ読み込み----------
	keyword = "zoom_ratio";
	InputTextValue(text, keyword, &parameter.zoomratio);

	keyword = "skiptitle";
	InputTextValue(text, keyword, &parameter.skiptitle);

	return true;
}

//対象の文字列にkeywordが含まれていればその後に続く値を取得する
//[text: 対象の文字列, keyword: 検索文字列, variable: 値を保持するint型変数]
void ResourceController::InputTextValue(std::string text, std::string keyword, int* variable)
{
	int num = text.find(keyword);
	if (num != std::string::npos){
		text.erase(0, keyword.length());
		*variable = std::stoi(text);
	}
}

//対象の文字列にkeywordが含まれていればその後に続く値を取得する
//[text: 対象の文字列, keyword: 検索文字列, variable: 値を保持するfloat型変数]
void ResourceController::InputTextValue(std::string text, std::string keyword, float* variable)
{
	int num = text.find(keyword);
	if (num != std::string::npos){
		text.erase(0, keyword.length());
		*variable = std::stof(text);
	}
}

//対象の文字列にkeywordが含まれていればその後に続く値を取得する
//[text: 対象の文字列, keyword: 検索文字列, variable: 値を保持するbool型変数]
void ResourceController::InputTextValue(std::string text, std::string keyword, bool* variable)
{
	int num = text.find(keyword);
	if (num != std::string::npos){
		text.erase(0, keyword.length());
		*variable = (std::stoi(text) > 0 ? true : false);
	}
}