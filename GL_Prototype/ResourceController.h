#ifndef _RC_
#define _RC_

#include<string>
#include<vector>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <opencv2\opencv.hpp>
#include "Define.h"

#define R_OBJECT ((std::string)"../Resource\\Object\\")
#define R_ITEM ((std::string)"../Resource\\Item\\")
#define R_CHARACTER ((std::string)"../Resource\\Character\\")
#define R_ENEMY ((std::string)"../Resource\\Enemy\\")
#define R_STAGE ((std::string)"../Resource\\Stage\\")
#define R_LIGHT ((std::string)"../Resource\\Light\\")
#define R_MENU ((std::string)"../Resource\\Menu\\")
#define R_CONFIG (((std::string)"../Resource\\Config\\"))
#define R_UI ((std::string)"..\\Resource\\UI\\")

//シングルトンクラス
class ResourceController
{
public:
	static ResourceController* GetResourceController(){
		static ResourceController RC;
		return &RC;
	}
	bool Initialize();
	struct Item{
		GLuint gold;
		GLuint kunai;
		GLuint makibishi;
		GLuint neko;
		GLuint key;
	}item;
	struct StageObject{
		GLuint box;
		GLuint ladder;
		GLuint door;
	}stageObject;
	struct Player{
		std::vector<GLuint> idol;
		std::vector<GLuint> walk;
		std::vector<GLuint> shinobi;
		std::vector<GLuint> dash;
		std::vector<GLuint> ladder;
		std::vector<GLuint> attack;
		std::vector<GLuint> furoshiki;
		std::vector<GLuint> found;
		GLuint jump;
	}player;
	struct Enemy{
		std::vector<GLuint> idol;
		std::vector<GLuint> walk;
		std::vector<GLuint> dash;
		std::vector<GLuint> ladder;
		std::vector<GLuint> found;
		std::vector<GLuint> search;
	}enemy1, enemy2, enemy3, enemy4, boss;
	StageParamSet stage_param;
	struct Stage{
		std::vector<GLuint> stage;
		std::vector<IplImage*> stage_data;
		std::vector<GLuint> background;
		
	}stage;
	struct Filter{
		GLuint on;
		GLuint off;
	}filter;
	struct Parameter{
		PlayerParamSet player_param;
		ItemParamSet   itemparam;
		WeaponParamSet kunai;
		WeaponParamSet makibishi;
		EnemyParamSet  enemy1_param;
		EnemyParamSet  enemy2_param;
		EnemyParamSet  enemy3_param;
		EnemyParamSet  enemy4_param;
		EnemyParamSet  boss_param;
		float zoomratio;
		int skiptitle;
	}parameter;
	struct StageParameter{
		std::vector<std::vector<int>*> doorlist;
	}stageparameter;
	struct Menu{
		std::vector<GLuint> background;
		GLuint title;
		GLuint clear;
		GLuint over;
		GLuint loading;
	}menu;

	struct UI{
		std::vector<GLuint> images;
		std::vector<GLuint> title;
	}ui;
	struct SE{
		int clear;
		int key;
		int walk;
		int damage;
	}se;
private:
	bool LoadTexture(std::string& filepath, GLuint* texture_id);
	bool SetParameter(std::string);

	void InputTextValue(std::string text, std::string keyword, int* variable);
	void InputTextValue(std::string text, std::string keyword, float* variable);
	void InputTextValue(std::string text, std::string keyword, bool* variable);

	ResourceController();
	~ResourceController();
};

#endif