#ifndef _Scene_H_
#define _Scene_H_
#include "Stage.h"
#include "Menu.h"

class Game;

class Scene
{
public:
	static Scene* GetScene(){
		static Scene scene;
		return &scene;
	}
	~Scene();
	bool Initialize(Game*);
	int UpDate();
	/*現在のステージ*/
	Stage* Now();
	/*シーンを変更する*/
	bool StageChange(int num);
	bool MenuChange(unsigned int num);
	bool inStage();
	bool ClearFlag();
	bool setClear(bool b = 1);
	int keyhr;
	int clearhr;
private:
	Scene();
	void StageSetup();
	bool AutoMove();
	Game* game;
	ResourceController* RC;
	std::vector<Menu*> menuList;
	std::vector<Stage> stageList;
	Vertex* loading;
	std::string s1;
	std::string s2;
	Title title;
	Stage* stage1;
	Stage* stage2;
	Stage* now;
	bool gameClear;
	bool changeflag;
	int stage_num;
	int before_stage_num;
	int menu_num;
	bool TitleUpdate();
};

#endif