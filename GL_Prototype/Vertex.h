#ifndef _Vertex_H_
#define _Vertex_H_


#include <GL/glew.h>
#include <GL/glfw.h>
#include <opencv2\opencv.hpp>

#include<vector>
#include<iterator>

#define ARRAY_LENGTH(array)(sizeof(array)/sizeof(array[0]))
/*頂点とテクスチャを扱うクラス*/
class Vertex
{
public:

	Vertex::Vertex();
	/*Vertex(テクスチャのない四角形)*/
	//Vertex::Vertex(const GLint x, const GLint y, int sizeX, int sizeY, GLfloat R = 1.0f, GLfloat G = 1.0f, GLfloat B = 1.0f, GLfloat A = 1.0f);

	/*Vertex(テクスチャ)*/
	Vertex::Vertex(const int x, const int y, GLuint* texture_id);

	Vertex::Vertex(const int x, const int y, const int width, const int height, GLuint* texture_id ,bool loop_S = false, bool loop_T = false);

	Vertex(const Vertex &v);

	Vertex& operator=(const Vertex &v);

	~Vertex();
	
	//テクスチャを識別子持っているか
	inline bool UseTexture(){
		if (texture_id != NULL)
			return true;
		else
			return false;
	}
	/*頂点を取得*/
	inline const GLfloat* GetVertex()
	{
		return &(vertex[0]);
	};
	//テクスチャIDを取得
	inline const GLuint GetTextureID()
	{
		return texture_id;
	}
	inline GLfloat* GetTextureUV()
	{
		return &(texture_uv[0]);
	}
	inline int GetX()
	{
		return (int)vertex[0];
	}
	inline int GetY()
	{
		return (int)vertex[1];
	}
	inline int GetWidth()
	{
		return width;
	}
	inline int GetHeight()
	{
		return height;
	}
	inline const GLfloat* GetColor()
	{
		return &(color[0]);
	}
	void SetPosition(float x , float y )
	{
		vertex[0] = vertex[6] = (float)x;
		vertex[1] = vertex[3] = (float)y;
		SetupTexture(0, 0, 0);
	}
	/*描画情報を更新する*/
	/*SetDraw(X座標,Y座標,反転,アルファ値(0.0f~1.0f))*/
	Vertex* SetDraw(float x, float y, bool turn = 0, float alpha = 1.0f);
	/*アルファチャンネルを変更する(0.0f~1.0f)*/
	void ChangeAlpha(GLfloat alpha);
	/*カラーを設定する*/
	/*SetColor(赤,緑,青,アルファ)*/
	void SetColor(float R, float G, float B, float A);

private:
	/*テクスチャの設定*/
	bool SetupTexture(bool setSize, bool loop_S,bool loop_T);
	std::vector<GLfloat> vertex;	//頂点配列
	GLuint texture_id;				//テクスチャID
	std::vector<GLfloat> texture_uv;//テクスチャ頂点
	std::string filename;					//ファイル名
	GLuint width;					//幅
	GLuint height;					//高さ
	std::vector<GLfloat> color;					//テクスチャが設定されていない場合のみ利用
};

#endif