#include "Screen.h"
#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/GL.h>
#include <GL/GLU.h>

Screen::Screen()
{
}


Screen::~Screen()
{
	
}

void Screen::SetScreen(int w,int h){
	width = w;
	height = h;
	right = (float)width;
	bottom = (float)height;
}

void Screen::Scroll(){
	glLoadIdentity();
	gluOrtho2D(left +x, right +x, bottom +y , top +y);
}

void Screen::Zoom(int num){
	zoom += num;
	left += num;
	top += num;
	right -= num;
	bottom -= num;
	scale = ((float)width-zoom) / width;
	gluOrtho2D(left + x, right + x, bottom + y, top + y);
}