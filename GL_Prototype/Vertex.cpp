#include<Windows.h>	//メッセージボックス表示のため
#include "Vertex.h"
#include<fstream>
#include<vector>
Vertex::Vertex(){
}

///*Vertex(テクスチャのない四角形)*/
//Vertex::Vertex(const GLint x, const GLint y, int sizeX,int sizeY, GLfloat R, GLfloat G, GLfloat B, GLfloat A)
//{
//	vertex.resize(8);
//	vertex[0] = vertex[6] = (GLfloat)x;
//	vertex[2] = vertex[4] = (GLfloat)x + sizeX;
//	vertex[1] = vertex[3] = (GLfloat)y;
//	vertex[5] = vertex[7] = (GLfloat)y + sizeY;
//
//	width = sizeX;
//	height = sizeY;
//
//	SetColor(R, G, B, A);
//
//	texture_id = NULL;
//}

Vertex::Vertex(const int x, const int y, GLuint* tex_id)
{
	vertex.resize(8);
	vertex[0] = vertex[6] = (GLfloat)x;
	vertex[1] = vertex[3] = (GLfloat)y;

	texture_id = *tex_id;

	float R, G, B, A;
	R = G = B = 255.0f;
	A = 1.0f;
	color = {
		R, G, B, A,
		R, G, B, A,
		R, G, B, A,
		R, G, B, A
	};

	SetColor(255, 255, 255, 1.0f);

	SetupTexture(1, 0, 0);
}
Vertex::Vertex(const int x, const int y, const int w, const int h, GLuint* tex_id, bool LOOP_S, bool LOOP_T)
{

	width = w;
	height = h;
	vertex.resize(8);
	vertex[0] = vertex[6] = (GLfloat)x;
	vertex[1] = vertex[3] = (GLfloat)y;
	//	vertex[2] = vertex[4] = (GLfloat)x + width;
	//	vertex[5] = vertex[7] = (GLfloat)y + height;


	texture_id = *tex_id;

	float R, G, B, A;
	R = G = B = 255.0f;
	A = 1.0f;

	SetColor(R,G,B,A);

	SetupTexture(0, LOOP_S, LOOP_T);

}

Vertex::Vertex(const Vertex &v){
	vertex = v.vertex;	//頂点配列
	texture_id = v.texture_id;				//テクスチャID
	texture_uv = v.texture_uv;//テクスチャ頂点
	filename = v.filename;					//ファイル名
	width = v.width;					//幅
	height = v.height;					//高さ
	color = v.color;					//テクスチャが設定されていない場合のみ利用
}

Vertex& Vertex::operator=(const Vertex &v){
	Vertex();
	vertex= v.vertex;	//頂点配列
	texture_id = v.texture_id;				//テクスチャID
	texture_uv = v.texture_uv;//テクスチャ頂点
	filename = v.filename;					//ファイル名
	width = v.width;					//幅
	height = v.height;					//高さ
	color = v.color;					//テクスチャが設定されていない場合のみ利用
	return (*this);
}

Vertex::~Vertex()
{
	//IplImage*型の画像データをリリース
	//cvReleaseImage(&image);
}
/*テクスチャを設定する*/
bool Vertex::SetupTexture(bool setSize, bool loop_S, bool loop_T){
	GLint img_width = width, img_height = height;
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &img_width);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &img_height);
	if (setSize){
		width = img_width;
		height = img_height;
	}

	//画像の幅と高さから頂点を代入する
	vertex[2] = vertex[4] = vertex[0] + (GLfloat)width;
	vertex[5] = vertex[7] = vertex[1] + (GLfloat)height;


	GLfloat uv_width, uv_height;
	if (loop_S){
		uv_width = (float)width / img_width;
	}
	else{
		uv_width = 1.0f;
	}

	if (loop_T){
		uv_height = (float)height / img_height;
	}
	else{
		uv_height = 1.0f;
	}


	//テクスチャ頂点を設定
	texture_uv = {
		0.0f, 0.0f,
		uv_width, 0.0f,
		uv_width, uv_height,
		0.0f, uv_height,
	};


	//cvReleaseImage(&image);

 	return true;
}


/*描画の位置が変わる際に使用*/
Vertex* Vertex::SetDraw(float x, float y, bool direction, float alpha){
	if (direction == 0){
		vertex[0] = vertex[6] = x;
		vertex[1] = vertex[3] = y;
		vertex[2] = vertex[4] = x + width;
		vertex[5] = vertex[7] = y + height;
	}
	else if (direction == 1){
		vertex[0] = vertex[6] = x + width;
		vertex[1] = vertex[3] = y;
		vertex[2] = vertex[4] = x;
		vertex[5] = vertex[7] = y + height;
	}
	ChangeAlpha(alpha);
	return this;
}
void Vertex::ChangeAlpha(float a){
	color = {
		255, 255, 255, a,
		255, 255, 255, a,
		255, 255, 255, a,
		255, 255, 255, a
	};
}
void Vertex::SetColor(float R, float G, float B, float A){
	color = {
		R, G, B, A,
		R, G, B, A,
		R, G, B, A,
		R, G, B, A
	};
}
