#include "Player.h"
//#include <string>

#include "Animation.h"
#include "ResourceController.h"
#include "Stage.h"
#include "Screen.h"
#include "StageItem.h"

#include "Weapon.h"

Player::Player(float posX, float posY, float width, float height, float speed, float jumpF, Direction dir, int drRank, int hp, bool useGravity)
	:Character(posX, posY, width, height, speed, jumpF, dir, drRank, hp, useGravity)
{
}

void Player::CreatePlayer()
{
	dr = Draw::GetDraw();

	//スクリーンクラスのインスタンスを取得
	screen = Screen::GetScreen();

	//アニメーショングラフィックを設定
	SetAnimationGraphic();

	//武器クラスの生成
	kunai = new Weapon(50.0f, 30.0f, 16.0f, 0, 40);
	kunai->CreateWeapon(ItemType::Kunai);

	makibishi = new Weapon(50.0f, 30.0f, 16.0f, 0, 40);
	makibishi->CreateWeapon(ItemType::Makibishi);

	//フックショットクラスの用意
	hookshot.Initialize();

	walkhr = SoundManager::GetSoundManager()->SetSound("walk.wav");
	damagehr = SoundManager::GetSoundManager()->SetSound("damage.wav");
	furoshikihr = SoundManager::GetSoundManager()->SetSound("furoshiki.wav");
}

void Player::LoadResourceControllerValue()
{
	ResourceController* rc = ResourceController::GetResourceController();

	debugMode            = rc->parameter.player_param.debugMode;
	speed                = CHECK_VALUE(rc->parameter.player_param.speed, -1.0f, 2.6f);
	fastMinSpeed         = CHECK_VALUE(rc->parameter.player_param.dashMinSpeed, -1.0f, speed * 1.4f);
	fastMaxSpeed         = CHECK_VALUE(rc->parameter.player_param.dashMaxSpeed, -1.0f, speed * 3.0f);
	fastAdd              = CHECK_VALUE(rc->parameter.player_param.dashAdd, -1.0f, speed * 0.1f);
	fastSub              = CHECK_VALUE(rc->parameter.player_param.dashSub, -1.0f, speed * 0.2f);
	slowSpeed            = CHECK_VALUE(rc->parameter.player_param.slowSpeed, -1.0f, speed * 0.5f);
	jumpForce            = CHECK_VALUE(rc->parameter.player_param.jump, -1.0f, 16.0f);
	hp                   = CHECK_VALUE(rc->parameter.player_param.hp, -1, 100);
	wallet               = CHECK_VALUE(rc->parameter.player_param.wallet, -1, 0);
	weaponInterval       = CHECK_VALUE(rc->parameter.player_param.weaponInterval, -1, 50);
	weaponSwitchFrameNum = CHECK_VALUE(rc->parameter.player_param.weaponSwitchNum, -1, 20);

}

Weapon* Player::GetWeapon(ItemType name)
{
	switch (name){
	case ItemType::Kunai:
		return kunai;
		break;

	case ItemType::Makibishi:
		return makibishi;
		break;

	default:
		return nullptr;
	}
}

void Player::Initialize()
{
	direction = startDir;		//初期方向設定
	animationState = Idol;		//アニメーションの状態を初期化
	fastSpeed = fastMinSpeed;

	//アニメーションクラスの初期化
	idolAnim->Initialize();
	normalMoveAnim->Initialize();
	slowMoveAnim->Initialize();
	fastMoveAnim->Initialize();
	ladderAnim->Initialize();
	//Init jumpAnim
	furoshikiAnim->SetPlayMode(AnimPlayMode::NormalPlay);
	furoshikiAnim->Initialize();
	attackAnim->Initialize();
	foundAnim->Initialize();

	searchPosY = Y_CENTER;		//ステージの感知座標を設定

	//武器関係の初期化
	kunai->Initialize();
	makibishi->Initialize();
	weaponFrameCounter = -1;
	weaponKeyCounter = 0;

	//UI表示位置の初期化
	uiOffsetX = 50.0f;
	uiOffsetY = 100.0f;
	uiDelta = 0.73f;

	gameOverFlag = false;			//ゲームオーバーフラグの初期化
	prevFindCount = findCount = 0;	//Enemyからの発見数を初期化
	ScrollScreen();					//初期位置にスクロールしておく
}

void Player::Update(Stage* stage, bool inStage)
{
	if (!inStage){ return; }

	UpdateKeyState();

	prevX = x;
	prevY = y;

	hideRate = 0.0f;

	LifeCheck();

	//接地中に発見されたらFoundアニメーションをする
	if (onTheGround && findCount > 0 && prevFindCount == 0 || animationState == State::Found){
		animationState = State::Found;
		if (foundAnim->EndOfAnimation()){
			foundAnim->Initialize();
			animationState = Idol;
		}
	}
	else{ animationState = State::Idol; }

	UseItem(stage);
	if (useHook){
		animationState = State::Jump;
	}
	else {
		Move(stage);
		if (damageFlag && direction == enemyDir){
			x = prevX;
			animationState = State::Idol;
			if (calldamage == false){
				SoundManager::GetSoundManager()->CallSound(damagehr);
				calldamage = true;
			}
		}
		else{
			calldamage = false;
		}
		y += fallSpeed;
		checkCollisionStage(stage);	//ステージとの接触処理

		if (!onTheGround && (onTheLadder && fallSpeed == 0.0f)){	//空中かつハシゴにつかまっているとき
			animationState = Ladder;
			hideRate = 0.0f;	//ハシゴに掴まっているときは隠れられない
		}
		else if (!onTheGround && !attacking){		//空中にいるとき
			animationState = Jump;
			hideRate = 0.0f;	//ジャンプ中は隠れられない
		}
	}
	useHook = hookshot.Update(this,x,y);
	ScrollScreen();	//スクロール制御

	UpdateAnimation();	//アニメーションの更新
	PrintUI();

	prevFindCount = findCount;
	findCount = 0;	//Enemyからの発見数をリセット
	damageFlag = false;

	//Weaponクラスの更新
	kunai->Update(stage);
	makibishi->Update(stage);
	if (weaponFrameCounter >= 0){ weaponFrameCounter--; }
}

//キーの状態を更新する
void Player::UpdateKeyState()
{
	prevKeyState = keyState;

	//移動関係キー
	keyState.left = glfwGetKey(MOVE_KEY_LEFT) == GLFW_PRESS;
	keyState.right = glfwGetKey(MOVE_KEY_RIGHT) == GLFW_PRESS;
	keyState.up = glfwGetKey(MOVE_KEY_UP) == GLFW_PRESS;
	keyState.down = glfwGetKey(MOVE_KEY_DOWN) == GLFW_PRESS;
	keyState.jump = glfwGetKey(ACTION_KEY_JUMP) == GLFW_PRESS;
	keyState.shinobi = glfwGetKey(ACTION_KEY_SHINOBI) == GLFW_PRESS;
	keyState.dash = glfwGetKey(ACTION_KEY_DASH) == GLFW_PRESS;

	//特殊アクションキー
	keyState.furoshiki = glfwGetMouseButton(ITEM_KEY_FUROSHIKI) == GLFW_PRESS;
	keyState.attack = glfwGetKey(ITEM_KEY_WEAPON) == GLFW_PRESS;

	//ステージチェックキー
	keyState.checkStage = glfwGetKey(STAGE_CHECK) == GLFW_PRESS;
}

void Player::Move(Stage* stage)
{
	//移動キーの入力で変化するフラグを初期化
	moving = false;
	stealthMoving = false;

	if (animationState == State::Found){ return; }		//fondアニメーション中は以下を処理しない
	if (usingFuroshiki || attacking) { return; }		//アイテム使用中・攻撃モーション中は以下を処理しない
	
	//float moveSpeed = speed;	//通常の移動速度

	int hDir = 0;	//水平移動方向(directionだけでは停止(0)が設定できない)

	if (keyState.left && !keyState.right){
		hDir = direction = Left;
		animationState = (keyState.shinobi ? State::Shinobi : State::Walk);
		moving = true;
		moveSpeed = speed;
	}
	else if (keyState.right && !keyState.left){
		hDir = direction = Right;
		animationState = (keyState.shinobi ? State::Shinobi : State::Walk);
		moving = true;
		moveSpeed = speed;
	}
	else if ((!keyState.right && !keyState.left) && moveSpeed <= fastMinSpeed){	//ダッシュ慣性が働いていない場合
		hDir = 0;
		moveSpeed = 0;
	}

	//ダッシュ押されていない場合(ダッシュ慣性が有効な場合)
	if (!keyState.dash || (keyState.dash && !keyState.left && !keyState.right)){
		fastSpeed -= fastSpeed * fastSub;	//ダッシュ速度を初期値に戻す
		if (fastSpeed <= fastMinSpeed){
			fastSpeed = speed;			//ダッシュ下限値を下回ると通常の移動速度にする
		}
		moveSpeed = fastSpeed;
	}
	//ダッシュキーが押されていたら
	else if (keyState.dash && hDir != 0){
		fastSpeed += fastSpeed * fastAdd;//どんどん速くする

		if (fastSpeed >= fastMaxSpeed)
			fastSpeed = fastMaxSpeed;	//上限値を超えると上がらない
		moveSpeed = fastSpeed;			//ダッシュの移動速度に変更
	}

	//忍びキーが押されたら
	if (keyState.shinobi && (moveSpeed < fastMinSpeed)){
		moveSpeed = slowSpeed;	//忍び移動中の移動速度に変更
		animationState = State::Shinobi;
		stealthMoving = true;
	}
	x += hDir * moveSpeed;	//X座標を更新する

	if (onTheLadder && fallSpeed == 0.0f){
		//ハシゴでの移動
		if (keyState.up && !keyState.down){
			y -= speed / 2.0f;		//移動速度の1/2の速度でハシゴを登れる
			if (!stage->GetStageData(S_CAST_I(X_CENTER), S_CAST_I(BOTTOM)) == 1){	//ハシゴの一番上まで登ったとき
				y = prevY;
			}
			else{
				moving = true;
			}
		}
		else if (keyState.down && !keyState.up){
			y += speed / 2.0f;		//移動速度の1/2の速度でハシゴを降りられる
			if (!stage->GetStageData(S_CAST_I(X_CENTER), S_CAST_I(BOTTOM)) == 1){	//ハシゴの一番下まで降りたとき
				onTheLadder = false;
			}
			else{
				moving = true;
			}
		}
	}
	else{
		onTheLadder = false;
	}

	//ジャンプ動作(地上にいるときのみ可能)
	if (keyState.jump && !prevKeyState.jump){
		//ハシゴに掴まっているとき
		if (!onTheGround && onTheLadder && fallSpeed == 0.0f){
			fallSpeed = -jumpForce * 0.8f;	//80%の跳躍力でジャンプする
		}
		//地上にいるとき
		else if (onTheGround){
			fallSpeed = -jumpForce;			//ジャンプする(-16.0f)
			onTheGround = false;
		}
	}

	if (collisionWall && onTheGround){	//移動中に壁と接触しているとき
		animationState = State::Idol;
		moving = false;
	}

 	StageAction(stage);	//ステージ上でのアクション
}

void Player::StageAction(Stage* stage)
{
	if (usingFuroshiki) { return; }	//アイテム(フロシキ)使用中は以下を処理しない

	//[立ち状態: 中心Y - 高さ / 4] [しゃがみ状態: 中心Y + 高さ / 4]
	searchPosY = Y_CENTER + (keyState.shinobi ? height / 4.0f : -height / 4.0f);

	//Ｃキーでアイテム取得（ステージ上のオブジェクトにアクセスする）
	if (keyState.checkStage){
		int itemNum = stage->CheckStageItem(x, y, width, height);
		if (itemNum >= 0){
			itemList.push_back(stage->GetStageItem(itemNum));
		}
		stage->CheckStageObject(S_CAST_I(X_CENTER), S_CAST_I(Y_CENTER));
	}
	
	//Enemyに発見されていないときは隠れられる
	if (findCount == 0){
		//箱の裏にいるとき
		//しゃがみ時は 中心Y + (高さ / 4), 立ち状態は 中心Y - (高さ / 4)
		if (stage->GetStageData(S_CAST_I(X_CENTER), S_CAST_I(searchPosY)) == 3){
			hideRate = (moving ? 0.5f : 1.0f);	//移動していると50%隠れる
		}
		else if (!moving && keyState.shinobi){	//忍び足＋停止中
			hideRate = 0.3f;
		}
	}
}

void Player::UseItem(Stage* stage)
{
	usingFuroshiki = false;

	//----------フロシキ動作
	int furoshikiAnimNum = furoshikiAnim->GetCurrentVertexNum();
	if (keyState.furoshiki && onTheGround && findCount == 0){
		//フロシキを広げる処理
		furoshikiAnim->SetPlayMode(AnimPlayMode::NormalPlay);
		if (furoshikiAnimNum == 11){
			hideRate = 1.0f;
		}
		else if (furoshikiAnimNum == 6 && callfuroshiki == false){
			SoundManager::GetSoundManager()->CallSound(furoshikihr);
			callfuroshiki = true;
		}
		
		else{
			furoshikiAnim->Update();
		}
		animationState = State::Furoshiki;
		usingFuroshiki = true;
	}
	else if (furoshikiAnimNum > 0){
		//フロシキをしまう処理
		furoshikiAnim->SetPlayMode(AnimPlayMode::ReversePlay);
		furoshikiAnim->Update();
		animationState = State::Furoshiki;
		usingFuroshiki = true;
	}
	//フロシキ動作----------

	if (usingFuroshiki) { return; }	//アイテム(フロシキ)使用中は以下を処理しない
	callfuroshiki = false;	//風呂敷SE再生可能状態にする
	//攻撃キーの押下時間によって使用する武器を切り替える
	if (keyState.attack && !kunai->GetActive() && !makibishi->GetActive()){
		weaponKeyCounter++;
	}
	else if (weaponKeyCounter != 0){
		//境界値で武器切り替え
		if (weaponKeyCounter < weaponSwitchFrameNum){
			usingKunai = true;		//クナイ使用トリガー
		}
		else{
			usingMakibishi = true;	//マキビシ使用トリガー
		}
		weaponKeyCounter = 0;
	}

	//----------攻撃処理
	if (!attacking && !kunai->GetActive() && !makibishi->GetActive() && weaponFrameCounter < 0){
		if (usingKunai || usingMakibishi){
			attacking = true;
			attackAnim->Initialize();
		}
	}
	else if (attacking){
		attackAnim->Update();
		animationState = State::Attack;
		if (attackAnim->GetCurrentVertexNum() == 7){
			//アニメーションフレームが7番の時に武器を出現させる
			if (usingKunai){
				kunai->Activate(X_CENTER, Y_CENTER, direction);
				usingKunai = false;
			}
			else if (usingMakibishi){
				makibishi->Activate(X_CENTER, Y_CENTER, direction);
				usingMakibishi = false;
			}
			weaponFrameCounter = weaponInterval;
		}
		else if(attackAnim->EndOfAnimation()){
			attacking = false;
		}
	}
	//攻撃処理----------
}

void Player::LifeCheck()
{
	if (hp <= 0){
		if (debugMode){
			//デバッグモード時は体力をリセット
			hp = ResourceController::GetResourceController()->parameter.player_param.hp;
		}
		else{
			gameOverFlag = true;
		}
	}
}

void Player::SetAnimationGraphic()
{
	ResourceController* RC = ResourceController::GetResourceController();

	idolAnim = new Animation();
	idolAnim->CreateAnimation(&RC->player.idol);
	idolAnim->SetParameter(4);

	normalMoveAnim = new Animation();
	normalMoveAnim->CreateAnimation(&RC->player.walk);
	normalMoveAnim->SetParameter(4);

	slowMoveAnim = new Animation();
	slowMoveAnim->CreateAnimation(&RC->player.shinobi);
	slowMoveAnim->SetParameter(4);

	fastMoveAnim = new Animation();
	fastMoveAnim->CreateAnimation(&RC->player.dash);
	fastMoveAnim->SetParameter(1);

	ladderAnim = new Animation();
	ladderAnim->CreateAnimation(&RC->player.ladder);
	ladderAnim->SetParameter(4);

	attackAnim = new Animation();
	attackAnim->CreateAnimation(&RC->player.attack);
	attackAnim->SetParameter(1);

	furoshikiAnim = new Animation();
	furoshikiAnim->CreateAnimation(&RC->player.furoshiki);
	furoshikiAnim->SetParameter(3);

	foundAnim = new Animation();
	foundAnim->CreateAnimation(&RC->player.found);
	foundAnim->SetParameter(1);

	jumpAnim = new Vertex(0, 0, &RC->player.jump);

	UIImages.resize(5);
	for (std::vector<Vertex*>::iterator it = UIImages.begin(); it != UIImages.end(); it++){
		(*it) = new Vertex(0, 0, &RC->ui.images[it - UIImages.begin()]);
	}
}

void Player::UpdateAnimation()
{
	int defaultHP = ResourceController::GetResourceController()->parameter.player_param.hp;
	float alpha = S_CAST_F(hp) / defaultHP;
	bool mirrorFlag = (direction == Left);	//グラフィック反転フラグ
	
	switch (animationState){
	case State::Idol:	//待機中
		idolAnim->Update();
		dr->VertexPushBack(idolAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		break;

	case State::Walk:	//移動中
		if (keyState.dash /*|| findCount != 0*/){	//見つかっているときもダッシュアニメーションに変わるようにもできる
			fastMoveAnim->Update();
			dr->VertexPushBack(fastMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
			if ((fastMoveAnim->GetCurrentVertexNum() == 3 || fastMoveAnim->GetCurrentVertexNum() == 9) && callwalk == false){
				SoundManager::GetSoundManager()->CallSound(walkhr);
				callwalk = true;
			}
			else{
				callwalk = false;
			}
		}
		else{
			normalMoveAnim->Update();
			if ((normalMoveAnim->GetCurrentVertexNum() == 3 || normalMoveAnim->GetCurrentVertexNum() == 9) && callwalk == false){
				SoundManager::GetSoundManager()->CallSound(walkhr);
				callwalk = true;
			}
			else{
				callwalk = false;
			}
			dr->VertexPushBack(normalMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		}
		break;

	case State::Shinobi:	//忍び移動中
		if (moving){
			slowMoveAnim->Update();	//移動中のみアニメーションを更新する
			dr->VertexPushBack(slowMoveAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		}
		else{
			dr->VertexPushBack(slowMoveAnim->GetVertex(0)->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		}
		break;

	case State::Ladder:	//ハシゴ移動中
		if (moving){
			ladderAnim->Update();	//移動中のみアニメーションを更新する
		}
		dr->VertexPushBack(ladderAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		break;

	case State::Jump:	//ジャンプ中
		dr->VertexPushBack(jumpAnim->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		break;

	case State::Furoshiki:
		dr->VertexPushBack(furoshikiAnim->GetCurrentVertex()->SetDraw(x, y, false, alpha), drawRank);
		break;

	case State::Attack:
		dr->VertexPushBack(attackAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		break;

	case State::Found:
		foundAnim->Update();
		dr->VertexPushBack(foundAnim->GetCurrentVertex()->SetDraw(x, y, mirrorFlag, alpha), drawRank);
		break;
	}
}

void Player::PrintUI()
{
	uiOffsetY += uiDelta;
	if (uiOffsetY > 110.0f || uiOffsetY < 100.0f){
		uiDelta = -uiDelta;
	}

	int defaultHP = ResourceController::GetResourceController()->parameter.player_param.hp;
	float alpha = 1.0f - S_CAST_F(hp) / defaultHP;

	UIImageName imgName;
	if (damageFlag){
		imgName = UIImageName::Captuer;
		dr->VertexPushBack(UIImages[imgName]->SetDraw(X_CENTER - uiOffsetX, TOP - uiOffsetY, false, alpha));
	}
	else if (findCount != 0){
		imgName = UIImageName::Exclamation;
		dr->VertexPushBack(UIImages[imgName]->SetDraw(X_CENTER - uiOffsetX, TOP - uiOffsetY));
	}
	else if (hideRate >= 0.5f){		//隠れ率が50%以上なら
		imgName = UIImageName::Hide;	//「隠」マークを表示する
		dr->VertexPushBack(UIImages[imgName]->SetDraw(X_CENTER - uiOffsetX, TOP - uiOffsetY));
	}
	else if (stealthMoving && onTheGround && !onTheLadder){	//しゃがみ＋地上＋ハシゴでない
		imgName = UIImageName::Shinobi;
		dr->VertexPushBack(UIImages[imgName]->SetDraw(X_CENTER - uiOffsetX, TOP - uiOffsetY));
	}
}

void Player::InitCamera(Stage* stage)
{
	stageWidth = stage->GetWidth();	//ステージの幅を取得
	stageHeight = stage->GetHeight();	//ステージの高さを取得
	
	//プレイヤーが中心になるようにスクリーンを設定
	screen->x = X_CENTER - screen->Width() / 2.0f;
	screen->y = Y_CENTER - screen->Height() / 2.0f;
}

void Player::ScrollScreen()
{
	float      dx = x - prevX;	//Xスクロール量
	float      dy = y - prevY;	//Yスクロール量
	float hMargin = screen->Width() / 2.5f;		//水平のスクロール開始マージン
	float vMargin = screen->Height() / 5.0f;	//垂直のスクロール開始マージン

	//水平スクロール
	if (x < screen->x + hMargin){	//左側のスクロール範囲内にいるとき
		screen->x += (dx > 0.0f) ? (0.0f) : (dx);	//プラス方向へのスクロールを制限する
	}
	else if (RIGHT > screen->x + screen->Width() - hMargin){	//右側のスクロール範囲内にいるとき
		screen->x += (dx < 0.0f) ? (0.0f) : (dx);	//マイナス方向へのスクロールを制限する
	}

	//垂直スクロール
	if (y < screen->y + vMargin){	//上側のスクロール範囲内にいるとき
		screen->y += (dy > 0.0f) ? (0.0f) : (dy);	//プラス方向へのスクロールを制限する
	}
	else if (BOTTOM > screen->y + screen->Height() - vMargin){	//下側のスクロール範囲内にいるとき
		screen->y += (dy < 0.0f) ? (0.0f) : (dy);	//マイナス方向へのスクロールを制限する
	}

	//ステージ外へのスクロールを修正する(水平)
	if (screen->x < 0.0f){
		screen->x = 0.0f;
	}
	else if (screen->x > stageWidth - screen->Width()){
		screen->x = (float)stageWidth - screen->Width();
	}

	//ステージ外へのスクロールを修正する(垂直)
	if (screen->y < 0.0f){
		screen->y = 0.0f;
	}
	else if (screen->y > stageHeight - screen->Height()){
		screen->y = (float)stageHeight - screen->Height();
	}
}

void Player::DestroyPlayer()
{
	delete idolAnim;
	delete normalMoveAnim;
	delete slowMoveAnim;
	delete fastMoveAnim;
	delete ladderAnim;
	delete attackAnim;
	delete furoshikiAnim;
	delete foundAnim;
	delete jumpAnim;

	delete kunai;
	delete makibishi;

	for (unsigned int i = 0; i < UIImages.size(); i++){
		delete UIImages[i];
	}
}
