//---------------------
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
//--メモリリーク検出用--

//_CrtDumpMemoryLeaks()を呼び出すだけのクラス
//　このクラスはGameクラスより先にstatic宣言するだけでOK
class MemoryLeakChecker{
public:
	~MemoryLeakChecker()
	{
		_CrtDumpMemoryLeaks();
		//OutputDebugString("MemoryLeakChecker::~MemoryLeakChecker was called.\n");
	}
};