#ifndef _STAGEITEM_H_
#define _STAGEITEM_H_

#include <vector>
#include <opencv2\opencv.hpp>
#include "Item.h"
#include "StageObject.h"
#include <string>
class Vertex;
class Draw;
class Scene;
class ResourceController;
class Item;
class StageItem;
class StageObject;




//ステージクラス
class Stage
{
public:
	Stage();
	Stage(int num, int background);
	~Stage();
	Stage(const Stage &v);
	Stage& operator=(const Stage &v);
	bool Initialize();
	bool UpDate();
	//アイテムをリストから取り出す
	Item GetStageItem(int num);
	//ステージの終了処理
	bool Close();
	//ステージの幅を取得する
	int GetWidth()
	{
		return width;
	}
	//ステージの高さを取得する
	int GetHeight()
	{
		return height;
	}
	bool InitializeCheck(){
		return initializeFlag;
	}

	//プレイヤーの開始位置を取得する
	Position* GetStartPos(bool reverse = false);
	//エネミーの位置を取得する（変更不可）
	const std::vector<Position>* Enemy0Pos;
	const std::vector<Position>* Enemy1Pos;
	const std::vector<Position>* Enemy2Pos;
	const std::vector<Position>* Enemy3Pos;


	//テンプレート関数
	template <typename T>
	//天井を調べる(cx,開始位置cy)返値　天井y座標
	T GetCollision_Top(T cx, T cy){
		if ((cx <= 0 || cx >= width) || (cy <= 0 || cy >= height))	//キャラクターがステージ上から外れた場合（仮）
			return 0;
		for (int j = (int)cy; j > 0; j--){
			if (colision[(unsigned int)cx][j] == 1){
				return (T)j;			//Y座標を返す
			}
		}
		return 0;				//天井が存在しない場合

	}
	template <typename T>
	//地面を調べる(cx,開始位置cy)返値　地面y座標
	T GetCollision_Bottom(T cx, T cy){
		if ((cx <= 0 || cx >= width) || (cy <= 0 || cy >= height))	//キャラクターがステージ上から外れた場合（仮）
			return (T)height;
		for (int j = (int)cy; j < height; j++){
			if (colision[(unsigned int)cx][j] == 1){
				return (T)j;			//Y座標を返す
			}
		}
		return (T)height;				//地面が存在しない場合
	}
	template <typename T>
	//左壁を調べる(開始位置cx,cy)返値　左壁x座標
	T GetCollision_Left(T cx, T cy){
		if ((cx <= 0 || cx >= width) || (cy <= 0 || cy >= height)){	//キャラクターがステージ上から外れた場合
			return 0;
		}
		else{
			for (int x = (int)cx; 0 <= x; x--){
				if (colision[x][(unsigned int)cy] == 1){
					return (T)x;
				}
			}
			return 0;
		}
	}
	template <typename T>
	//右壁を調べる(開始位置cx,cy)返値　右壁x座標
	T GetCollision_Right(T cx, T cy){
		if ((cx <= 0 || cx >= width) || (cy <= 0 || cy >= height)){	//キャラクターがステージ上から外れた場合
			return (T)width;
		}
		else{
			for (int x = (int)cx; x < width; x++){
				if (colision[x][(unsigned int)cy] == 1){
					return (T)x;
				}
			}
			return (T)width;
		}
	}
	template <typename T>
	//ステージのオブジェクトデータを取得
	T GetStageData(T x, T y){
		if (x < 0)
			x = 0;
		else if (width <= x)
			x = width-1;
		if (y < 0)
			y = 0;
		else if (height <= y)
			y = height-1;
		return (T)stageData[(unsigned int)x][(unsigned int)y];
	}
	template <typename T>
	//指定位置のコリジョンを取得
	T GetStageCollision(T x, T y){
		if (width <= x)
			x = width - 1;
		if (height <= y)
			y = height - 1;
		return (T)colision[(unsigned int)x][(unsigned int)y];
	}
	//アイテムがあるか調べる
	template <typename T>
	int CheckStageItem(T x, T y,T width, T height){
		for (unsigned int i = 0; i < itemlist.size(); i++){
			if (itemlist[i].CheckPosition((int)x, (int)y,(int)width,(int)height)){
				return i;
			}
		}


		return -1;
	}
	template <typename T>
	//オブジェクトを位置で取得する
	bool CheckStageObject(T x, T y){
		for (unsigned int i = 0; i < objectlist.size(); i++){
			if (objectlist[i].CheckPosition(x, y)){
				ActionObject(objectlist[i]);
				return true;
			}
		}

		return false;
	}
private:
	//データ画像の指定色をカウント（正方形の場合はtrue）
	bool CheckRectImage(IplImage& img, int x, int y, int R, int G, int B, int& cntX, int&cntY);
	//指定領域を設定する
	void ClearRectImage(int x, int y, int countx, int county, int colision = 0, int stageDatanum = 0);
	bool ActionObject(StageObject& so);
	int stageNum;
	int backgroundNum;
	Scene* scene;
	Draw* dr;
	ResourceController* RC;
	Vertex* vertex;
	Vertex* background;
	int width, height;
	Position PlayerStartPos;
	std::vector<Position> EnemyStartPos0;
	std::vector<Position> EnemyStartPos1;
	std::vector<Position> EnemyStartPos2;
	std::vector<Position> EnemyStartPos3;
	std::vector<std::vector<int>> colision;	//コリジョン配列
	std::vector<std::vector<int>> stageData;//ステージ情報配列
	std::vector<int> enemy;//仮
	std::vector<StageItem> itemlist;		//アイテムリスト
	std::vector<StageObject> objectlist;	//オブジェクトリスト
	bool stageClear;							//クリアフラグ
	bool initializeFlag;
	bool initializeFlag_item;
};

#endif