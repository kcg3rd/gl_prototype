#ifndef ___BOSS_H___
#define ___BOSS_H___

#include "Enemy.h"
#include "Define.h"

class Animation;
class Player;
class Stage;

//ボスキャラクラス
class Boss : public Enemy
{
	//Bossの状態列挙型
	//enum BossState{ Walk, Attack };

public:
	Boss() : Enemy() {}	//デフォルトコンストラクタ
	Boss(float startPosX, float startPosY, float width, float height, float speed, float jumpForce, Direction startDir, int drawRank);		//コンストラクタ
	~Boss(){}	//デストラクタ

	void CreateBoss(Player* player);

	void LoadResourceControllerValue();

	void Initialize();
	void Update(Stage* stage);

private:
	Animation* normalMoveAnim;	//通常移動アニメーション
	Animation* attackAnim;		//攻撃アニメーション

	Player* playerInfo;		//Playerクラスへのポインター

	//BossState state;

	void SetAnimationGraphic();		//アニメーショングラフィックを設定する
	void UpdateAnimation();			//アニメーションクラスを更新する

	void DestroyBoss();

};
#endif